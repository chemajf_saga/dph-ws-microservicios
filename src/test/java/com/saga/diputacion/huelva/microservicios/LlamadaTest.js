/**
 * Package com.saga.diputacion.huelva.microservicios
 * File LlamadaTest.java
 * Created at 8 may. 2018
 * Created by chema.jimenez
 * 
 * Creamos un recurso de tipo DPHContenido (equivalente a Contenido Básiico) con id 15 en la zona "Rabida"
 */
var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://webservicems.ocms199.sagasoluciones.com/dphcontenido/create/",
  "method": "POST",
  "headers": {
    "Content-Type": "application/json",
    "Cache-Control": "no-cache",
    "Postman-Token": "199b73b7-2832-49a4-85ba-b49c9b48396b"
  },
  "processData": false,
  "data": "{\n\t\"Type\": \"B\",\n\t\"Id\": 15,\n\t\"Zona\": \"Rabida\",\n\t\"Data\": {\n\t\t\"Title\": \"La Feria\",\n\t\t\"Content\": {\n\t\t\t\"Text\": \"<h1 style=\\\"text-align: center;\\\">Lorem ipsum</h1><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer libero dolor, egestas egestas arcu ut, viverra dapibus lacus. Mauris ante est, semper a commodo ullamcorper, dapibus eget nisl. Cras faucibus metus diam, vitae ullamcorper quam dignissim vel. Proin feugiat sed orci ut consectetur. <a title=\\\"SAGA\\\" href=\\\"http://www.sagasoluciones.com/\\\" target=\\\"_blank\\\" rel=\\\"noopener noreferrer\\\">Sed hendrerit ex nisl, ut sagittis </a>tellus venenatis vitae. Nam tempor felis et rhoncus varius. Quisque efficitur nec dolor at commodo. Nullam nec egestas ante. Fusce at fermentum odio, ut molestie velit. Nulla iaculis diam libero, non vulputate erat consectetur sed. <strong>Phasellus</strong> ut aliquet nulla. Quisque ligula massa, pellentesque quis vulputate ut, ultrices sit amet magna. Duis ultrices turpis fermentum, viverra turpis fringilla, facilisis leo. Maecenas et eros dignissim, <em>sodales leo vitae</em>, luctus diam. Etiam lectus sem, cursus vitae tristique at, pulvinar ut mi.</p><p style=\\\"aling:center;margin:auto 2px; border:2px solid red;padding: 2px\\\"><img src=\\\"http://www.diphuelva.es/portalweb/zonas/1/Imagenes/Abril_18/Web_capsula_tiempo.jpg\\\" alt=\\\"La imagen de una Asociacion\\\" /></p><blockquote><p>\\\"<em>quam id faucibus. Aenean quis iaculis tortor, vitae tincidunt tortor. Nam scelerisque ante elementum finibus semper. Nullam quis purus turpis. Aliquam dapibus sagittis nulla quis tincidunt. Vestibulum rhoncus cursus sapien, nec pellentesque metus rutrum sit amet. Quisque consectetur, enim eget iaculis cursus, tortor tellus posuere sem, in sagittis mi leo a felis. Ut nec ullamcorper sapien, ut vehicula urna.</em>\\\"</p></blockquote>\"\n\t\t},\n\t\t\"Availability\": {\n\t\t\t\"Expiration\": \"\"\n\t\t},\n\t\t\"DestacadoPortada\": \"true\",\n\t\t\"Tags\": [\"feria\", \"fiestas\"]\n\t},\n\t\"Municipio\": [\"La Rábida\", \"Huelva\"],\n\t\"Tesela\": {\n\t\t\"Texto\": \"¡Viva la feria!\",\n\t\t\"Ancho\": 2,\n\t\t\"Alto\": 2,\n\t\t\"Imagen\": \"http://www.diphuelva.es/portalweb/zonas/1/Imagenes/Abril_18/tesela_proyecto_discriminacion.jpg\"\n\t},\n\t\"Relacionados\": {\n\t\t\"1\": [\"cultura\",\"B\"],\n\t\t\"2\": [\"cultura\",\"B\"]\n\t}\n}"
}

$.ajax(settings).done(function (response) {
  console.log(response);
});