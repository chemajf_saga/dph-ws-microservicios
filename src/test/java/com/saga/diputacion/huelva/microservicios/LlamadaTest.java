/**
 * Package com.saga.diputacion.huelva.microservicios
 * File LlamadaTest.java
 * Created at 8 may. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * @author chema.jimenez
 * @since 8 may. 2018
 *
 *        Creamos un recurso de tipo DPHContenido (equivalente a Contenido Básico) con id 15 en la zona "Rabida"
 */
public class LlamadaTest {
    /**
     * <pre>
     * Dependencias necesarias:
           https://mvnrepository.com/artifact/com.mashape.unirest/unirest-java
           https://mvnrepository.com/artifact/org.apache.httpcomponents/httpclient
           https://mvnrepository.com/artifact/org.apache.httpcomponents/httpasyncclient
           https://mvnrepository.com/artifact/org.apache.httpcomponents/httpmime
           https://mvnrepository.com/artifact/org.apache.httpcomponents/httpcore
     * </pre>
     *
     * @param args
     */
    public static void main(String[] args) {
        try {

            HttpResponse<String> response = Unirest.post("http://webservicems.ocms199.sagasoluciones.com/dphcontenido/create/")
                            .header("Content-Type", "application/json").header("Cache-Control", "no-cache")
                            .header("Postman-Token", "2cc84791-18d1-4d2f-9c5f-bdd9957e20dd")
                            .body("{\"Type\": \"B\"," + "\"Id\": 15555," + "\"Zona\": \"cultura\"," + "\"Data\": {" + "\"Title\": \"La Feria\","
                                            + "\"Content\": {"
                                            + "\"Text\": \"<h1 style=\\\"text-align: center;\\\">Lorem ipsum</h1><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer libero dolor, egestas egestas arcu ut, viverra dapibus lacus. Mauris ante est, semper a commodo ullamcorper, dapibus eget nisl. Cras faucibus metus diam, vitae ullamcorper quam dignissim vel. Proin feugiat sed orci ut consectetur. <a title=\\\"SAGA\\\" href=\\\"http://www.sagasoluciones.com/\\\" target=\\\"_blank\\\" rel=\\\"noopener noreferrer\\\">Sed hendrerit ex nisl, ut sagittis </a>tellus venenatis vitae. Nam tempor felis et rhoncus varius. Quisque efficitur nec dolor at commodo. Nullam nec egestas ante. Fusce at fermentum odio, ut molestie velit. Nulla iaculis diam libero, non vulputate erat consectetur sed. <strong>Phasellus</strong> ut aliquet nulla. Quisque ligula massa, pellentesque quis vulputate ut, ultrices sit amet magna. Duis ultrices turpis fermentum, viverra turpis fringilla, facilisis leo. Maecenas et eros dignissim, <em>sodales leo vitae</em>, luctus diam. Etiam lectus sem, cursus vitae tristique at, pulvinar ut mi.</p><p style=\\\"aling:center;margin:auto 2px; border:2px solid red;padding: 2px\\\"><img src=\\\"http://www.diphuelva.es/portalweb/zonas/1/Imagenes/Abril_18/Web_capsula_tiempo.jpg\\\" alt=\\\"La imagen de una Asociacion\\\" /></p><blockquote><p>\\\"<em>quam id faucibus. Aenean quis iaculis tortor, vitae tincidunt tortor. Nam scelerisque ante elementum finibus semper. Nullam quis purus turpis. Aliquam dapibus sagittis nulla quis tincidunt. Vestibulum rhoncus cursus sapien, nec pellentesque metus rutrum sit amet. Quisque consectetur, enim eget iaculis cursus, tortor tellus posuere sem, in sagittis mi leo a felis. Ut nec ullamcorper sapien, ut vehicula urna.</em>\\\"</p></blockquote>\""
                                            + "}," + "\"Availability\": {" + "\"Expiration\": \"\"" + "}," + "\"DestacadoPortada\": \"true\","
                                            + "\"Tags\": [\"feria\", \"fiestas\"]" + "}," + "\"Municipio\": [\"La Rábida\", \"Huelva\"],"
                                            + "\"Tesela\": {" + "\"Texto\": \"¡Viva la feria!\"," + "\"Ancho\": 2," + "\"Alto\": 2,"
                                            + "\"Imagen\": \"http://www.diphuelva.es/portalweb/zonas/1/Imagenes/Abril_18/tesela_proyecto_discriminacion.jpg\""
                                            + "}," + "\"Relacionados\": {" + "\"1\": [\"cultura\",\"B\"]," + "\"2\": [\"cultura\",\"B\"]" + "}" + "}")
                            .asString();
            System.out.println(response.getBody());
        } catch (UnirestException e) {
            e.printStackTrace();
        }
    }
}
