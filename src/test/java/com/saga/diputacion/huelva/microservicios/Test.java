package com.saga.diputacion.huelva.microservicios;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.http.client.ClientProtocolException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.opencms.json.JSONException;

import com.saga.diputacion.huelva.microservicios.config.Constants;
import com.saga.diputacion.huelva.microservicios.util.StringUtils;

@SuppressWarnings("unused")
public class Test extends Constants {

    public static final String html =
                    "<div><p>Lorem ipsum dolor sit amet, <img src=\"/portalweb/zonas/44/Imagenes/tesela_sala_1.jpg\" /> consectetur adipiscing elit. Donec eu tincidunt lorem. Nullam placerat put. Phasellus vitae lacus <a href=\"www.google.es\">laoreet</a> lacus tincidunt mattis. Fusce sodales magna non sodales molestie. Nam iaculis ornare odio nec rhoncus.</p><p>Lorem ipsum dolor sit amet, <img src=\"/portalweb/zonas/44/Imagenes/tesela_sala_2.jpg\" />consectetur adipiscing elit. Donec eu tincidunt lorem. Nullam placerat put. Phasellus vitae lacus <a href=\"www.google.es\">laoreet</a> lacus tincidunt mattis. Fusce <img src=\"/portalweb/zonas/44/Imagenes/tesela_sala_3.jpg\" /> sodales magna non sodales molestie. Nam iaculis ornare odio nec rhoncus.</p></div>";

    public static final String html2 =
                    "<div><p><h1 style=\"text-align: center;\">Lorem ipsum</h1></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer libero dolor, egestas egestas arcu ut, viverra dapibus lacus. Mauris ante est, semper a commodo ullamcorper, dapibus eget nisl. Cras faucibus metus diam, vitae ullamcorper quam dignissim vel. Proin feugiat sed orci ut consectetur. <a title=\"SAGA\" href=\"http://www.sagasoluciones.com/\" target=\"_blank\" rel=\"noopener noreferrer\">Sed hendrerit ex nisl, ut sagittis </a>tellus venenatis vitae. Nam tempor felis et rhoncus varius. Quisque efficitur nec dolor at commodo. Nullam nec egestas ante. Fusce at fermentum odio, ut molestie velit. Nulla iaculis diam libero, non vulputate erat consectetur sed. <strong>Phasellus</strong> ut aliquet nulla. Quisque ligula massa, pellentesque quis vulputate ut, ultrices sit amet magna. Duis ultrices turpis fermentum, viverra turpis fringilla, facilisis leo. Maecenas et eros dignissim, <em>sodales leo vitae</em>, luctus diam. Etiam lectus sem, cursus vitae tristique at, pulvinar ut mi.</p><p style=\"aling:center;margin:auto 2px; border:2px solid red;padding: 2px\"><img src=\"http://www.diphuelva.es/portalweb/zonas/14/Imagenes/Plan_de_Acci%C3%B3n_2015.jpg\" alt=\"La imagen de una Asociacion\" /></p><p>Pellentesque vitae sem est. Cras malesuada lorem ligula, in ornare massa pulvinar ac. Praesent laoreet, orci non auctor pharetra, massa nulla dapibus erat, euismod gravida justo nisl quis ex. Duis eu finibus purus, ut molestie nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; <span style=\"text-decoration: line-through;\">Nunc dignissim condimentum pulvinar</span>. <span style=\"text-decoration: underline;\">Curabitur varius odio</span> ac nisl laoreet efficitur ac vehicula orci. In hac habitasse platea dictumst. Integer sem mauris, placerat ut ipsum eget, placerat ultrices eros. Proin magna eros, dapibus a porttitor ut, auctor in massa. Praesent iaculis euismod tortor, in suscipit sem feugiat id.</p><p>Vivamus eget tellus eros. Aenean ullamcorper imperdiet feugiat:</p><ul><li>Donec in sapien eu tellus sagittis fringilla.</li><li>Etiam mollis cursus lacus tempus vestibulum.<ul><li>Integer posuere eget lectus non vestibulum.</li><li>Suspendisse non lobortis neque.</li></ul></li><li>Vestibulum a turpis quis quam ultrices tincidunt viverra quis lorem.<ol><li>Praesent tortor metus,</li><li>facilisis eu bibendum et,</li><li>tempus in purus.</li></ol></li></ul><h2 style=\"text-align: center;\">Aenean vel eleifend enim. Nunc aliquam erat sed vehicula porttitor.</h2><p>Sed vestibulum, sem eu aliquet mattis, tortor nisl porttitor odio, non pulvinar risus enim et arcu. Fusce nulla lorem, vestibulum eu justo vel, efficitur vestibulum augue. Morbi rhoncus scelerisque sodales. Fusce porttitor quam et nisi consectetur efficitur. Donec ultrices justo ac risus venenatis commodo. Cras eu laoreet neque. Fusce nec sem dolor. Morbi sagittis vehicula nisl id faucibus. Praesent et ultricies erat, congue tempus justo. Vivamus posuere ante nec quam elementum, ut fringilla libero lacinia. Sed luctus tellus neque, id cursus turpis blandit nec. Pellentesque ac imperdiet nunc. Phasellus cursus nibh quis rhoncus ornare. Sed faucibus nisi consectetur tellus tristique, quis sollicitudin purus viverra.</p><h3 style=\"text-align: center;\">x<sup>2</sup> = y<sup>2</sup> * (Π+r<sup>2</sup>)</h3><p>Aenean tincidunt, nisi sed interdum imperdiet, neque mi viverra felis, ac semper velit dolor fringilla dolor. Morbi ullamcorper molestie erat, quis condimentum odio tincidunt sit amet. Pellentesque massa erat, placerat in sapien eget, convallis egestas lacus. Quisque mollis dictum ex, molestie mattis neque. Quisque quis odio est. Curabitur vel finibus massa. Sed et mollis elit. Vestibulum lacinia purus orci, sed tempor ipsum vestibulum eu. Donec at vestibulum lorem.</p><p>Curabitur sollicitudin elit non enim pellentesque pharetra. Suspendisse vehicula dolor vitae vulputate viverra. Sed pharetra elit rutrum mauris vestibulum, venenatis ultricies quam luctus. Phasellus id leo bibendum, laoreet tortor a, porta nibh. Mauris scelerisque ex a tellus dictum, eu tempor eros ornare. Nunc lacus mi, varius id volutpat sit amet, congue eget turpis.</p><p>Mauris pretium aliquam:</p><blockquote><p>\"<em>quam id faucibus. Aenean quis iaculis tortor, vitae tincidunt tortor. Nam scelerisque ante elementum finibus semper. Nullam quis purus turpis. Aliquam dapibus sagittis nulla quis tincidunt. Vestibulum rhoncus cursus sapien, nec pellentesque metus rutrum sit amet. Quisque consectetur, enim eget iaculis cursus, tortor tellus posuere sem, in sagittis mi leo a felis. Ut nec ullamcorper sapien, ut vehicula urna.</em>\"</p></blockquote></div>";

    public static final String html3 =
                    "<a title=\"SAGA\" href=\"http://www.diphuelva.es/portalweb/zonas/32/Imagenes/sala1.JPG\" target=\"_blank\" rel=\"noopener noreferrer\">imagen DPH abs</a>"
                                    + "<a title=\"SAGA\" href=\"/portalweb/zonas/32/Imagenes/sala1.JPG\" target=\"_blank\" rel=\"noopener noreferrer\">imagen DPH rel</a>"
                                    + "<a title=\"SAGA\" href=\"http://www.sagasoluciones.com//export/sites/sagaweb/.galleries/imagenes-contenido/main-image-servicios.jpg/\" target=\"_blank\" rel=\"noopener noreferrer\">imagen externa</a>"
                                    + "<a title=\"SAGA\" href=\"http://www.diphuelva.es/portalweb/zonas/1/Multimedia/Noticia_Huelva_Extrema_WEB.mp4\" target=\"_blank\" rel=\"noopener noreferrer\">documento DPH abs</a>"
                                    + "<a title=\"SAGA\" href=\"/portalweb/zonas/1/Multimedia/Noticia_Huelva_Extrema_WEB.mp4\" target=\"_blank\" rel=\"noopener noreferrer\">documento DPH rel</a>"
                                    + "<a title=\"SAGA\" href=\"http://www.sagasoluciones.com/doc/killo.mp3\" target=\"_blank\" rel=\"noopener noreferrer\">documento externa</a>";

    static String file1 = "/gsdfg/sdfg/sdfg.sdfgsd/sdfg-sdfg/Gsdfg/file.mp3";
    static String file2 = "/gsdfg/sdfg/sdfg.sdfgsd/sdfg-sdfg/Gsdfg/file.wav";
    static String file3 = "/gsdfg/sdfg/sdfg.sdfgsd/sdfg-sdfg/Gsdfg/file.ogg";
    static String file11 = "/gsdfg/sdfg/sdfg.sdfgsd/sdfg-sdfg/Gsdfg/file.MP3";
    static String file22 = "/gsdfg/sdfg/sdfg.sdfgsd/sdfg-sdfg/Gsdfg/file.WAV";
    static String file33 = "/gsdfg/sdfg/sdfg.sdfgsd/sdfg-sdfg/Gsdfg/file.OGG";

    static Map<String, String> AUDIOS = new HashMap<>();

    static {
        Map<String, String> auxMap = new HashMap<>();
        auxMap.put("mp3", "FileMp3");
        auxMap.put("wav", "FileWav");
        auxMap.put("ogg", "FileOgg");
        AUDIOS = Collections.unmodifiableMap(auxMap);
    }

    public static void main(String[] args) {
        try {
            System.out.println(repairSrcInHtml(html3));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String repairSrcInHtml(String html) throws JSONException, Exception {
        StringTokenizer st = new StringTokenizer(html.trim(), "\"", true);
        String res = "";
        while (st.hasMoreElements()) {
            String token = st.nextElement().toString();
            res += token;
            if (token.contains(SRC) || token.contains(HREF)) {
                res += st.nextElement().toString();
                res += manageSrcHref(st.nextElement().toString());
            }
        }
        return res;
    }

    private static String manageSrcHref(String href)
                    throws MalformedURLException, ClientProtocolException, JSONException, IOException, URISyntaxException, Exception {
        System.out.println("href: " + href);
        if (href.startsWith("/")) {
            href = HOST_DPH_WITH_HTTP + href;
        }

        if (StringUtils.isNotEmpty(href) && (new URL(href)).getHost().equalsIgnoreCase(HOST_DPH)) {
            if (isImg(href)) {
                return createFileInOpenCms(href, IMAGE_TYPE);
            } else {
                if (isDoc(href)) {
                    return createFileInOpenCms(href, BINARY_TYPE);
                }
            }
        }
        return href;
    }

    /**
     * @param href
     * @return
     */
    private static boolean isDoc(String href) {
        return (

        StringUtils.isNotEmpty(href) && (StringUtils.endsWithAnyIgnoreCase(href, EXT_AUDIO) || StringUtils.endsWithAnyIgnoreCase(href, EXT_VIDEO)));
    }

    /**
     * @param href
     * @return
     */
    private static boolean isImg(String href) {
        return (StringUtils.isNotEmpty(href) && StringUtils.endsWithAnyIgnoreCase(href, EXT_IMG));
    }

    private static String createFileInOpenCms(String fileUrl, String fileType)
                    throws JSONException, ClientProtocolException, IOException, URISyntaxException, Exception {
        String fileCreated = "";
        if (StringUtils.isNotBlank(fileUrl) && StringUtils.isNotBlank(fileType)) {
            String typeToLog = "";
            String typeToCreate = "";
            String galeryPath = "";
            // Que tipo de archivos vamos a "subir"?
            if (fileType.equalsIgnoreCase(IMAGE_TYPE)) {
                // Imagen
                typeToLog = "Imagen";
                typeToCreate = IMAGE_TYPE;
                galeryPath = "galeria_imagenes";
            } else {
                if (fileType.equalsIgnoreCase(VIDEO_TYPE)) {
                    // Video
                    typeToLog = "Video";
                    typeToCreate = BINARY_TYPE;
                    galeryPath = "galeria_videos";
                } else {
                    if (fileType.equalsIgnoreCase(AUDIO_TYPE)) {
                        // Audio
                        typeToLog = "Audio";
                        typeToCreate = BINARY_TYPE;
                        galeryPath = "galeria_audio";
                    } else {
                        if (fileType.equalsIgnoreCase("binary")) {
                            // Audio
                            typeToLog = "Documento";
                            typeToCreate = BINARY_TYPE;
                            galeryPath = "galeria_documento";
                        } else {

                        }
                    }
                }
            }
            System.out.println(" - " + typeToLog + ": " + fileUrl);
            fileCreated = ("target_site/" + "zona/" + galeryPath + getRelativePathAndNameFromURL(fileUrl)).replaceAll("//", "/");
            System.out.println(fileCreated);
        }
        return fileCreated;
    }

    private static String getRelativePathAndNameFromURL(String url) {
        String res = "";
        if (StringUtils.isNotEmpty(url)) {
            if (url.contains(IMAGES_GALLERIES_ORIGIN)) {
                res = url.split(IMAGES_GALLERIES_ORIGIN)[1].replace("//", "/");
            } else {
                if (url.contains(VIDEOS_GALLERIES_ORIGIN)) {
                    res = url.split(VIDEOS_GALLERIES_ORIGIN)[1].replace("//", "/");
                }
            }
        }
        return res;
    }

    private static void htmlToPlainText(String htmlStr) {
        Document doc = Jsoup.parse(htmlStr);

        Elements eltosLI = doc.select("li");
        int nLi = eltosLI.size();

        // System.out.println(doc.toString());
        StringBuffer res = new StringBuffer();
        // System.out.println("");
        htmlStr = htmlStr.replace("<", " <").replace(">", "> ");
        StringTokenizer st = new StringTokenizer(htmlStr, " ", false);
        boolean salto = false;
        int contLI = 0;
        while (st.hasMoreElements()) {
            salto = false;
            String object = st.nextElement().toString();
            // System.out.println(object);

            if (object.contains("<") && object.contains(">")) {
                if (object.contains("</p>") || object.contains("</div>") || object.contains("</h")) {
                    res.append("\r\n");
                }
                if (object.contains("<li")) {
                    res.append("\r\n").append(" - ");
                }
                if (object.contains("/li")) {
                    contLI++;
                    if (contLI == nLi - 1) {
                        res.append("\r\n");
                    }
                }
            } else {
                if (object.contains("<")) {
                    while (st.hasMoreElements()) {
                        salto = false;
                        String object2 = st.nextElement().toString();
                        if (object2.contains(">")) {
                            break;
                        }
                    }
                } else {
                    res.append(object).append(" ");
                    if (salto) {
                        res.append("\r\n");
                    }
                }
            }

        }
        System.out.println(res.toString().replace("\r\n\r\n", "\r\n").replace(" , ", ", ").replace(" . ", ". ").replace(" \" ", " \"")
                        .replace(". \" ", ".\"").replace(". \"", ".\""));
    }

    private static void getFileExtension(String file) {
        // FileMp3, FileWav, FileOgg
        String key = (file.split("\\.")[file.split("\\.").length - 1]).toLowerCase();
        System.out.println(AUDIOS.get(key));
    }

    private static void dateToTimemilis() {
        GregorianCalendar hoy = new GregorianCalendar(2053, 11, 28, 0, 0, 0);
        System.out.println(hoy.getTimeInMillis());
    }

    private static void tokenizando(String str) {
        System.out.println("");
        StringTokenizer st = new StringTokenizer(str, "\"", true);
        while (st.hasMoreElements()) {
            String object = st.nextElement().toString();
            System.out.println(object);
        }

    }

    private static void stringToHtml(String htmlStr) {
        Document doc = Jsoup.parse(htmlStr);
        System.out.println(doc.toString());
        System.out.println();

        Elements links = doc.select("img[src]");
        for (Element elto : links) {
            System.out.print(elto + "\t--\t");
            System.out.println(elto.attr("src"));
        }
    }
}