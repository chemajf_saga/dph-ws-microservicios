/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphnoticia
 * File DPHNoticia.java
 * Created at 26 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.view.dphnoticia;

import static com.saga.diputacion.huelva.microservicios.config.Constants.IMAGE_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ALTO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ANCHO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_CONTENT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DATA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_IMAGE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_IMAGEN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_IMAGE_MAIN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_HEIGHT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_IMAGE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_TITLE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_WIDTH;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_MEDIA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TESELA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TEXT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TEXTO;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.opencms.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.saga.diputacion.huelva.microservicios.controller.Controller;
import com.saga.diputacion.huelva.microservicios.util.StringUtils;

/**
 * @author chema.jimenez
 * @since 26 abr. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
@SuppressWarnings("unchecked")
public class DPHNoticiaController extends Controller {

    private HashMap<String, Object> data;

    /**
     * @param context
     * @param req
     * @param res
     */
    public DPHNoticiaController(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /**
     *
     * @return Mapa con los atributos del recurso a crear/modificar
     * @throws Exception
     */
    protected HashMap<String, Object> generateAttributesMap() throws Exception {
        String jsonData = this.getJsonObject().getJSONObject(JSON_DATA).toString();
        this.data = new ObjectMapper().readValue(jsonData, (new HashMap<String, Object>()).getClass());

        String textHtml = this.repairSrcInHtml(this.getJsonObject().getJSONObject(JSON_DATA).getJSONObject(JSON_CONTENT).getString(JSON_TEXT));
        ((HashMap<String, Object>) this.data.get(JSON_CONTENT)).put(JSON_TEXT, textHtml);

        // Informacion de la TESELA
        JSONObject tesela = this.getJsonObject().getJSONObject(JSON_TESELA);
        this.data.put(JSON_LIST_TITLE, tesela.getString(JSON_TEXTO));

        // Tratamos la imgen de la tesela
        String teselaImgOrigin = tesela.getString(JSON_IMAGEN);
        String teselaImgCreated = this.createFileInOpenCms(teselaImgOrigin, IMAGE_TYPE);
        this.data.put(JSON_LIST_IMAGE, teselaImgCreated);

        // Hay que controlar el ancho y alto
        this.data.put(JSON_LIST_WIDTH, (tesela.getInt(JSON_ANCHO) > 4 ? 4 : tesela.getInt(JSON_ALTO)) + "");
        this.data.put(JSON_LIST_HEIGHT, (tesela.getInt(JSON_ALTO) > 2 ? 2 : tesela.getInt(JSON_ALTO)) + "");

        // Tratamos la imagen de la noticia
        String imgNoticiaOrigin = this.getJsonObject().getJSONObject(JSON_DATA).getJSONObject(JSON_CONTENT).getJSONObject(JSON_MEDIA)
                        .getJSONObject(JSON_IMAGE_MAIN).getString(JSON_IMAGE);

        if (StringUtils.isNotBlank(imgNoticiaOrigin)) {
            String imgNoticiaCreated = this.createFileInOpenCms(imgNoticiaOrigin, IMAGE_TYPE);
            (((HashMap<String, Object>) ((HashMap<String, Object>) ((HashMap<String, Object>) this.data.get(JSON_CONTENT)).get(JSON_MEDIA))
                            .get(JSON_IMAGE_MAIN))).put(JSON_IMAGE, imgNoticiaCreated);
        } else {
            ((HashMap<String, Object>) this.data.get(JSON_CONTENT)).remove(JSON_MEDIA);
        }

        return this.data;
    }

}
