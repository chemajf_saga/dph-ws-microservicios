/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphevento
 * File DPHEventoControllerRead.java
 * Created at 9 may. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.view.dphevento;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

/**
 * @author chema.jimenez
 * @since 9 may. 2018
 * 
 *        <pre>
 *
 *        </pre>
 */
public class DPHEventoControllerRead extends DPHEventoController {

    /**
     * @param context
     * @param req
     * @param res
     */
    public DPHEventoControllerRead(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.saga.diputacion.huelva.microservicios.controller.Controller#execute()
     */
    @Override
    public void execute() throws Exception {
        this.read();
    }

}
