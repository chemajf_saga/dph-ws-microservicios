/**
 * Package com.saga.diputacion.huelva.microservicios.exception
 * File NotJsonInRequestException.java
 * Created at 13 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.exception;

/**
 * @author chema.jimenez
 * @since 13 abr. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class NotJsonInRequestException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 4171909666648923440L;

    /**
     *
     */
    public NotJsonInRequestException() {
        super();
    }

    /**
     * @param message
     */
    public NotJsonInRequestException(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public NotJsonInRequestException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public NotJsonInRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public NotJsonInRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
