/**
 * Package com.saga.diputacion.huelva.microservicios.exception
 * File NotPropertiFoundException.java
 * Created at 10 may. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.exception;

/**
 * @author chema.jimenez
 * @since 10 may. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class NotPropertiFoundException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -4754308415883928030L;

    /**
     *
     */
    public NotPropertiFoundException() {
    }

    /**
     * @param message
     */
    public NotPropertiFoundException(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public NotPropertiFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public NotPropertiFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public NotPropertiFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
