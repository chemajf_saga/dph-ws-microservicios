/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphenlace
 * File DPHEnlaceController.java
 * Created at 30 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.view.dphenlace;

import static com.saga.diputacion.huelva.microservicios.config.Constants.IMAGE_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ALTO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ANCHO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DATA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_IMAGE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_HEIGHT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_TITLE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_WIDTH;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TESELA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TEXTO;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.opencms.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.saga.diputacion.huelva.microservicios.controller.Controller;

/**
 * @author chema.jimenez
 * @since 30 abr. 2018
 *
 */
@SuppressWarnings("unchecked")
public class DPHEnlaceController extends Controller {

    /**
     * @param context
     * @param req
     * @param res
     */
    public DPHEnlaceController(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /**
     *
     * @return Mapa con los atributos del recurso a crear/modificar
     * @throws Exception
     */
    protected HashMap<String, Object> generateAttributesMap() throws Exception {
        String jsonData = this.getJsonObject().getJSONObject(JSON_DATA).toString();
        HashMap<String, Object> data = new ObjectMapper().readValue(jsonData, (new HashMap<String, Object>()).getClass());

        // Informacion de la TESELA
        JSONObject tesela = this.getJsonObject().getJSONObject(JSON_TESELA);
        data.put(JSON_LIST_TITLE, tesela.getString(JSON_TEXTO));

        // Hay que controlar el ancho y alto
        data.put(JSON_LIST_WIDTH, (tesela.getInt(JSON_ANCHO) > 4 ? 4 : tesela.getInt(JSON_ALTO)) + "");
        data.put(JSON_LIST_HEIGHT, (tesela.getInt(JSON_ALTO) > 2 ? 2 : tesela.getInt(JSON_ALTO)) + "");

        // Tratamos la imgen del enlace
        String imgOrigin = this.getJsonObject().getJSONObject(JSON_DATA).getString(JSON_IMAGE);
        String imgCreated = this.createFileInOpenCms(imgOrigin, IMAGE_TYPE);
        data.put(JSON_IMAGE, imgCreated);

        return data;
    }
}
