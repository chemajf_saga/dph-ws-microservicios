/**
 * Package com.saga.diputacion.huelva.microservicios.controller
 * File Controller.java
 * Created at 12 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.controller;

import static com.saga.diputacion.huelva.microservicios.config.Constants.AGENDA_GALLERIES_ORIGIN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.AUDIO_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.BINARY_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.DOCUMENTOS_GALLERIES_ORIGIN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.EXT_AUDIO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.EXT_DOC;
import static com.saga.diputacion.huelva.microservicios.config.Constants.EXT_IMG;
import static com.saga.diputacion.huelva.microservicios.config.Constants.EXT_VIDEOS;
//import static com.saga.diputacion.huelva.microservicios.config.Constants.EXT_VIDEO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.FICHEROS_GALLERIES_ORIGIN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.FOLDER_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.HOST_DPH;
import static com.saga.diputacion.huelva.microservicios.config.Constants.HOST_DPH_WITH_HTTP;
import static com.saga.diputacion.huelva.microservicios.config.Constants.HREF;
import static com.saga.diputacion.huelva.microservicios.config.Constants.IMAGES_GALLERIES_ORIGIN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.IMAGE_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ATTACHMENT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_CODE_RESPONSE_PATTERN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_CONTENT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_CONTENT_RESPONSE_PATTERN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DATA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DOCUMENTOS;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ENLACES;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_FILE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_GALLERY;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_HREF;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ID;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LINK;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LINKS_GALLERY;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_MUNICIPIO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_NOMBRE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_OPTION;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_RELACIONADOS;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_RELATED_CONTENT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TEMATICA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TITLE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_URL;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ZONA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.LOCALE_STR;
import static com.saga.diputacion.huelva.microservicios.config.Constants.REQ_METHOD_OK;
import static com.saga.diputacion.huelva.microservicios.config.Constants.SRC;
import static com.saga.diputacion.huelva.microservicios.config.Constants.VIDEOS_GALLERIES_ORIGIN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.VIDEO_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.ZERO;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.apache.commons.logging.Log;
import org.apache.http.client.ClientProtocolException;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsProperty;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.json.JSONArray;
import org.opencms.json.JSONException;
import org.opencms.json.JSONObject;
import org.opencms.json.XML;
import org.opencms.jsp.CmsJspActionElement;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.opencms.workplace.CmsWorkplaceAction;

import com.saga.diputacion.huelva.microservicios.config.Configuration;
import com.saga.diputacion.huelva.microservicios.exception.NotJsonInRequestException;
import com.saga.diputacion.huelva.microservicios.exception.NotPropertiFoundException;
import com.saga.diputacion.huelva.microservicios.exception.NotValidJsonInRequestException;
import com.saga.diputacion.huelva.microservicios.exception.NotValidUseWebServiceMicroservicios;
import com.saga.diputacion.huelva.microservicios.manager.resources.ResourceCMS;
import com.saga.diputacion.huelva.microservicios.manager.resources.ResourceContent;
import com.saga.diputacion.huelva.microservicios.manager.resources.ResourcesManager;
import com.saga.diputacion.huelva.microservicios.util.StringUtils;
import com.saga.diputacion.huelva.microservicios.util.TimeUtils;

/**
 * Controlador principal del WebService.
 *
 * @author chema.jimenez
 * @since 12 abr. 2018
 *
 */
public class Controller extends CmsJspActionElement implements IController {
    private Log LOG = CmsLog.getLog(Controller.class);
    private String targetSite = "";
    private JSONObject jsonObject;
    private JSONObject jsonObjectResponse;
    private boolean isValid = false;
    private CmsObject cmsObjectAdmin;
    private ResourceCMS resourceCMS;
    private ResourcesManager resourceManager;
    String jsonRes = "{\"code\" : \"" + JSON_CODE_RESPONSE_PATTERN + "\", \"content\" : " + JSON_CONTENT_RESPONSE_PATTERN + "}";

    /**
     * @param context
     * @param req
     * @param res
     */
    public Controller(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.saga.diputacion.huelva.microservicios.controller.IController#save()
     */
    @Override
    public void save() throws Exception {

    }

    /*
     * (non-Javadoc)
     *
     * @see com.saga.diputacion.huelva.microservicios.controller.IController#execute()
     */
    @Override
    public void execute() throws Exception {
        this.LOG.debug("-EXECUTE-");
        this.LOG.debug(" - Cambiamos proyecto a Offline");
        this.changeProyectOffline();
    }

    protected CmsResource createResource(HashMap<String, Object> data) throws Exception {
        CmsResource resource = null;

        String typeTargetContenido = this.getTargetType();

        // Tenemos que crear el contenido y guardarlo
        String resourcePathAndName = this.getResourcePathAndName();

        // Si el recurso existe
        boolean exists = this.getCmsObjectAdmin().existsResource(resourcePathAndName, CmsResourceFilter.ALL);
        if (exists) {
            this.LOG.debug(" - Recurso " + resourcePathAndName + " ya existe.");
            this.delete();
        } else {
            this.LOG.debug(" - Recurso " + resourcePathAndName + " no existe.");
        }

        resource = this.getResourceManager().save(data, resourcePathAndName, typeTargetContenido, false, LOCALE_STR);

        if (exists) {
            this.LOG.debug(" - Recurso [" + resourcePathAndName + "] modificado");
        } else {
            this.LOG.debug(" - Recurso [" + resourcePathAndName + "] creado");
        }
        // En este punto tenemos el recurso creado.
        return resource;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.saga.diputacion.huelva.microservicios.controller.IController#validate()
     */
    @Override
    public boolean validate() throws Exception {
        boolean valid = true;
        String enc = this.getRequest().getCharacterEncoding();
        if (!enc.equalsIgnoreCase("UTF-8")) {
            valid = false;
            this.setValid(valid);
            throw new NotValidUseWebServiceMicroservicios("Petición incorrecta, el Encoding es " + enc + " y deberia ser UTF-8");
        } else {
            valid = this.validateTypeIdZonaFromJSON();
            if (valid) {
                // Validamos la petición
                if (!REQ_METHOD_OK.equalsIgnoreCase(this.getRequest().getMethod())) {
                    valid = false;
                    throw new NotValidUseWebServiceMicroservicios("La llamada ha de ser de tipo POST");
                } else {
                    // Validamos que tenemos un JSON
                    if (this.getJsonObject() == null) {
                        valid = false;
                        throw new NotJsonInRequestException("No existe JSON en la peticion");
                    }
                }
                this.setValid(valid);
            }
        }
        return valid;
    }

    /**
     * @return Devuelve <b>true</b> si tenemos Type, Id y Zona válidos o <b>false</b> en caso contrario
     * @return
     * @throws NotValidJsonInRequestException
     * @throws NotPropertiFoundException
     */
    private boolean validateTypeIdZonaFromJSON() throws NotValidJsonInRequestException, NotPropertiFoundException {
        boolean valid = true;
        try {
            if (StringUtils.isEmpty(this.getJsonObject().getString(JSON_TYPE))) {
                valid = false;
                this.setValid(valid);
                throw new NotValidJsonInRequestException("Obligatorio valor en " + JSON_TYPE);
            } else {
                if (StringUtils.isEmpty(this.getJsonObject().getString(JSON_ID)) || ZERO.equals(this.getJsonObject().getString(JSON_ID))) {
                    valid = false;
                    this.setValid(valid);
                    throw new NotValidJsonInRequestException("Obligatorio valor en " + JSON_ID + " mayor de 0");
                } else {
                    if (StringUtils.isEmpty(this.getJsonObject().getString(JSON_ZONA))) {
                        valid = false;
                        this.setValid(valid);
                        throw new NotValidJsonInRequestException("Obligatorio valor en " + JSON_ZONA);
                    } else {
                        if (!this.existZona(this.getJsonObject().getString(JSON_ZONA))
                                        && !this.getJsonObject().getString(JSON_ZONA).equalsIgnoreCase("/")) {
                            valid = false;
                            this.setValid(valid);
                            throw new NotValidJsonInRequestException("No existe el SubSiteMap de zona: " + this.getJsonObject().getString(JSON_ZONA));
                        } else {

                        }
                    }
                }
            }
        } catch (JSONException e) {
            valid = false;
            this.setValid(valid);
            throw new NotValidJsonInRequestException("Los atributos '" + JSON_TYPE + "', '" + JSON_ID + "' y '" + JSON_ZONA + "' son obligatorios");
        }
        return valid;
    }

    /**
     *
     * @param zona
     * @return Devuelve <b>true</b> si existe la zona indicada o <b>false</b> en caso contrario
     * @throws NotPropertiFoundException
     */
    private boolean existZona(String zona) throws NotPropertiFoundException {
        return this.getCmsObjectAdmin().existsResource(this.getTargetSite() + zona);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.saga.diputacion.huelva.microservicios.controller.IController#load()
     */
    @Override
    public void load() throws Exception {
        this.cmsObjectAdmin = CmsWorkplaceAction.getInstance().getCmsAdminObject();
        this.resourceCMS = new ResourceCMS(this.getCmsObjectAdmin());
        this.resourceManager = new ResourcesManager(this.getCmsObjectAdmin());
        this.targetSite = this.loadTargetSite();
        // Cargamos el JSON
        StringBuffer jb = new StringBuffer();
        String line = null;
        try {
            BufferedReader reader = this.getRequest().getReader();
            while ((line = reader.readLine()) != null) {
                jb.append(line);
            }
        } catch (Exception e) {
            throw new NotJsonInRequestException("No existe JSON en la peticion");
        }
        try {
            this.LOG.debug("JSON:\n" + jb.toString());
            this.setJsonObject(new JSONObject(jb.toString()));
        } catch (JSONException e) {
            throw new NotValidJsonInRequestException("Error de parseo de la cadena de la peticion a JSON");
        }
    }

    /**
     * @return Carga en el controlador el site destino indicado en los parámetros del módulo en OpenCms
     * @throws NotPropertiFoundException
     */
    private String loadTargetSite() throws NotPropertiFoundException {
        return OpenCms.getModuleManager().getModule(Configuration.getValue(Configuration.MODULE_NAME)).getParameter(Configuration.SITE_DESTINO);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.saga.diputacion.huelva.microservicios.controller.IController#handleRequest()
     */
    @Override
    public void handleRequest() throws NotPropertiFoundException, Exception {
        this.LOG.info("Microservicios WebService - INIT - " + this.getClass().getSimpleName());
        long init = System.currentTimeMillis();
        int responseCode = 0;
        String responseMsg = "OK";
        try {
            this.load();
            if (this.validate()) {
                this.getResponse().setCharacterEncoding("UTF-8");
                this.getResponse().setContentType("application/json;charset=UTF-8");
                this.execute();
                this.getResponse().setStatus(HttpServletResponse.SC_OK);
                responseCode = HttpServletResponse.SC_OK;
                this.getResponse().getWriter().write(this.getJsonResponseText(HttpServletResponse.SC_OK));
            }
        } catch (NotValidJsonInRequestException | NotJsonInRequestException | NotValidUseWebServiceMicroservicios e) {
            // JSON no valido o inexistente o uso incorrecto del WebService
            this.LOG.error(e);
            this.getResponse().setStatus(HttpServletResponse.SC_BAD_REQUEST);
            responseCode = HttpServletResponse.SC_BAD_REQUEST;
            this.getResponse().getWriter().write(this.getJsonResponseText(HttpServletResponse.SC_BAD_REQUEST, e));
            responseMsg = e.getMessage();
        } catch (Exception e) {
            this.LOG.error(e);
            this.getResponse().setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            responseCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
            this.getResponse().getWriter().write(this.getJsonResponseText(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e));
            responseMsg = e.getMessage();
            e.printStackTrace();
        } finally {
            this.save();
        }
        this.LOG.info("Microservicios WebService - END in " + TimeUtils.calculateTimesFromMilis((System.currentTimeMillis() - init)) + ". Resultado ["
                        + responseCode + ": " + responseMsg + "]");
    }

    /**
     * @param code
     * @return Devuelve el JSON de respuesta del WebService
     */
    private char[] getJsonResponseText(int code) {
        String jsonStr = this.jsonRes.replace(JSON_CODE_RESPONSE_PATTERN, code + "");
        if (this.getJsonObjectResponse() != null) {
            jsonStr = jsonStr.replace(JSON_CONTENT_RESPONSE_PATTERN, this.getJsonObjectResponse().toString());
        } else {
            jsonStr = jsonStr.replace(JSON_CONTENT_RESPONSE_PATTERN, "{}");
        }
        return jsonStr.toCharArray();
    }

    /**
     *
     * @param code
     * @param e
     * @return Devuelve el JSON de respuesta del WebService en caso de tener una excepción
     */
    private char[] getJsonResponseText(int code, Exception e) {
        String msg = e.getMessage();
        String jsonStr = "";
        if (StringUtils.isNotBlank(msg)) {
            jsonStr = this.jsonRes.replace(JSON_CODE_RESPONSE_PATTERN, code + "").replace(JSON_CONTENT_RESPONSE_PATTERN,
                            "\"" + msg.replace("\"", "") + "\"");
        } else {
            jsonStr = this.jsonRes.replace(JSON_CODE_RESPONSE_PATTERN, code + "").replace(JSON_CONTENT_RESPONSE_PATTERN,
                            "\"" + "Error sin determinar" + "\"");
        }
        return jsonStr.toCharArray();
    }

    /**
     *
     * @return Devuelve el path para guardar los recursos en funcion del tipo de recurso a crear
     * @throws Exception
     */
    protected String getTargetPath() throws Exception {
        return this.getTargetPath(this.getJsonObject().getString(JSON_TYPE));
    }

    /**
     *
     * @param type
     * @return Devuelve el <i>path</i> destino
     * @throws Exception
     */
    protected String getTargetPath(String type) throws Exception {
        String key = Configuration.PATH_DESTINO_RECURSOS.replace(Configuration.PATTERN_ID_TIPO_ORIGEN, type);
        return Configuration.getValue(key);
    }

    /**
     *
     * @return Devuelve el path de la galería de imágenes
     * @throws Exception
     */
    protected String getGaleryPathImages() throws Exception {
        return Configuration.getValue(Configuration.GALERIA_IMAGENES_RECURSOS);
    }

    /**
     *
     * @return Devuelve el path de la galería de vídeos
     * @throws Exception
     */
    protected String getGaleryPathVideo() throws Exception {
        return Configuration.getValue(Configuration.GALERIA_VIDEOS_RECURSOS);
    }

    /**
     *
     * @return Devuelve el path de la galería de audios
     * @throws Exception
     */
    protected String getGaleryPathAudio() throws Exception {
        return Configuration.getValue(Configuration.GALERIA_AUDIOS_RECURSOS);
    }

    /**
     *
     * @return Devuelve el path de la galería de documentos
     * @throws Exception
     */
    protected String getGaleryPathDocument() throws Exception {
        return Configuration.getValue(Configuration.GALERIA_DOCUMENTOS_RECURSOS);
    }

    /**
     *
     * @return Devuelve el tipo de recurso a crear en función del tipo origen indicado en el JSON de la petición
     * @throws Exception
     */
    protected String getTargetType() throws Exception {
        return this.getTargetType(this.getJsonObject().getString(JSON_TYPE));
    }

    /**
     *
     * @param type
     * @return Devuelve el tipo de recurso a crear en función del tipo origen indicado
     * @throws Exception
     */
    protected String getTargetType(String type) throws Exception {
        String key = Configuration.RECURSO_DESTINO.replace(Configuration.PATTERN_ID_TIPO_ORIGEN, type);
        return Configuration.getValue(key);
    }

    /**
     *
     * @return Devuelve <b>true</b> si se ha cambiado el proyecto a Offline de forma correcta o <b>false</b> en caso
     *         contrario
     * @throws CmsException
     */
    protected boolean changeProyectOffline() throws CmsException {
        boolean change = false;
        if (this.getCmsObjectAdmin().getRequestContext().getCurrentProject().getName().equals("Online")) {
            this.getCmsObjectAdmin().getRequestContext().setCurrentProject(this.getCmsObjectAdmin().readProject("Offline"));
            change = true;
        }
        return change;
    }

    /**
     *
     * @return Devuelve el nombre del recurso a crear
     * @throws Exception
     */
    protected String getResourceName() throws Exception {
        return this.getJsonObject().getString(JSON_ID) + "-" + this.getTargetType() + ".xml";
    }

    /**
     *
     * @return Devuelve el path absoluto
     * @throws Exception
     */
    protected String getResourcePath() throws Exception {
        String siteTargetConf = this.getTargetSite();
        String zona = this.getJsonObject().getString(JSON_ZONA);
        String pathTargetDPHContenido = this.getTargetPath();
        return (siteTargetConf + zona + pathTargetDPHContenido).replaceAll("///", "/");
    }

    /**
     *
     * @return Devuelve el path absoluto y nombre del recurso a crear
     * @throws Exception
     */
    protected String getResourcePathAndName() throws Exception {
        return this.getResourcePath() + this.getResourceName();
    }

    /**
     *
     * @param id
     * @param zona
     * @param type
     * @return Devuelve el path absoluto y nombre del recurso a crear
     * @throws Exception
     */
    protected String getResourceName(String id, String zona, String type) throws Exception {
        String pathTargetDPHContenido = this.getTargetPath(type);
        String typeTargetContenido = this.getTargetType(type);
        return this.getTargetSite() + zona + pathTargetDPHContenido + id + "-" + typeTargetContenido + ".xml";
    }

    /**
     *
     * @param fileUrl
     * @param fileType
     * @param titleProp
     * @return
     * @throws JSONException
     * @throws ClientProtocolException
     * @throws IOException
     * @throws URISyntaxException
     * @throws Exception
     */
    protected String createFileInOpenCms(String fileUrl, String fileType, String titleProp)
                    throws JSONException, ClientProtocolException, IOException, URISyntaxException, Exception {
        String fileCreated = this.createFileInOpenCms(fileUrl, fileType);
        // Añadimos la propiedad TITLE
        this.addCustomTitleProperty(fileCreated, titleProp);
        return fileCreated;
    }

    /**
     * Sube un archivo a OpenCms publicando este después.
     *
     * @param fileUrl
     * @param fileType
     * @return Devuelve el path y el nombre del archivo subido
     * @throws JSONException
     * @throws ClientProtocolException
     * @throws IOException
     * @throws URISyntaxException
     * @throws Exception
     */
    protected String createFileInOpenCms(String fileUrl, String fileType)
                    throws JSONException, ClientProtocolException, IOException, URISyntaxException, Exception {
        String fileCreated = "";
        if (StringUtils.isNotBlank(fileUrl) && StringUtils.isNotBlank(fileType)) {
            String typeToLog = "";
            String typeToCreate = "";
            String galeryPath = "";
            // Que tipo de archivos vamos a "subir"?
            if (fileType.equalsIgnoreCase(IMAGE_TYPE)) {
                // Imagen
                typeToLog = "Imagen";
                typeToCreate = IMAGE_TYPE;
                galeryPath = this.getGaleryPathImages();
            } else {
                if (fileType.equalsIgnoreCase(VIDEO_TYPE)) {
                    // Video
                    typeToLog = "Video";
                    typeToCreate = BINARY_TYPE;
                    galeryPath = this.getGaleryPathVideo();
                } else {
                    if (fileType.equalsIgnoreCase(AUDIO_TYPE)) {
                        // Audio
                        typeToLog = "Audio";
                        typeToCreate = BINARY_TYPE;
                        galeryPath = this.getGaleryPathAudio();
                    } else {
                        if (fileType.equalsIgnoreCase(BINARY_TYPE)) {
                            // Audio
                            typeToLog = "Documento";
                            typeToCreate = BINARY_TYPE;
                            galeryPath = this.getGaleryPathDocument();
                        } else {

                        }
                    }
                }
            }

            this.LOG.debug(" - " + typeToLog + ": " + fileUrl);
            String file = (this.getTargetSite() + this.getJsonObject().getString(JSON_ZONA) + galeryPath
                            + this.getRelativePathAndNameFromURL(fileUrl)).replaceAll("///", "/").replaceAll("//", "/");
            fileCreated = OpenCms.getResourceManager().getFileTranslator().translateResource(file);
            try {
                if (!this.getCmsObjectAdmin().existsResource(fileCreated)) {
                    try {
                        fileCreated = this.getResourceCMS().uploadFile(fileUrl, fileCreated, typeToCreate, true);
                    } catch (Exception e) {
                        this.LOG.error(" - No se ha podido crear " + typeToLog + " [" + fileCreated + "]");
                        fileCreated = "";
                    }
                    if (StringUtils.isNotBlank(fileCreated)) {
                        this.LOG.debug(" - " + typeToLog + " [" + fileCreated + "] creada");
                        // Si todo ha ido bien, publicamos
                        OpenCms.getPublishManager().publishResource(this.getCmsObjectAdmin(), fileCreated);
                        // Esperamos la publicacion
                        OpenCms.getPublishManager().waitWhileRunning();
                    }
                } else {
                    this.LOG.debug(" - " + typeToLog + " [" + fileCreated + "] Ya existe");
                }
            } catch (Exception e) {
                e.printStackTrace();
                fileCreated = "";
                this.LOG.error("Error creando la " + typeToLog + " [" + fileCreated + "]", e);
                throw new Exception("Error creando la " + typeToLog + " [" + fileCreated + "]", e);
            }
        }
        return fileCreated;
    }

    /**
     *
     * @param url
     * @return Devuelve el path relativo y el nombre del archivo a partir de la URL pública de este
     */
    protected String getRelativePathAndNameFromURL(String url) {
        String res = "";
        if (StringUtils.isNotBlank(url)) {
            if (url.contains(IMAGES_GALLERIES_ORIGIN)) {
                res = url.split(IMAGES_GALLERIES_ORIGIN)[1].replace("//", "/");
            } else {
                if (url.contains(VIDEOS_GALLERIES_ORIGIN)) {
                    res = url.split(VIDEOS_GALLERIES_ORIGIN)[1].replace("//", "/");
                } else {
                    if (url.contains(FICHEROS_GALLERIES_ORIGIN)) {
                        res = url.split(FICHEROS_GALLERIES_ORIGIN)[1].replace("//", "/");
                    } else {
                        if (url.contains(DOCUMENTOS_GALLERIES_ORIGIN)) {
                            res = url.split(DOCUMENTOS_GALLERIES_ORIGIN)[1].replace("//", "/");
                        } else {
                            if (url.contains(AGENDA_GALLERIES_ORIGIN)) {
                                res = url.split(AGENDA_GALLERIES_ORIGIN)[1].replace("//", "/");
                            } else {
                                res = StringUtils.getLastStringFromSplit(url, "/");
                            }
                        }
                    }
                }
            }
        }
        return res;
    }

    /**
     *
     * @param resourceContent
     * @throws JSONException
     * @throws Exception
     * @throws CmsException
     */
    protected void addMunicipios(ResourceContent resourceContent) throws JSONException, Exception, CmsException {
        resourceContent.removeAllValue("Municipio");
        JSONArray municipios = this.getJsonObject().getJSONArray(JSON_MUNICIPIO);
        String pathMunicipios = Configuration.getValue(Configuration.PATH_MUNICIPIOS);
        String municipioDefecto = Configuration.getValue(Configuration.MUNICIPIO_DEFECTO);
        if (municipios.length() > 0) {
            for (int i = 0; i < municipios.length(); i++) {
                String municipio = OpenCms.getResourceManager().getFileTranslator()
                                .translateResource(this.getJsonObject().getJSONArray(JSON_MUNICIPIO).getString(i));
                String categoryMunicipioPath = this.getTargetSite() + pathMunicipios + municipio;
                try {
                    this.getResourceCMS().ensureResource(categoryMunicipioPath, FOLDER_TYPE, true);
                    this.LOG.debug("  > Categoria " + categoryMunicipioPath + " creada");
                } catch (Exception e) {
                    this.LOG.debug("  > La categoria " + categoryMunicipioPath + " ya existe");
                }
                resourceContent.appendCategoryStringValue(JSON_MUNICIPIO, categoryMunicipioPath);
            }
        } else {
            this.LOG.debug(" - No existen Municipios. Indicamos por defecto " + municipioDefecto);
            String huelva = this.getTargetSite() + pathMunicipios + municipioDefecto;
            try {
                this.getResourceCMS().ensureResource(huelva, FOLDER_TYPE, true);
                this.LOG.debug("  > Categoria " + huelva + " creada");
            } catch (Exception e) {
                this.LOG.debug("  > La categoria " + huelva + " ya existe");
            }
            resourceContent.appendCategoryStringValue("Municipio", huelva);
        }
    }

    /**
     *
     * @param resourceContent
     * @throws JSONException
     * @throws Exception
     * @throws CmsException
     */
    protected void addTematicaForZona(ResourceContent resourceContent, List<String> tematicasList) throws JSONException, Exception, CmsException {
        resourceContent.removeAllValue("JSON_TEMATICA");
        if (tematicasList == null) {
            tematicasList = new ArrayList<>();
        }
        String zona = this.getJsonObject().getString(JSON_ZONA);

        String pathTematicas = Configuration.getValue(Configuration.PATH_TEMATICAS);
        String tematicaDefecto = Configuration.getValue(Configuration.TEMATICA_DEFECTO);
        String tematicaDefectoZonaPrincipal = Configuration.getValue(Configuration.TEMATICA_DEFECTO_ZOPA_PRINCIPAL);
        if (StringUtils.isNotBlank(zona)) {

            String categoryZonaPath = "";
            if (zona.equals("/")) {
                this.LOG.debug(" - Añadimos la temática por defecto para la zona principal: " + tematicaDefectoZonaPrincipal);
                categoryZonaPath = this.getTargetSite() + pathTematicas + tematicaDefectoZonaPrincipal;
            } else {
                categoryZonaPath = this.getTargetSite() + pathTematicas + zona;
            }

            try {
                this.getResourceCMS().ensureResource(categoryZonaPath, FOLDER_TYPE, true);
                this.LOG.debug("  > Tematica " + categoryZonaPath + " creada");
            } catch (Exception e) {
                this.LOG.debug("  > La tematica " + categoryZonaPath + " ya existe");
            }
            resourceContent.appendCategoryStringValue(JSON_TEMATICA, categoryZonaPath);
        } else {
            // A este punto NO se llegará ya que la validacion del JSON de entrada falla si no se ha indicado zona
            this.LOG.debug("No se ha indicado zona. Añadimos la tematica por defecto: " + tematicaDefecto);
            String sinTematica = this.getTargetSite() + pathTematicas + tematicaDefecto;
            this.getResourceCMS().ensureResource(sinTematica, FOLDER_TYPE, true);
            resourceContent.appendCategoryStringValue("Tematica", sinTematica);
        }

        // Añadimos las tematicas adicionales
        for (String tematica : tematicasList) {
            String pathTematica = OpenCms.getResourceManager().getFileTranslator().translateResource(this.getTargetSite() + pathTematicas + tematica);

            try {
                this.getResourceCMS().createFolder(pathTematica);
                this.LOG.debug("  > Tematica " + pathTematica + " creada");
            } catch (Exception e) {
                this.LOG.debug("  > La tematica " + pathTematica + " ya existe");
            }
            resourceContent.appendCategoryStringValue(JSON_TEMATICA, pathTematica);
        }
    }

    /**
     *
     * @param siteTargetConf
     * @param resourceContent
     * @throws Exception
     */
    protected void addRelatedContent(ResourceContent resourceContent) throws Exception {
        resourceContent.removeAllValue("RelatedContent");
        JSONObject jsonRelacionados = this.getJsonObject().getJSONObject(JSON_RELACIONADOS);
        if (jsonRelacionados.length() > 0) {
            this.LOG.debug(" - Existen " + jsonRelacionados.length() + " contenidos relacionados");
            JSONArray contentOriginIds = jsonRelacionados.names();
            for (int i = 0; i < contentOriginIds.length(); i++) {
                String contentId = contentOriginIds.getString(i);
                String zona = jsonRelacionados.getJSONArray(contentId).getString(0);
                String type = jsonRelacionados.getJSONArray(contentId).getString(1);
                String pathTarget = Configuration.getValue(Configuration.PATH_DESTINO_RECURSOS.replace(Configuration.PATTERN_ID_TIPO_ORIGEN, type));
                String typeTarget = Configuration.getValue(Configuration.RECURSO_DESTINO.replace(Configuration.PATTERN_ID_TIPO_ORIGEN, type));

                String resourceFileNameRelated = this.getTargetSite() + zona + pathTarget + contentId + "-" + typeTarget + ".xml";
                if (this.getCmsObjectAdmin().existsResource(resourceFileNameRelated)) {
                    try {
                        resourceContent.appendStringValue(JSON_RELATED_CONTENT, resourceFileNameRelated);
                        this.LOG.debug("  > Contenido: " + contentId + " - Tipo: " + type + " - Zona: " + zona + " - [" + resourceFileNameRelated
                                        + "]");
                    } catch (Exception e) {
                        this.LOG.error("No se ha asignado el recurso [" + resourceFileNameRelated + "] como relacionado debido al error ("
                                        + e.getMessage() + ").");
                        e.printStackTrace();
                    }
                } else {
                    this.LOG.error(" - No existe el recurso: " + (resourceFileNameRelated));
                }
            }
        } else {
            this.LOG.debug(" - No existen contenidos relacionados");
        }
    }

    /**
     *
     * @param url
     * @return Devuelve el nombre de un archivo a partir de su URL
     */
    protected String getNameFromURL(String url) {
        String res = StringUtils.getLastStringFromSplit(url, "/");
        if (StringUtils.isNotBlank(res)) {
            return res;
        } else {
            return "img_" + System.currentTimeMillis() + ".png";
        }
    }

    /**
     * Lee un recurso
     *
     * @throws Exception
     */
    protected void read() throws Exception {
        String resourceFileName = this.getResourcePathAndName();
        CmsResource resource = this.getCmsObjectAdmin().readResource(resourceFileName, CmsResourceFilter.IGNORE_EXPIRATION);
        if (resource != null) {
            this.LOG.debug(" - Leemos el recurso [" + resourceFileName + "]");
            ResourceContent resourceContent = new ResourceContent(this.getCmsObjectAdmin(), resource, LOCALE_STR);
            this.setJsonObjectResponse(XML.toJSONObject(resourceContent.getXmlContent().toString()));
        } else {
            this.LOG.error(" - No existe el recurso indicado [" + resourceFileName + "]");
            throw new Exception("No existe el recurso indicado [" + resourceFileName + "]");
        }
    }

    protected void delete(String resourceFileName) throws Exception {
        CmsResource resource = this.getCmsObjectAdmin().readResource(resourceFileName, CmsResourceFilter.IGNORE_EXPIRATION);
        if (resource != null) {
            this.LOG.debug(" - Borramos el recurso [" + resourceFileName + "]");
            ResourceContent resourceContent = new ResourceContent(this.getCmsObjectAdmin(), resource, LOCALE_STR);
            String jsonResponseContentStr = resourceContent.getXmlContent().toString();
            try {
                this.changeProyectOffline();
                this.getCmsObjectAdmin().lockResource(resource);
                this.getCmsObjectAdmin().deleteResource(resource, null);
                this.getCmsObjectAdmin().unlockResource(resource);

                // Si todo ha ido bien, publicamos
                OpenCms.getPublishManager().publishResource(this.getCmsObjectAdmin(), resourceFileName);
                // Esperamos la publicacion
                OpenCms.getPublishManager().waitWhileRunning();
            } catch (CmsException e) {
                this.LOG.error(" - No se ha podido borrar el recurso [" + resourceFileName + "]", e);
                throw new Exception("No se ha podido borrar el recurso [" + resourceFileName + "]", e);
            }
            this.setJsonObjectResponse(XML.toJSONObject(jsonResponseContentStr));
        } else {
            this.LOG.debug(" - No existe el recurso indicado");
        }
    }

    /**
     * Borra un recurso
     *
     * @throws Exception
     */
    protected void delete() throws Exception {
        String resourceFileName = this.getResourcePathAndName();
        this.delete(resourceFileName);
    }

    /**
     * Repara las referencias a las imágenes que están en el texto de tipo HTML indicado. Por cada imagen, se descarga
     * esta, se añade a OpenCms y se sustituye la ruta en el HTML original
     *
     * @param html
     * @return Texto HTML con las rutas a las imágenes modificadas, indicnado rutas relativas a OpenCms
     * @throws JSONException
     * @throws Exception
     */
    protected String repairSrcInHtml(String html) throws JSONException, Exception {
        StringTokenizer st = new StringTokenizer(html.trim(), "\"", true);
        if (st.countTokens() == 1) {
            st = new StringTokenizer(html.trim(), "'", true);
        }
        String res = "";
        while (st.hasMoreElements()) {
            String token = st.nextElement().toString();
            res += token;
            if (token.contains(SRC) || token.contains(HREF)) {
                res += st.nextElement().toString();
                res += this.manageSrcHref(st.nextElement().toString());
            }
        }
        return res;
    }

    protected String manageSrcHref(String href)
                    throws MalformedURLException, ClientProtocolException, JSONException, IOException, URISyntaxException, Exception {
        System.out.println("href: " + href);
        if (href.startsWith("/")) {
            href = HOST_DPH_WITH_HTTP + href;
        }

        if (StringUtils.isNotBlank(href) && (new URL(href)).getHost().equalsIgnoreCase(HOST_DPH)) {
            if (this.isImg(href)) {
                return this.createFileInOpenCms(href, IMAGE_TYPE);
            } else {
                if (this.isDoc(href)) {
                    return this.createFileInOpenCms(href, BINARY_TYPE);
                } else {
                    if (this.isVideo(href)) {
                        return this.createFileInOpenCms(href, VIDEO_TYPE);
                    } else {
                        if (this.isAudio(href)) {
                            return this.createFileInOpenCms(href, AUDIO_TYPE);
                        }
                    }
                }
            }
        }
        return href;
    }

    /**
     *
     * @param href
     * @return
     */
    protected boolean isAudio(String href) {
        return (StringUtils.isNotBlank(href) && StringUtils.endsWithAnyIgnoreCase(href, EXT_AUDIO));
    }

    /**
     *
     * @param href
     * @return
     */
    protected boolean isVideo(String href) {
        return (StringUtils.isNotBlank(href) && StringUtils.endsWithAnyIgnoreCase(href, EXT_VIDEOS));
    }

    /**
     * @param href
     * @return
     */
    protected boolean isDoc(String href) {
        return (StringUtils.isNotBlank(href) && StringUtils.endsWithAnyIgnoreCase(href, EXT_DOC));
    }

    /**
     * @param href
     * @return
     */
    protected boolean isImg(String href) {
        return (StringUtils.isNotBlank(href) && StringUtils.endsWithAnyIgnoreCase(href, EXT_IMG));
    }

    /**
     *
     * @throws ClientProtocolException
     * @throws IOException
     * @throws URISyntaxException
     * @throws Exception
     */
    protected void addLinksAndDocuments() throws ClientProtocolException, IOException, URISyntaxException, Exception {
        this.getJsonObject().getJSONObject(JSON_DATA).put(JSON_CONTENT, new JSONObject("{\"" + JSON_GALLERY + "\":" + this.getGalleryJson() + "}"));
    }

    /**
     *
     * @return Devuelve una cadena de texto con el JSON de "LinksGallery"
     * @throws ClientProtocolException
     * @throws IOException
     * @throws URISyntaxException
     * @throws Exception
     */
    private String getGalleryJson() throws ClientProtocolException, IOException, URISyntaxException, Exception {
        return new StringBuffer().append("{\"" + JSON_LINKS_GALLERY + "\": {\"" + JSON_OPTION + "\": {").append(this.getJsonDocumentsStr())
                        .append(",").append(this.getJsonLinksStr()).append("}}}").toString();
    }

    /**
     *
     * @return Devuelve una cadena de texto con al JSON "Link" con los enlaces
     * @throws JSONException
     */
    private String getJsonLinksStr() throws JSONException {
        // Recuperamos el array de enlaces
        JSONArray enlaces = this.getJsonObject().getJSONObject(JSON_DATA).getJSONArray(JSON_ENLACES);
        // Una vez que los tenemos los eliminamos del JSON
        this.getJsonObject().getJSONObject(JSON_DATA).remove(JSON_ENLACES);
        String jsonTemplate = "[";
        for (int i = 0; i < enlaces.length(); i++) {
            JSONObject enlace = (JSONObject) enlaces.get(i);
            String href = enlace.getString(JSON_URL);
            String title = enlace.getString(JSON_NOMBRE);
            jsonTemplate += "{\"" + JSON_HREF + "\":\"" + href + "\",\"" + JSON_TITLE + "\":\"" + title + "\"}";
            if (enlaces.length() > 1 && i + 1 != enlaces.length()) {
                jsonTemplate += ",";
            }
        }
        jsonTemplate += "]";
        return new StringBuffer().append("\"").append(JSON_LINK).append("\":").append(jsonTemplate).toString();
    }

    /**
     *
     * @return Devuelve una cadena de texto con el JSON "Attachment" con los documentos del recurso
     * @throws ClientProtocolException
     * @throws IOException
     * @throws URISyntaxException
     * @throws Exception
     */
    private String getJsonDocumentsStr() throws ClientProtocolException, IOException, URISyntaxException, Exception {
        // Recuperamos el array de documentos
        JSONArray documentos = this.getJsonObject().getJSONObject(JSON_DATA).getJSONArray(JSON_DOCUMENTOS);
        // Una vez que los tenemos los eliminamos del JSON
        this.getJsonObject().getJSONObject(JSON_DATA).remove(JSON_DOCUMENTOS);
        String jsonTemplate = "[";
        for (int i = 0; i < documentos.length(); i++) {
            String path = this.createFileInOpenCms(documentos.getString(i), BINARY_TYPE);
            jsonTemplate += "{\"" + JSON_FILE + "\":\"" + path + "\",";
            jsonTemplate += "\"" + JSON_TITLE + "\":\"" + "Doc. " + i + "\"}";
            if (documentos.length() > 1 && i + 1 != documentos.length()) {
                jsonTemplate += ",";
            }
        }
        jsonTemplate += "]";
        return new StringBuffer().append("\"").append(JSON_ATTACHMENT).append("\":").append(jsonTemplate).toString();
    }

    /**
     *
     * @param resourceFileName
     * @param title
     * @throws Exception
     */
    protected void addCustomTitleProperty(String resourceFileName, String title) throws Exception {
        if (StringUtils.isNotBlank(title)) {
            this.getResourceCMS().lock(resourceFileName);
            CmsProperty prop = new CmsProperty(JSON_TITLE, title, null, true);
            this.getCmsObjectAdmin().writePropertyObject(resourceFileName, prop);
            this.getResourceCMS().unlock(resourceFileName);
            this.LOG.debug(" - Añadido [" + title + "] como propiedad de TITLE");
        } else {
            this.addTitleProperty(resourceFileName);
        }
    }

    /**
     * @param resourceFileName
     * @throws Exception
     */
    protected void addTitleProperty(String resourceFileName) throws Exception {
        String title = this.getJsonObject().getJSONObject(JSON_DATA).getString(JSON_TITLE);
        if (StringUtils.isEmpty(title)) {
            title = StringUtils.getLastStringFromSplit(resourceFileName, "/");
        }
        this.addCustomTitleProperty(resourceFileName, title);
    }

    /**
     * @return jsonObject
     */
    public JSONObject getJsonObject() {
        return this.jsonObject;
    }

    /**
     * @param jsonObject the jsonObject to set
     */
    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    /**
     * @return isValid
     */
    public boolean isValid() {
        return this.isValid;
    }

    /**
     * @param isValid the isValid to set
     */
    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }

    /**
     * @return cmsObjAdmin
     */
    public CmsObject getCmsObjectAdmin() {
        return this.cmsObjectAdmin;
    }

    /**
     * @param cmsObjAdmin the cmsObjAdmin to set
     */
    public void setCmsObjectAdmin(CmsObject cmsObjAdmin) {
        this.cmsObjectAdmin = cmsObjAdmin;
    }

    /**
     * @return resourceCMS
     */
    public ResourceCMS getResourceCMS() {
        return this.resourceCMS;
    }

    /**
     * @param resourceCMS the resourceCMS to set
     */
    public void setResourceCMS(ResourceCMS resourceCMS) {
        this.resourceCMS = resourceCMS;
    }

    /**
     * @return resourceManager
     */
    public ResourcesManager getResourceManager() {
        return this.resourceManager;
    }

    /**
     * @param resourceManager the resourceManager to set
     */
    public void setResourceManager(ResourcesManager resourceManager) {
        this.resourceManager = resourceManager;
    }

    /**
     * @return jsonObjectResponse
     */
    public JSONObject getJsonObjectResponse() {
        return this.jsonObjectResponse;
    }

    /**
     * @param jsonObjectResponse the jsonObjectResponse to set
     */
    public void setJsonObjectResponse(JSONObject jsonObjectResponse) {
        this.jsonObjectResponse = jsonObjectResponse;
    }

    /**
     * @return targetSite
     */
    public String getTargetSite() {
        return this.targetSite;
    }

    /**
     * @param targetSite the targetSite to set
     */
    public void setTargetSite(String targetSite) {
        this.targetSite = targetSite;
    }

}