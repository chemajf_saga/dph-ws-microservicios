/**
 * Package com.saga.diputacion.huelva.microservicios.util
 * File JSONUtils.java
 * Created at 9 may. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.util;

import java.util.ArrayList;
import java.util.List;

import org.opencms.json.JSONArray;
import org.opencms.json.JSONException;
import org.opencms.json.JSONObject;

/**
 * @author chema.jimenez
 * @since 9 may. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class JSONUtils {

    private JSONObject jsonObject;
    private JSONArray jsonArray;

    /**
     *
     */
    public JSONUtils() {
    }

    /**
     *
     * @param jsonObject
     */
    public JSONUtils(JSONObject jsonObject) {
        super();
        this.jsonObject = jsonObject;
    }

    /**
     *
     * @param jsonArray
     */
    public JSONUtils(JSONArray jsonArray) {
        super();
        this.jsonArray = jsonArray;
    }

    /**
     *
     * @param jsonObject
     * @param jsonArray
     */
    public JSONUtils(JSONObject jsonObject, JSONArray jsonArray) {
        super();
        this.jsonObject = jsonObject;
        this.jsonArray = jsonArray;
    }

    public List<String> toList() {
        List<String> res = new ArrayList<>();
        if (this.jsonArray != null) {
            for (int i = 0; i < this.jsonArray.length(); i++) {
                try {
                    res.add(this.jsonArray.get(i).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return res;
    }

    /**
     * @return jsonObject
     */
    public JSONObject getJsonObject() {
        return this.jsonObject;
    }

    /**
     * @param jsonObject the jsonObject to set
     */
    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

}
