/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphgaleria
 * File DPHGaleriaControllerRead.java
 * Created at 2 may. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.view.dphgaleria;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

/**
 * @author chema.jimenez
 * @since 2 may. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class DPHGaleriaControllerRead extends DPHGaleriaController {

    /**
     * @param context
     * @param req
     * @param res
     */
    public DPHGaleriaControllerRead(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.saga.diputacion.huelva.microservicios.controller.Controller#execute()
     */
    @Override
    public void execute() throws Exception {
        this.read();
    }

}
