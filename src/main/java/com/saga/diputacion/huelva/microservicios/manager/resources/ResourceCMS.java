/**
 * Package com.saga.diputacion.huelva.microservicios.manager
 * File ResourceCMS.java
 * Created at 17 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.manager.resources;

import static com.saga.diputacion.huelva.microservicios.config.Constants.BINARY_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.DOWNLOAD_GALLERY_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.FOLDER_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.GALLERIES;
import static com.saga.diputacion.huelva.microservicios.config.Constants.HOST_DPH;
import static com.saga.diputacion.huelva.microservicios.config.Constants.HTTP;
import static com.saga.diputacion.huelva.microservicios.config.Constants.IMAGE_GALLERY_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.IMAGE_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.LINK_GALLERY_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.POINTER_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.WWW;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.util.EntityUtils;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsProperty;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.file.types.I_CmsResourceType;
import org.opencms.flex.CmsFlexController;
import org.opencms.loader.CmsLoaderException;
import org.opencms.lock.CmsLock;
import org.opencms.main.CmsException;
import org.opencms.main.OpenCms;
import org.opencms.security.I_CmsPrincipal;
import org.opencms.staticexport.CmsLinkManager;
import org.opencms.util.CmsStringUtil;

import com.saga.diputacion.huelva.microservicios.exception.BadRequestException;

/**
 * @author chema.jimenez
 * @since 17 abr. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class ResourceCMS {

    CmsObject cmso;

    public ResourceCMS(CmsObject cmso) {
        this.cmso = cmso;
    }

    /**
     * Genera una recurso del tipo dado
     *
     * @param path Ruta del nuevo recurso
     * @param type Tipo del recurso
     * @return Objeto {@link ResourceCMS}
     */
    public ResourceCMS createResource(String path, String type) throws CmsException {
        this.cmso.createResource(path, OpenCms.getResourceManager().getResourceType(type));
        this.unlock(path);
        return this;
    }

    /**
     * Genera una recurso del tipo dado publicando si así se indica en <b>publish</b>
     *
     * @param path
     * @param type
     * @param publish
     * @return Objeto {@link ResourceCMS}
     * @throws Exception
     */
    public ResourceCMS createResource(String path, String type, boolean publish) throws Exception {
        this.createResource(path, type);
        if (publish) {
            OpenCms.getPublishManager().publishResource(this.cmso, path);
            OpenCms.getPublishManager().waitWhileRunning();
        }

        return this;
    }

    /**
     * Asegura la existencia de un recurso del tipo dado y de sus carpetas padres.
     *
     * @param path Ruta del nuevo recurso
     * @param type Tipo del recurso
     * @return Objeto {@link ResourceCMS}
     * @throws Exception
     */
    public ResourceCMS ensureResource(String path, String type, boolean publish) throws Exception {
        if (!this.cmso.existsResource(path)) {
            String parentFolder = CmsResource.getParentFolder(path);
            this.ensureResource(parentFolder, FOLDER_TYPE, publish);
            this.createResource(path, type, publish);
        }
        return this;
    }

    /**
     * Genera una carpeta
     *
     * @param path Ruta de la carpeta
     * @return Objeto {@link ResourceCMS}
     */
    public ResourceCMS createFolder(String path) throws CmsException {
        this.createResource(path, FOLDER_TYPE);
        return this;
    }

    /**
     * Genera una galeria de imagenes
     *
     * @param path Ruta de la galeria de imagenes
     * @return Objeto {@link ResourceCMS}
     */
    public ResourceCMS createImageGallery(String path) throws CmsException {
        this.createResource(path, IMAGE_GALLERY_TYPE);
        return this;
    }

    /**
     * Genera una galeria de descargas
     *
     * @param path Ruta de la galeria de descargas
     * @return Objeto {@link ResourceCMS}
     */
    public ResourceCMS createDownloadGallery(String path) throws CmsException {
        this.createResource(path, DOWNLOAD_GALLERY_TYPE);
        return this;
    }

    /**
     * Genera una galeria de enlaces
     *
     * @param path Ruta de la galeria de enlaces
     * @return Objeto {@link ResourceCMS}
     */
    public ResourceCMS createLinkGallery(String path) throws CmsException {
        this.createResource(path, LINK_GALLERY_TYPE);
        return this;
    }

    /**
     * Unlock resource. If resource is lock by inheritance try to unlock parent folder. If lock owns to another user
     * steal lock and unlock.
     *
     * @param path
     * @return Objeto {@link ResourceCMS}
     */
    public ResourceCMS unlock(String path) throws CmsException {
        CmsLock lock = this.cmso.getLock(path);
        if (lock.isInherited()) {
            this.unlock(CmsResource.getParentFolder(path));
        } else
            if (!lock.isUnlocked()) {
                if (!lock.isOwnedBy(this.cmso.getRequestContext().getCurrentUser())) {
                    this.cmso.changeLock(path);
                }
                this.cmso.unlockResource(path);
            }
        return this;
    }

    /**
     *
     * @param cmso
     * @param path
     * @throws CmsException
     */
    public static void unlock(CmsObject cmso, String path) throws CmsException {
        CmsLock lock = cmso.getLock(path);
        if (lock.isInherited()) {
            unlock(cmso, CmsResource.getParentFolder(path));
        } else
            if (!lock.isUnlocked()) {
                if (!lock.isOwnedBy(cmso.getRequestContext().getCurrentUser())) {
                    cmso.changeLock(path);
                }
                cmso.unlockResource(path);
            }
    }

    /**
     * Lock resource if it is possible. If lock owns to another user steal lock.
     *
     * @param path
     * @return Objeto {@link ResourceCMS}
     */
    public ResourceCMS lock(String path) throws CmsException {
        CmsLock lock = this.cmso.getLock(path);
        if (!lock.isUnlocked() && !lock.isOwnedBy(this.cmso.getRequestContext().getCurrentUser())) {
            this.cmso.changeLock(path);
        }
        this.cmso.lockResource(path);
        return this;
    }

    /**
     * Lock resource if it is possible. If lock owns to another user steal lock
     *
     * @param cmso
     * @param path
     * @throws CmsException
     */
    public static void lock(CmsObject cmso, String path) throws CmsException {
        CmsLock lock = cmso.getLock(path);
        if (!lock.isUnlocked() && !lock.isOwnedBy(cmso.getRequestContext().getCurrentUser())) {
            cmso.changeLock(path);
        }
        cmso.lockResource(path);
    }

    /**
     * Dada una carpeta contenedora y un patron formado por el nombre y un índice, genera la ruta en la que se debería
     * crear el siguiente recurso.
     *
     * @param folder
     * @param pattern
     * @return {@link String} con el path del siguiente recurso
     */
    public String nextResoucePath(String folder, String pattern) throws CmsException {
        String namePattern = CmsStringUtil.joinPaths(folder, pattern);
        String newResPath = OpenCms.getResourceManager().getNameGenerator().getNewFileName(this.cmso, namePattern, 5);
        return newResPath;
    }

    /**
     * Set expire date
     *
     * @param path
     * @param dateExp
     * @return Objeto {@link ResourceCMS}
     */
    public ResourceCMS setDateExpire(String path, Long dateExp) throws CmsException {
        this.lock(path);
        this.cmso.setDateExpired(path, dateExp, true);
        this.unlock(path);
        return this;
    }

    /**
     * Check if resource is expired
     *
     * @param path
     * @param dateExp
     * @return Objeto {@link ResourceCMS}
     */
    public boolean isExpired(String path, Date dateExp) throws CmsException {
        return this.cmso.readResource(path, CmsResourceFilter.ALL).isExpired(dateExp.getTime());
    }

    /**
     * Check resource type
     *
     * @param path
     * @return <b>True</b> si el recurso el del tipo indicado o <b>false</b> en caso contrario
     */
    public boolean isType(String path, I_CmsResourceType type) throws CmsException {
        CmsResource resource = this.cmso.readResource(path, CmsResourceFilter.ALL);
        I_CmsResourceType rType = OpenCms.getResourceManager().getResourceType(resource);
        return rType.isIdentical(type);
    }

    /**
     * Copy resource
     *
     * @param pathFrom
     * @param pathTo
     * @return Objeto {@link ResourceCMS}
     */
    public ResourceCMS copyResource(String pathFrom, String pathTo) throws CmsException {

        // Comprobamos si ya existe
        if (this.cmso.existsResource(pathTo)) {
            throw new IllegalArgumentException("ERROR: resource $pathTo already exists");
        }

        // Copiamos el recurso
        this.lock(pathFrom);
        this.cmso.copyResource(pathFrom, pathTo, CmsResource.CmsResourceCopyMode.valueOf(1));
        this.unlock(pathFrom);
        this.unlock(pathTo);

        return this;
    }

    /**
     * Generamos un nombre válido para un grupo dada su ou y su nombre. La ou no puede empezar y debe terminar con slash
     * "/". El nombre no puede empezar ni terminar con slash "/".
     *
     * @param ou
     * @param groupname
     * @return
     */
    public String generateGroupName(String ou, String groupname) {
        String group = ou;
        if (group.startsWith("/")) {
            group = group.substring(1);
        }
        if (!group.endsWith("/")) {
            group = group + "/";
        }
        return group + groupname;
    }

    /**
     * Crea grupo
     *
     * @param groupNameWithOU
     * @param description
     * @param isEnabled
     * @param parentGroupName
     * @return Objeto {@link ResourceCMS}
     * @throws CmsException
     */
    public ResourceCMS createGroup(String groupNameWithOU, String description, boolean isEnabled, String parentGroupName) throws CmsException {
        int flag = I_CmsPrincipal.FLAG_DISABLED;
        if (isEnabled) {
            flag = I_CmsPrincipal.FLAG_ENABLED;
        }
        this.cmso.createGroup(groupNameWithOU, description, flag, parentGroupName);
        return this;
    }

    /**
     * Create absolute link from relative uri
     *
     * @param req
     * @param uri
     * @return
     */
    public static String link(HttpServletRequest req, String uri) {
        CmsFlexController controller = CmsFlexController.getController(req);
        // be sure the link is absolute
        String target = CmsLinkManager.getAbsoluteUri(uri, controller.getCurrentRequest().getElementUri());
        CmsObject cms = controller.getCmsObject();

        // generate the link
        return OpenCms.getLinkManager().substituteLinkForUnknownTarget(cms, target);
    }

    /**
     * Performs a touch operation for a single resource.<p>
     *
     * @param resourceName the resource name of the resource to touch
     * @param recursive the flag if the touch operation is recursive
     *
     * @throws CmsException if touching the resource fails
     */
    protected void touch(String resourceName, boolean recursive) throws CmsException {

        // lock resource
        this.lock(resourceName);

        // if file rewrite
        CmsResource sourceRes = this.cmso.readResource(resourceName, CmsResourceFilter.ALL);
        if (sourceRes.isFile()) {
            this.rewrite(sourceRes);
        } else
            if (recursive) {

                // if recursive rewrite all sub files
                Iterator<CmsResource> it = this.cmso.readResources(resourceName, CmsResourceFilter.ALL, true).iterator();
                while (it.hasNext()) {
                    CmsResource subRes = it.next();
                    if (subRes.isFile()) {
                        this.rewrite(subRes);
                    }
                }
            }
    }

    /**
     * Rewrites the content of the given file.<p>
     *
     * @param resource the resource to rewrite the content for
     *
     * @throws CmsException if something goes wrong
     */
    private void rewrite(CmsResource resource) throws CmsException {

        CmsFile file = this.cmso.readFile(resource);
        file.setContents(file.getContents());
        this.cmso.writeFile(file);
    }

    // /**
    // * Performs the correction of the XML content resources according to their XML schema definition.<p>
    // *
    // * @param folderPath
    // * @param resourceType
    // * @param isIncludeSubFolders
    // * @param isForce
    // * @throws CmsException if reading the list of resources to repair fails
    // */
    // private void repairXmlContents(String folderPath, String resourceType, boolean isIncludeSubFolders, boolean
    // isForce) throws CmsException {
    //
    // // set the resource filter to filter XML contents of the selected type
    // CmsResourceFilter filter =
    // CmsResourceFilter.IGNORE_EXPIRATION.addRequireType(OpenCms.getResourceManager().getResourceType(resourceType));
    // String path = CmsResource.getFolderPath(folderPath);
    // // get the list of resources to check
    // List<CmsResource> resources = this.cmso.readResources(path, filter, isIncludeSubFolders);
    //
    // // create an entity resolver to use
    // CmsXmlEntityResolver resolver = new CmsXmlEntityResolver(this.cmso);
    //
    // // iterate the resources
    // Iterator<CmsResource> i = resources.iterator();
    // while (i.hasNext()) {
    //
    // CmsResource res = i.next();
    //
    // // get the file contents
    // CmsFile file = this.cmso.readFile(res);
    // // get the XML content
    // CmsXmlContent xmlContent = CmsXmlContentFactory.unmarshal(this.cmso, file);
    //
    // // check the XML structure
    // boolean fixFile = isForce;
    // if (!fixFile) {
    // try {
    // xmlContent.validateXmlStructure(resolver);
    // } catch (CmsXmlException e) {
    // // XML structure is not valid, this file has to be fixed
    // fixFile = true;
    // }
    // }
    // if (fixFile) {
    //
    // // check the lock state of the file to repair
    // CmsLock lock = this.cmso.getLock(res);
    // boolean isLocked = false;
    // boolean canWrite = false;
    // if (lock.isNullLock()) {
    // // file is not locked, lock it
    // this.cmso.lockResource(this.cmso.getSitePath(res));
    // isLocked = true;
    // canWrite = true;
    // } else
    // if (lock.isOwnedBy(this.cmso.getRequestContext().getCurrentUser())) {
    // // file is locked by current user
    // canWrite = true;
    // }
    //
    // if (canWrite) {
    // // enable "auto correction mode" - this is required or the XML structure will not be fully corrected
    // xmlContent.setAutoCorrectionEnabled(true);
    // // now correct the XML
    // xmlContent.correctXmlStructure(this.cmso);
    // file.setContents(xmlContent.marshal());
    // // write the corrected file
    // this.cmso.writeFile(file);
    // }
    //
    // if (isLocked) {
    // // unlock previously locked resource
    // this.cmso.unlockResource(this.cmso.getSitePath(res));
    // }
    // }
    // }
    // }

    /**
     *
     * @param url
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     * @throws URISyntaxException
     * @throws BadRequestException
     */
    public byte[] downloadFile(String url) throws ClientProtocolException, IOException, URISyntaxException, BadRequestException {
        byte[] f = null;
        // url = !url.contains("http://") ? "http://" + url : url;
        if (url.startsWith(WWW)) {
            url = HTTP + url;
        } else {
            if (url.startsWith("/")) {
                url = HTTP + HOST_DPH + url;
            }
        }
        CloseableHttpClient httpclient = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
        CloseableHttpResponse response = httpclient.execute(new HttpGet(new URIBuilder(url).build()));
        int code = response.getStatusLine().getStatusCode();
        if (code == HttpServletResponse.SC_BAD_REQUEST || code == HttpServletResponse.SC_NOT_FOUND) {
            throw new BadRequestException("URL [" + url + "] incorrecta");
        }
        if (code == HttpServletResponse.SC_OK) {
            f = EntityUtils.toByteArray(response.getEntity());
        }
        httpclient.close();
        return f;

    }

    /**
     *
     * @param url
     * @param path
     * @param type
     * @return
     * @throws Exception
     */
    public String uploadFile(String url, String path, String type, boolean publish) throws Exception {
        CmsResource res = null;

        // Download file bytes array
        byte[] file = this.downloadFile(url);

        // Create resource
        if (file != null && file.length > 0) {
            if (!this.cmso.existsResource(path)) {
                String parentFolder = CmsResource.getFolderPath(path);
                if (this.isInGalleryResource(path)) {
                    String inGalleryFolderType = this.getInGalleryFolderType(type);
                    if (StringUtils.isNotBlank(inGalleryFolderType)) {
                        this.ensureGalleryFolder(parentFolder, inGalleryFolderType, publish);
                    }
                } else {
                    this.ensureResource(parentFolder, FOLDER_TYPE, publish);
                }

                I_CmsResourceType resType = this.resType(type);
                res = this.cmso.createResource(path, resType, file, new ArrayList<CmsProperty>(0));
                this.cmso.unlockResource(res);
            }
        }

        return path;
    }

    /**
     * Check if resource is into gallery
     *
     * @param path
     * @return
     */
    boolean isInGalleryResource(String path) {
        if (path.contains(GALLERIES)) {
            return true;
        }
        return false;
    }

    /**
     * Return gallery type for resource type in gallery
     *
     * @param type
     * @return
     */
    String getInGalleryFolderType(String type) {
        if (type.equals(IMAGE_TYPE)) {
            return IMAGE_GALLERY_TYPE;
        } else
            if (type.equals(BINARY_TYPE)) {
                return DOWNLOAD_GALLERY_TYPE;
            } else
                if (type.equals(POINTER_TYPE)) {
                    return LINK_GALLERY_TYPE;
                } else {
                    return null;
                }
    }

    /**
     * Ensure gallery folder and parent folders exist. Force unlock.
     *
     * @param path Resource path to ensure
     * @param galleryType Resource type
     * @return Objeto {@link ResourceCMS}
     * @throws Exception
     */
    public ResourceCMS ensureGalleryFolder(String path, String galleryType, boolean publish) throws Exception {
        if (this.cmso.existsResource(path)) {

            // Ensure folder type
            if (!this.isType(path, galleryType)) {
                this.changeType(path, galleryType);
            }
        } else {

            // Ensure parent folder
            String parentFolder = CmsResource.getParentFolder(path);
            if (this.isInGalleryResource(parentFolder)) {
                this.ensureGalleryFolder(parentFolder, galleryType, publish);
            } else {
                this.ensureResource(parentFolder, FOLDER_TYPE, publish);
            }

            // Create gallery folder
            this.createResource(path, galleryType, publish);
        }
        return this;
    }

    /**
     * Check resource type
     *
     * @param path
     * @param type
     * @return Objeto {@link ResourceCMS}
     * @throws CmsException
     */
    boolean isType(String path, String type) throws CmsException {
        I_CmsResourceType resType = OpenCms.getResourceManager().getResourceType(type);
        return this.isType(path, resType);
    }

    /**
     * Change resource type
     *
     * @param path
     * @param type
     * @return Objeto {@link ResourceCMS}
     * @throws CmsException
     */
    private ResourceCMS changeType(String path, I_CmsResourceType type) throws CmsException {
        this.lock(path);
        this.cmso.chtype(path, type);
        this.unlock(path);
        return this;
    }

    /**
     * Change resource type
     *
     * @param path
     * @param type
     * @return Objeto {@link ResourceCMS}
     * @throws CmsException
     */
    private ResourceCMS changeType(String path, String type) throws CmsException {
        I_CmsResourceType resType = OpenCms.getResourceManager().getResourceType(type);
        this.changeType(path, resType);
        return this;
    }

    /**
     * Return resource type
     *
     * @param type
     * @return
     * @throws CmsLoaderException
     */
    private I_CmsResourceType resType(String type) throws CmsLoaderException {
        I_CmsResourceType resType = OpenCms.getResourceManager().getResourceType(type);
        return resType;
    }
}
