/**
 * Package com.saga.diputacion.huelva.microservicios.dphcontenido
 * File package-info.java
 * Created by chema.jimenez
 */
/**
 * @author chema.jimenez
 * @since 8 may. 2018
 *
 *        <pre>
 * Este paquete contiene las clases para gestionar el recurso DPHContenido
 *        </pre>
 */
package com.saga.diputacion.huelva.microservicios.view.dphcontenido;