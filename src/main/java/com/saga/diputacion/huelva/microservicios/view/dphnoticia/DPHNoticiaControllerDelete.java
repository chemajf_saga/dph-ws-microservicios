/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphnoticia
 * File DPHNoticiaControllerDelete.java
 * Created at 27 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.view.dphnoticia;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

/**
 * @author chema.jimenez
 * @since 27 abr. 2018
 * 
 *        <pre>
 *
 *        </pre>
 */
public class DPHNoticiaControllerDelete extends DPHNoticiaController {

    /**
     * @param context
     * @param req
     * @param res
     */
    public DPHNoticiaControllerDelete(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.saga.diputacion.huelva.microservicios.controller.Controller#execute()
     */
    @Override
    public void execute() throws Exception {
        this.delete();
    }
}
