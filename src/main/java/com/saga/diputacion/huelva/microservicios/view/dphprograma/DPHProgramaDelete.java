/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphprograma
 * File DPHProgramaRead.java
 * Created at 7 may. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.view.dphprograma;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

/**
 * @author chema.jimenez
 * @since 7 may. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class DPHProgramaDelete extends DPHProgramaController {

    /**
     * @param context
     * @param req
     * @param res
     */
    public DPHProgramaDelete(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.saga.diputacion.huelva.microservicios.controller.Controller#execute()
     */
    @Override
    public void execute() throws Exception {
        this.delete();
    }

}
