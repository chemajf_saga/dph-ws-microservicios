/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphcontenido
 * File DPHContenidoControllerDelete.java
 * Created at 26 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.view.dphcontenido;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

/**
 * @author chema.jimenez
 * @since 26 abr. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class DPHContenidoControllerDelete extends DPHContenidoController {

    /**
     * @param context
     * @param req
     * @param res
     */
    public DPHContenidoControllerDelete(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.saga.diputacion.huelva.microservicios.controller.Controller#execute()
     */
    @Override
    public void execute() throws Exception {
        this.delete();
    }

}
