/**
 * Package com.saga.diputacion.huelva.microservicios
 * File Config.java
 * Created at 19 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.config;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import com.saga.diputacion.huelva.microservicios.exception.NotPropertiFoundException;
import com.saga.diputacion.huelva.microservicios.util.StringUtils;

/**
 * Clase que proporciona acceso a la configuración de los Microservicios. Se accede a trvés del patrón <i>Singleton</i>
 * utilizando el método {@link Configuration#getConfigurationInstance()} <br/> Una vez instanciada utilizamos el método
 * {@link Configuration#getProp()} para obtener el objeto {@link Properties} y obtener la configuración deseada. <br/>
 *
 * La clesa posee una serie de atributos estáticos de tipo cadena de texto con los nombres de las claves para facilitar
 * el uso de la configuración.
 *
 * @author chema.jimenez
 * @since 19 abr. 2018
 */
public class Configuration {

    private static Log LOG = CmsLog.getLog(Configuration.class);

    public static final String KEY_CONFIG = "dph.microservicios.webservice";
    private static final String KEY_SITE_DESTINO = ".site.destino";
    public static final String KEY_TIPO_ORIGEN = ".tipo.origen.";

    public static final String KEY_FINAL_MODULE_NAME = ".module.name";

    public static final String KEY_FINAL_PATH_MUNICIPIOS = ".path.municipios";
    public static final String KEY_FINAL_DEFECTO_MUNICIPIO = ".defecto.municipio";

    public static final String KEY_FINAL_PATH_TEMATICAS = ".path.tematicas";
    public static final String KEY_FINAL_DEFECTO_TEMATICA = ".defecto.tematica";
    public static final String KEY_FINAL_DEFECTO_TEMATICA_ZOPA_PRINCIPAL = ".defecto.tematica.zona.principal";

    public static final String KEY_FINAL_RECURSO_DESTINO = ".recurso.destino";

    public static final String KEY_FINAL_PATH_DESTINO = ".path.destino";
    public static final String KEY_FINAL_GALERIA_IMAGENES = ".galeria.imagenes";
    public static final String KEY_FINAL_GALERIA_DOCUMENTOS = ".galeria.documentos";
    public static final String KEY_FINAL_GALERIA_VIDEOS = ".galeria.videos";
    public static final String KEY_FINAL_GALERIA_AUDIOS = ".galeria.audios";

    // key COMPLETAS
    public static final String MODULE_NAME = KEY_CONFIG + KEY_FINAL_MODULE_NAME;
    public static final String SITE_DESTINO = KEY_CONFIG + KEY_SITE_DESTINO;

    public static final String PATH_MUNICIPIOS = KEY_CONFIG + KEY_FINAL_PATH_MUNICIPIOS;
    public static final String MUNICIPIO_DEFECTO = KEY_CONFIG + KEY_FINAL_DEFECTO_MUNICIPIO;

    public static final String PATH_TEMATICAS = KEY_CONFIG + KEY_FINAL_PATH_TEMATICAS;
    public static final String TEMATICA_DEFECTO = KEY_CONFIG + KEY_FINAL_DEFECTO_TEMATICA;
    public static final String TEMATICA_DEFECTO_ZOPA_PRINCIPAL = KEY_CONFIG + KEY_FINAL_DEFECTO_TEMATICA_ZOPA_PRINCIPAL;

    public static final String PATTERN_ID_TIPO_ORIGEN = "@@TIPO-ORIGEN@@";
    public static final String PATH_DESTINO_RECURSOS = KEY_CONFIG + KEY_TIPO_ORIGEN + PATTERN_ID_TIPO_ORIGEN + KEY_FINAL_PATH_DESTINO;
    public static final String RECURSO_DESTINO = KEY_CONFIG + KEY_TIPO_ORIGEN + PATTERN_ID_TIPO_ORIGEN + KEY_FINAL_RECURSO_DESTINO;

    public static final String GALERIA_IMAGENES_RECURSOS = KEY_CONFIG + KEY_FINAL_GALERIA_IMAGENES;
    public static final String GALERIA_VIDEOS_RECURSOS = KEY_CONFIG + KEY_FINAL_GALERIA_VIDEOS;
    public static final String GALERIA_AUDIOS_RECURSOS = KEY_CONFIG + KEY_FINAL_GALERIA_AUDIOS;
    public static final String GALERIA_DOCUMENTOS_RECURSOS = KEY_CONFIG + KEY_FINAL_GALERIA_DOCUMENTOS;

    /** The conf. */
    private static Configuration conf;

    /** The prop. */
    private Properties prop;

    /**
     * Instantiates a new configuration.
     *
     * @throws IOException
     *
     * @throws Exception
     */
    private Configuration() throws IOException {
        LOG.info("--- Creamos instancia de configuracion ---");
        this.prop = new Properties();
        this.prop.load(this.getClass().getResourceAsStream("/config.properties"));
        LOG.info("Archivo ce configuracion /config.properties cargado");
        LOG.info("\r\n" + this.toString() + "\r\n");

        LOG.info("------------------------------------------");
    }

    /**
     * Gets the configuration instance.
     *
     * @return the configuration instance
     * @throws Exception
     */
    public static Configuration getConfigurationInstance() throws Exception {
        if (conf == null) {
            conf = new Configuration();
        }
        return conf;
    }

    /**
     * Gets the prop.
     *
     * @return the prop
     */
    public Properties getProp() {
        return this.prop;
    }

    /**
     * Devuelve el valor de la clave indicada en la configuración. Para ello primero obtiene una instancia de la clase
     * {@link Configuration} obtiene el objeto {@link Properties} de la misma y utiliza el método
     * {@link Properties#get(Object)} para obtener el valor. <br/> Si se pruduce una excepción se informa en el log y se
     * devuelve cadena vacía.
     *
     * @param key Clave para obtener el valor
     * @return
     * @throws Exception
     */
    public static String getValue(String key) throws NotPropertiFoundException {
        try {
            return getConfigurationInstance().getProp().get(key).toString();
        } catch (Exception e) {
            LOG.error("Error en la obtencion de una propiedad de la configuracion: [" + e.getMessage() + "] Devolvemos cadena vacía.");
            throw new NotPropertiFoundException(
                            "Error en la obtencion de una propiedad de la configuracion: [" + e.getMessage() + "] Devolvemos cadena vacía.");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        if (this.prop != null) {
            StringBuffer aux = new StringBuffer();
            Enumeration<Object> keys = this.prop.keys();

            Map<String, Integer> m = StringUtils.getMaxLengthForProp(this.prop);
            int maxLenthKeys = m.get("keys");
            int maxLenthProperties = m.get("properties");
            String blanks = StringUtils.getBlanks(10);

            //
            int max = 0;
            keys = this.prop.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                String keyToLog = StringUtils.completeWithBlanksToNumbre(key, maxLenthKeys, true);
                String propertie = this.prop.getProperty(key);
                String propertieToLog = StringUtils.completeWithBlanksToNumbre(propertie, maxLenthProperties, false);
                String toAdd = "| " + blanks + keyToLog + " = " + propertieToLog + blanks + " |";
                aux.append(toAdd + "\r\n");
                if (toAdd.length() > max) {
                    max = toAdd.length();
                }
            }

            StringBuffer res = new StringBuffer();
            res.append(StringUtils.getLine(max) + "\r\n");
            res.append(aux);
            res.append(StringUtils.getLine(max) + "\r\n");

            return res.toString();
        } else {
            return super.toString();
        }
    }

}
