/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphnoticia
 * File DPHNoticiaControllerCreateUpdate.java
 * Created at 26 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.view.dphnoticia;

import static com.saga.diputacion.huelva.microservicios.config.Constants.LOCALE_STR;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.json.XML;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;

import com.saga.diputacion.huelva.microservicios.manager.resources.ResourceContent;

/**
 * @author chema.jimenez
 * @since 26 abr. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class DPHNoticiaControllerCreateUpdate extends DPHNoticiaController {

    private Log LOG = CmsLog.getLog(DPHNoticiaControllerCreateUpdate.class);

    /**
     * @param context
     * @param req
     * @param res
     */
    public DPHNoticiaControllerCreateUpdate(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.saga.diputacion.huelva.microservicios.controller.Controller#execute()
     */
    @Override
    public void execute() throws Exception {
        super.execute();
        String typeTargetContenido = this.getTargetType();

        // Tenemos que crear el contenido y guardarlo
        String resourceFileName = this.getResourcePathAndName();

        CmsResource resource;
        ResourceContent resourceContent;
        // Si el recurso existe
        boolean exists = this.getCmsObjectAdmin().existsResource(resourceFileName, CmsResourceFilter.ALL);
        if (exists) {
            this.LOG.debug(" - Recurso " + resourceFileName + " ya existe.");
            this.delete();
        } else {
            this.LOG.debug(" - Recurso " + resourceFileName + " no existe.");
        }

        // Creamos el HashMap
        HashMap<String, Object> data = this.generateAttributesMap();
        resource = this.getResourceManager().save(data, resourceFileName, typeTargetContenido, false, LOCALE_STR);
        resourceContent = new ResourceContent(this.getCmsObjectAdmin(), resource, LOCALE_STR);

        if (exists) {
            this.LOG.debug(" - Recurso [" + resourceFileName + "] modificado");
        } else {
            this.LOG.debug(" - Recurso [" + resourceFileName + "] creado");
        }
        // En este punto tenemos el recurso creado.

        // Le añadimos las categorías
        this.addMunicipios(resourceContent);

        // Metemos la tematica en funcion de la Zona
        this.addTematicaForZona(resourceContent, null);

        // Tratamos los contenidos relacionados
        this.addRelatedContent(resourceContent);

        // Guardamos los cambios efectuados
        resourceContent.saveXml();

        // Guardamos JSON para la respuesta
        this.setJsonObjectResponse(XML.toJSONObject(resourceContent.getXmlContent().toString()));

        // Si todo ha ido bien, publicamos
        OpenCms.getPublishManager().publishResource(this.getCmsObjectAdmin(), resourceFileName);
        // Esperamos la publicacion
        OpenCms.getPublishManager().waitWhileRunning();
    }

}
