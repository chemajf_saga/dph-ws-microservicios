/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphevento
 * File package-info.java
 * Created at 9 may. 2018
 * Created by chema.jimenez
 */
/**
 * @author chema.jimenez
 * @since 9 may. 2018
 * 
 *        <pre>
 * Este paquete contiene las clases para gestionar el recurso DPHEvento
 *        </pre>
 */
package com.saga.diputacion.huelva.microservicios.view.dphevento;