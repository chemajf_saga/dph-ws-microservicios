/**
 * Package com.saga.diputacion.huelva.microservicios.view
 * File package-info.java
 * Created by chema.jimenez
 */
/**
 * @author chema.jimenez
 * @since 8 may. 2018
 *
 *        <pre>
 * Este paquete contiene un paquete por cada tipo de contenido a gestionar desde el WebService
 *        </pre>
 */
package com.saga.diputacion.huelva.microservicios.view;