/**
 * Package com.saga.diputacion.huelva.microservicios.config
 * File package-info.java
 * Created by chema.jimenez
 */
/**
 * @author chema.jimenez
 * @since 8 may. 2018
 *
 *        <pre>
 * Este paquete contiene la clase que da acceso a la configuración del WebService
 *        </pre>
 */
package com.saga.diputacion.huelva.microservicios.config;