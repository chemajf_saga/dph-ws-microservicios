/**
 * Package com.saga.diputacion.huelva.microservicios.util
 * File package-info.java
 * Created by chema.jimenez
 */
/**
 * @author chema.jimenez
 * @since 8 may. 2018
 *
 *        <pre>
 * Este paquete contiene las clases de utilidades del WebService
 *        </pre>
 */
package com.saga.diputacion.huelva.microservicios.util;