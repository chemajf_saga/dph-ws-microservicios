/**
 * Package com.saga.diputacion.huelva.microservicios.manager
 * File RsManager.java
 * Created at 16 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.manager.resources;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsProject;
import org.opencms.file.CmsProperty;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.file.types.I_CmsResourceType;
import org.opencms.loader.CmsLoaderException;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.opencms.xml.CmsXmlContentDefinition;
import org.opencms.xml.CmsXmlEntityResolver;
import org.opencms.xml.content.CmsXmlContent;
import org.opencms.xml.content.CmsXmlContentFactory;
import org.opencms.xml.types.I_CmsXmlContentValue;

/**
 * @author chema.jimenez
 * @since 16 abr. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class ResourcesManager {

    private static final Log LOG = CmsLog.getLog(ResourcesManager.class);

    // Update content only if value is not empty
    // Update content only if value is not null
    // Update content and removes when value is null
    public static enum Mode {
        MODE_UPDATE_NON_EMPTY, MODE_UPDATE_NON_NULL, MODE_REMOVE_WHEN_NULL
    };

    CmsObject cmso;
    Mode mode;

    public ResourcesManager() {
        this.mode = Mode.MODE_UPDATE_NON_EMPTY;
    }

    /**
     * Initialize resource manager.
     *
     * @param cmso
     */
    public ResourcesManager(CmsObject cmso) {
        this();
        this.cmso = cmso;
    }

    /**
     * Initialize resource manager.
     *
     * @param cmso
     */
    public ResourcesManager(CmsObject cmso, Mode mode) {
        this.cmso = cmso;
        this.mode = mode;
    }

    public CmsResource save(HashMap data, String resource, String resourceTypeName) {
        return this.save(data, resource, resourceTypeName, false, (String) null);
    }

    public CmsResource save(HashMap data, String resource, String resourceTypeName, boolean publish) {
        return this.save(data, resource, resourceTypeName, publish, (String) null);
    }

    public CmsResource save(HashMap data, String resource, String type, boolean publish, String customLocale) {
        Object cmsResource = null;

        try {
            boolean exc = this.cmso.existsResource(resource, CmsResourceFilter.ALL);
            CmsXmlContent content = null;
            Locale localizacion = this.cmso.getRequestContext().getLocale();
            if (customLocale != null) {
                localizacion = new Locale(customLocale);
            }

            if (exc) {
                CmsFile keys = this.cmso.readFile(resource);
                cmsResource = keys;
                content = CmsXmlContentFactory.unmarshal(this.cmso, keys);
                List itKeys = content.getLocales();
                if (!itKeys.contains(localizacion)) {
                    content.copyLocale((Locale) itKeys.get(0), localizacion);
                }
            } else {
                String schema = getSchemaByType(type);
                CmsXmlContentDefinition schemaDef = CmsXmlContentDefinition.unmarshal(schema, new CmsXmlEntityResolver(this.cmso));
                content = CmsXmlContentFactory.createDocument(this.cmso, localizacion, "UTF-8", schemaDef);
            }

            Set keys2 = data.keySet();
            Iterator itKeys2 = keys2.iterator();
            boolean modified = false;

            while (itKeys2.hasNext()) {
                String key = (String) itKeys2.next();
                Object value = data.get(key);
                if (value instanceof ArrayList) {
                    modified = this.manageMultipleContent((ArrayList) value, key, localizacion, content) || modified;
                } else
                    if (value instanceof HashMap) {
                        modified = this.manageNestedContent((HashMap) value, key, localizacion, content) || modified;
                    } else
                        if (value instanceof Choice) {
                            modified = this.manageChoiceContent(((Choice) value).getSubfields(), key, localizacion, content) || modified;
                        } else {
                            modified = this.manageSimpleContent(key, (String) value, localizacion, content) || modified;
                        }
            }

            if (modified) {
                cmsResource = this.createOrEditResource(resource, type, content, publish);
                if (cmsResource == null) {
                } else {
                }
            } else {
            }
        } catch (Exception var16) {
            var16.printStackTrace();
            LOG.error(var16.toString());
        }

        return (CmsResource) cmsResource;
    }

    public static String getSchemaByType(int type) throws UtilException {
        try {
            I_CmsResourceType e = OpenCms.getResourceManager().getResourceType(type);
            String schema = e.getConfiguration().get("schema");
            return "opencms:/" + schema;
        } catch (CmsLoaderException var3) {
            throw new UtilException("No existe el recurso con id " + type);
        }
    }

    public static String getSchemaByType(String typeName) throws UtilException {
        try {
            I_CmsResourceType e = OpenCms.getResourceManager().getResourceType(typeName);
            String schema = e.getConfiguration().get("schema");
            return "opencms:/" + schema;
        } catch (CmsLoaderException var3) {
            throw new UtilException("No existe el recurso con id " + typeName);
        }
    }

    public static class UtilException extends Exception {
        private static final long serialVersionUID = 1L;

        public UtilException() {
        }

        public UtilException(String msg) {
            super(msg);
        }
    }

    protected CmsResource createOrEditResource(String resource, String typeName, CmsXmlContent content, boolean publish) {
        boolean exists = this.cmso.existsResource(resource, CmsResourceFilter.ALL);
        boolean change = false;
        Object cmsResource = null;

        try {
            CmsProject e = this.cmso.getRequestContext().getCurrentProject();
            if (e.getName().equals("Online")) {
                this.cmso.getRequestContext().setCurrentProject(this.cmso.readProject("Offline"));
                change = true;
            }

            byte[] byteContent = content.marshal();
            CmsFile cmsFile;
            if (exists) {
                this.cmso.lockResource(resource);
                cmsFile = this.cmso.readFile(resource);
                cmsFile.setContents(byteContent);
                this.cmso.writeFile(cmsFile);
                cmsFile = this.cmso.readFile(resource);
                content = CmsXmlContentFactory.unmarshal(this.cmso, cmsFile);
                cmsFile = content.getHandler().prepareForWrite(this.cmso, content, cmsFile);
                this.cmso.unlockResource(resource);
                if (publish) {
                    OpenCms.getPublishManager().publishResource(this.cmso, resource);
                }

                cmsResource = cmsFile;
            } else {
                cmsResource = this.cmso.createResource(resource, OpenCms.getResourceManager().getResourceType(typeName), byteContent,
                                new ArrayList<CmsProperty>());
                cmsFile = this.cmso.readFile(resource);
                content = CmsXmlContentFactory.unmarshal(this.cmso, cmsFile);
                content.getHandler().prepareForWrite(this.cmso, content, cmsFile);
                this.cmso.unlockResource(resource);
                if (publish) {
                    OpenCms.getPublishManager().publishResource(this.cmso, resource);
                }
            }

            if (change) {
                this.cmso.getRequestContext().setCurrentProject(e);
            }
        } catch (Exception var11) {
            var11.printStackTrace();
            LOG.error(var11.toString());
        }

        return (CmsResource) cmsResource;
    }

    protected boolean manageMultipleContent(List listaValores, String key, Locale localizacion, CmsXmlContent content) {
        boolean modified = false;
        if (listaValores != null && listaValores.size() > 0) {
            int i = 0;
            I_CmsXmlContentValue contentValue = null;
            if (content.hasValue(key, localizacion)) {
                contentValue = content.getValue(key, localizacion);
                int itList = contentValue.getMaxIndex();
                Iterator map2;
                HashMap map21;
                if (listaValores.size() == itList) {
                    if (listaValores.get(0) instanceof HashMap) {
                        for (map2 = listaValores.iterator(); map2.hasNext(); ++i) {
                            map21 = (HashMap) map2.next();
                            modified = this.manageNestedContent(map21, key, localizacion, content, i) || modified;
                        }
                    } else {
                        while (i < listaValores.size()) {
                            modified = this.manageSimpleContent(key, (String) listaValores.get(i), localizacion, content, i) || modified;
                            ++i;
                        }
                    }
                } else {
                    for (int var13 = itList - 1; var13 > 0; --var13) {
                        content.removeValue(key, localizacion, var13);
                    }

                    if (listaValores.get(0) instanceof HashMap) {
                        for (map2 = listaValores.iterator(); map2.hasNext(); ++i) {
                            map21 = (HashMap) map2.next();
                            this.manageNestedContent(map21, key, localizacion, content, i);
                        }
                    } else {
                        while (i < listaValores.size()) {
                            this.manageSimpleContent(key, (String) listaValores.get(i), localizacion, content, i);
                            ++i;
                        }
                    }

                    modified = true;
                }
            } else {
                if (listaValores.get(0) instanceof HashMap) {
                    for (Iterator var12 = listaValores.iterator(); var12.hasNext(); ++i) {
                        HashMap var14 = (HashMap) var12.next();
                        this.manageNestedContent(var14, key, localizacion, content, i);
                    }
                } else {
                    while (i < listaValores.size()) {
                        this.manageSimpleContent(key, (String) listaValores.get(i), localizacion, content, i);
                        ++i;
                    }
                }

                modified = true;
            }
        }

        return modified;
    }

    public boolean manageChoiceContent(List<HashMap> listaValores, String key, Locale localizacion, CmsXmlContent content) {
        boolean modified = false;
        if (listaValores != null && listaValores.size() > 0) {
            I_CmsXmlContentValue var20 = null;
            boolean borrarYCrear = false;
            Iterator contadorMap1;
            HashMap c;
            Iterator it;
            HashMap<String, Integer> var23;
            if (content.hasValue(key, localizacion)) {
                var20 = content.getValue(key, localizacion);
                int xPath = var20.getMaxIndex();
                int contadorMap;
                String currentCont;
                if (xPath == listaValores.size()) {
                    contadorMap = 1;

                    for (contadorMap1 = listaValores.iterator(); contadorMap1.hasNext(); ++contadorMap) {
                        c = (HashMap) contadorMap1.next();
                        it = c.keySet().iterator();

                        while (it.hasNext()) {
                            Object key2 = it.next();
                            currentCont = key + "[" + contadorMap + "]";
                            List valor2 = content.getSubValues(currentCont, localizacion);
                            if (valor2.size() > 0 && !("" + key2).equals(((I_CmsXmlContentValue) valor2.get(0)).getName())) {
                                borrarYCrear = true;
                            }
                        }
                    }
                } else {
                    borrarYCrear = true;
                }

                Object valor21;
                Iterator var27;
                HashMap var28;
                Iterator var29;
                Integer var32;
                if (borrarYCrear) {
                    for (contadorMap = xPath - 1; contadorMap >= 0; --contadorMap) {
                        content.removeValue(key, localizacion, contadorMap);
                    }

                    var23 = new HashMap<>();
                    var20 = content.addValue(this.cmso, key, localizacion, 0);
                    String var24 = var20.getPath() + "/";
                    var27 = listaValores.iterator();

                    while (var27.hasNext()) {
                        var28 = (HashMap) var27.next();
                        var29 = var28.keySet().iterator();

                        while (var29.hasNext()) {
                            currentCont = (String) var29.next();
                            var32 = Integer.valueOf(1);
                            if (var23.containsKey(currentCont)) {
                                var32 = var23.get(currentCont);
                                var32 = Integer.valueOf(var32.intValue() + 1);
                                var23.put(currentCont, var32);
                            } else {
                                var23.put(currentCont, Integer.valueOf(1));
                            }

                            valor21 = var28.get(currentCont);
                            if (valor21 instanceof ArrayList) {
                                this.manageMultipleContent((ArrayList) valor21, var24 + currentCont + "[" + var32 + "]", localizacion, content);
                            } else
                                if (valor21 instanceof HashMap) {
                                    this.manageNestedContent((HashMap) valor21, var24 + currentCont, localizacion, content, var32.intValue() - 1);
                                } else
                                    if (valor21 instanceof Choice) {
                                        this.manageChoiceContent(((Choice) valor21).getSubfields(), var24 + currentCont + "[" + var32 + "]",
                                                        localizacion, content);
                                    } else
                                        if (valor21 instanceof String) {
                                            this.manageSimpleContent(var24 + currentCont, (String) valor21, localizacion, content,
                                                            var32.intValue() - 1);
                                        }
                        }
                    }

                    modified = true;
                } else {
                    String var25 = key + "[1]" + "/";
                    HashMap<String, Integer> var26 = new HashMap<>();
                    var27 = listaValores.iterator();

                    while (var27.hasNext()) {
                        var28 = (HashMap) var27.next();
                        var29 = var28.keySet().iterator();

                        while (var29.hasNext()) {
                            currentCont = (String) var29.next();
                            var32 = Integer.valueOf(1);
                            if (var26.containsKey(currentCont)) {
                                var32 = var26.get(currentCont);
                                var32 = Integer.valueOf(var32.intValue() + 1);
                                var26.put(currentCont, var32);
                            } else {
                                var26.put(currentCont, Integer.valueOf(1));
                            }

                            valor21 = var28.get(currentCont);
                            if (valor21 instanceof ArrayList) {
                                this.manageMultipleContent((ArrayList) valor21, var25 + currentCont + "[" + var32 + "]", localizacion, content);
                            } else
                                if (valor21 instanceof HashMap) {
                                    this.manageNestedContent((HashMap) valor21, var25 + currentCont, localizacion, content, var32.intValue() - 1);
                                } else
                                    if (valor21 instanceof Choice) {
                                        this.manageChoiceContent(((Choice) valor21).getSubfields(), var25 + currentCont + "[" + var32 + "]",
                                                        localizacion, content);
                                    } else
                                        if (valor21 instanceof String) {
                                            this.manageSimpleContent(var25 + currentCont, (String) valor21, localizacion, content,
                                                            var32.intValue() - 1);
                                        }
                        }
                    }
                }
            } else {
                var20 = content.addValue(this.cmso, key, localizacion, 0);
                String var22 = var20.getPath() + "/";
                var23 = new HashMap<>();
                contadorMap1 = listaValores.iterator();

                while (contadorMap1.hasNext()) {
                    c = (HashMap) contadorMap1.next();
                    it = c.keySet().iterator();

                    while (it.hasNext()) {
                        String var30 = (String) it.next();
                        Integer var31 = Integer.valueOf(1);
                        if (var23.containsKey(var30)) {
                            var31 = var23.get(var30);
                            var31 = Integer.valueOf(var31.intValue() + 1);
                            var23.put(var30, var31);
                        } else {
                            var23.put(var30, Integer.valueOf(1));
                        }

                        Object var33 = c.get(var30);
                        if (var33 instanceof ArrayList) {
                            this.manageMultipleContent((ArrayList) var33, var22 + var30 + "[" + var31 + "]", localizacion, content);
                        } else
                            if (var33 instanceof HashMap) {
                                this.manageNestedContent((HashMap) var33, var22 + var30, localizacion, content, var31.intValue() - 1);
                            } else
                                if (var33 instanceof Choice) {
                                    this.manageChoiceContent(((Choice) var33).getSubfields(), var22 + var30 + "[" + var31 + "]", localizacion,
                                                    content);
                                } else
                                    if (var33 instanceof String) {
                                        this.manageSimpleContent(var22 + var30, (String) var33, localizacion, content, var31.intValue() - 1);
                                    }
                    }
                }

                modified = true;
            }
        } else {
            if (content.hasValue(key, localizacion, 0)) {
                I_CmsXmlContentValue contentValue = content.getValue(key, localizacion);
                int numElementos = contentValue.getMaxIndex();

                for (int j = numElementos - 1; j >= contentValue.getMinOccurs(); --j) {
                    content.removeValue(key, localizacion, j);
                }
            }

            modified = true;
        }

        return modified;
    }

    public boolean manageNestedContent(HashMap map2, String key, Locale localizacion, CmsXmlContent content) {
        return this.manageNestedContent(map2, key, localizacion, content, 0);
    }

    public boolean manageNestedContent(HashMap map2, String key, Locale localizacion, CmsXmlContent content, int i) {
        boolean modified = false;
        I_CmsXmlContentValue contentValue = null;
        if (!content.hasValue(key, localizacion, i)) {
            contentValue = content.addValue(this.cmso, key, localizacion, i);
            modified = true;
        } else {
            contentValue = content.getValue(key, localizacion, i);
        }

        String xPath = contentValue.getPath() + "/";
        Set keys2 = map2.keySet();
        Iterator itKeys2 = keys2.iterator();

        while (itKeys2.hasNext()) {
            String key2 = (String) itKeys2.next();
            Object valor2 = map2.get(key2);
            if (valor2 instanceof ArrayList) {
                modified = this.manageMultipleContent((ArrayList) valor2, xPath + key2, localizacion, content) || modified;
            } else
                if (valor2 instanceof HashMap) {
                    modified = this.manageNestedContent((HashMap) valor2, xPath + key2, localizacion, content) || modified;
                } else
                    if (valor2 instanceof Choice) {
                        modified = this.manageChoiceContent(((Choice) valor2).getSubfields(), xPath + key, localizacion, content) || modified;
                    } else {
                        modified = this.manageSimpleContent(xPath + key2, (String) valor2, localizacion, content) || modified;
                    }
        }

        return modified;
    }

    public boolean manageSimpleContent(String key, String valor, Locale localizacion, CmsXmlContent content) {
        return this.manageSimpleContent(key, valor, localizacion, content, 0);
    }

    public boolean manageSimpleContent(String key, String valor, Locale localizacion, CmsXmlContent content, int i) {
        boolean modified = false;

        try {
            switch (this.mode) {
                case MODE_UPDATE_NON_EMPTY:
                    modified = this.updateNotEmpty(key, valor, localizacion, content, i);
                    break;
                case MODE_UPDATE_NON_NULL:
                    modified = this.updateNotNull(key, valor, localizacion, content, i);
                    break;
                case MODE_REMOVE_WHEN_NULL:
                    modified = this.updateRmWhenNull(key, valor, localizacion, content, i);
                    break;
                default:
                    throw new UtilException("Mode " + this.mode + " is not defined");
            }
        } catch (UtilException e) {
            LOG.error(e);
        }
        return modified;
    }

    private boolean updateRmWhenNull(String key, String valor, Locale localizacion, CmsXmlContent content, int i) {
        boolean modified = false;
        I_CmsXmlContentValue contentValue = null;

        if (content.hasValue(key, localizacion, i) && valor != null) {
            contentValue = content.getValue(key, localizacion, i);
            if (!valor.equals(contentValue.getStringValue(this.cmso))) {
                contentValue.setStringValue(this.cmso, valor);
                modified = true;
            }
        } else
            if (content.hasValue(key, localizacion, i) && valor == null) {
                content.removeValue(key, localizacion, i);
                modified = true;
            } else
                if (valor != null) {
                    contentValue = content.addValue(this.cmso, key, localizacion, i);
                    contentValue.setStringValue(this.cmso, valor);
                    modified = true;
                }
        return modified;
    }

    private boolean updateNotNull(String key, String valor, Locale localizacion, CmsXmlContent content, int i) {
        boolean modified = false;
        I_CmsXmlContentValue contentValue = null;

        if (valor != null) {
            if (content.hasValue(key, localizacion, i)) {
                contentValue = content.getValue(key, localizacion, i);
                if (!valor.equals(contentValue.getStringValue(this.cmso))) {
                    contentValue.setStringValue(this.cmso, valor);
                }
            } else {
                contentValue = content.addValue(this.cmso, key, localizacion, i);
                contentValue.setStringValue(this.cmso, valor);
            }
            modified = true;
        }
        return modified;
    }

    /**
     * Update resource field if new value is not empty
     *
     * @param key
     * @param valor
     * @param localizacion
     * @param content
     * @param i
     * @return
     */
    public boolean updateNotEmpty(String key, String valor, Locale localizacion, CmsXmlContent content, int i) {
        boolean modified = false;
        I_CmsXmlContentValue contentValue = null;

        if (!StringUtils.isEmpty(valor)) {
            if (content.hasValue(key, localizacion, i)) {
                contentValue = content.getValue(key, localizacion, i);
                if (!valor.equals(contentValue.getStringValue(this.cmso))) {
                    contentValue.setStringValue(this.cmso, valor);
                }
            } else {
                contentValue = content.addValue(this.cmso, key, localizacion, i);
                contentValue.setStringValue(this.cmso, valor);
            }
            modified = true;
        }
        return modified;
    }

    public class Choice {
        private String fieldName;
        private List subfields;

        public Choice(String fieldName, List subfields) {
            this.fieldName = fieldName;
            this.subfields = subfields;
        }

        public Choice(String fieldName) {
            this.fieldName = fieldName;
        }

        public String getFieldName() {
            return this.fieldName;
        }

        public void setFieldName(String fieldName) {
            this.fieldName = fieldName;
        }

        public List getSubfields() {
            return this.subfields;
        }

        public void setSubfields(List subfields) {
            this.subfields = subfields;
        }
    }

    public boolean copyToLocale(String resource, Locale fromLocale, Locale toLocales) {
        return this.copyToLocale(resource, fromLocale, toLocales, true);
    }

    public boolean copyToLocale(String resource, Locale fromLocale, Locale toLocales, boolean publicar) {
        ArrayList<Locale> l = new ArrayList<>();
        l.add(toLocales);
        return this.copyToLocale(resource, fromLocale, l, publicar);
    }

    public boolean copyToLocale(String resource, Locale fromLocale, List<Locale> toLocales) {
        return this.copyToLocale(resource, fromLocale, toLocales, true);
    }

    public boolean copyToLocale(String ruta, Locale fromLocale, List<Locale> toLocales, boolean publicar) {
        boolean b = true;

        try {
            CmsResource e = this.cmso.readResource(ruta);
            this.cmso.lockResource(ruta);
            CmsFile file = this.cmso.readFile(e);
            CmsXmlContent content = CmsXmlContentFactory.unmarshal(this.cmso, file);

            // Por cada locale de destino copiamos el de origen
            for (int i = 0; i < toLocales.size(); i++) {
                Locale to = toLocales.get(i);
                if (!to.equals(fromLocale)) {
                    if (content.hasLocale(to)) {
                        content.removeLocale(to);
                    }
                    content.copyLocale(fromLocale, to);
                }
            }

            // Guardamos los cambios
            String decodedContent1 = content.toString();
            file.setContents(decodedContent1.getBytes(content.getEncoding()));
            this.cmso.writeFile(file);
            this.cmso.unlockResource(ruta);
            if (publicar) {
                OpenCms.getPublishManager().publishResource(this.cmso, ruta);
            }
        } catch (UnsupportedEncodingException var11) {
            b = false;
            var11.printStackTrace();
            LOG.error("Error copiando de un idioma a otro");
        } catch (CmsException var12) {
            b = false;
            var12.printStackTrace();
            LOG.error("Error copiando de un idioma a otro");
        } catch (Exception var13) {
            b = false;
            LOG.error("Error copiando de un idioma a otro");
            var13.printStackTrace();
        }

        return b;
    }

}
