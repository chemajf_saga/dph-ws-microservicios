/**
 * Package com.saga.diputacion.huelva.microservicios.exception
 * File NotValidJsonInRequestException.java
 * Created at 24 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.exception;

/**
 * @author chema.jimenez
 * @since 24 abr. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class NotValidJsonInRequestException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 5563062082198790339L;

    /**
     *
     */
    public NotValidJsonInRequestException() {
        super();
    }

    /**
     * @param message
     */
    public NotValidJsonInRequestException(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public NotValidJsonInRequestException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public NotValidJsonInRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public NotValidJsonInRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
