/**
 * Package com.saga.diputacion.huelva.microservicios.exception
 * File NotResourceFoundException.java
 * Created at 2 may. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.exception;

/**
 * @author chema.jimenez
 * @since 2 may. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class NotResourceFoundException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 4072433636082955956L;

    /**
     *
     */
    public NotResourceFoundException() {
    }

    /**
     * @param message
     */
    public NotResourceFoundException(String message) {
        super("No existe el recurso: " + message);
    }

    /**
     * @param cause
     */
    public NotResourceFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public NotResourceFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public NotResourceFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
