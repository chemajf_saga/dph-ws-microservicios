/**
 * Package com.saga.diputacion.huelva.microservicios.exception
 * File package-info.java
 * Created by chema.jimenez
 */
/**
 * @author chema.jimenez
 * @since 8 may. 2018
 *
 *        <pre>
 * Este paquete contiene las clases de tipo Exception que se lanzan desde el WebService
 *        </pre>
 */
package com.saga.diputacion.huelva.microservicios.exception;