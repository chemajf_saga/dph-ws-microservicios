/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphevento
 * File DPHEventoController.java
 * Created at 9 may. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.view.dphevento;

import static com.saga.diputacion.huelva.microservicios.config.Constants.IMAGE_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ALTO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ANCHO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_CONTENT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DATA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_FICHA_EVENTO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_IMAGE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_IMAGEN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_IMAGE_MAIN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_HEIGHT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_IMAGE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_TITLE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_WIDTH;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_MAS_INFO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_MEDIA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_REMARKS;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TEASER;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TEMATICA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TESELA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TEXTO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_VALUE;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.opencms.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.saga.diputacion.huelva.microservicios.controller.Controller;
import com.saga.diputacion.huelva.microservicios.util.HTMLUtils;
import com.saga.diputacion.huelva.microservicios.util.StringUtils;

/**
 * @author chema.jimenez
 * @since 9 may. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
@SuppressWarnings("unchecked")
public class DPHEventoController extends Controller {

    private HashMap<String, Object> data;

    /**
     * @param context
     * @param req
     * @param res
     */
    public DPHEventoController(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /**
     *
     * @return Mapa con los atributos del recurso a crear/modificar
     * @throws Exception
     */
    protected HashMap<String, Object> generateAttributesMap() throws Exception {
        // Tratamos los Enlaces y Documentos
        this.addLinksAndDocuments();

        // Tratamos el texto de la descripción
        String textDescHtml =
                        this.repairSrcInHtml(this.getJsonObject().getJSONObject(JSON_DATA).getJSONObject(JSON_FICHA_EVENTO).getString("Description"));
        this.getJsonObject().getJSONObject(JSON_DATA).getJSONObject(JSON_FICHA_EVENTO).put("Description", textDescHtml);

        // Tratamos el texto de las observaciones
        String textObsHtml =
                        this.repairSrcInHtml(this.getJsonObject().getJSONObject(JSON_DATA).getJSONObject(JSON_FICHA_EVENTO).getString(JSON_REMARKS));
        this.getJsonObject().getJSONObject(JSON_DATA).getJSONObject(JSON_FICHA_EVENTO).put(JSON_REMARKS, textObsHtml);

        // Tratamos el texto del horario
        String horario = this.repairSrcInHtml(this.getJsonObject().getJSONObject(JSON_DATA).getJSONObject(JSON_FICHA_EVENTO)
                        .getJSONObject(JSON_MAS_INFO).getString(JSON_VALUE));
        this.getJsonObject().getJSONObject(JSON_DATA).getJSONObject(JSON_FICHA_EVENTO).getJSONObject(JSON_MAS_INFO).put(JSON_VALUE, horario);

        // Tratamos el texto del resumen
        String textTeaser = this.repairSrcInHtml(this.getJsonObject().getJSONObject(JSON_DATA).getString(JSON_TEASER));
        this.getJsonObject().getJSONObject(JSON_DATA).put(JSON_TEASER, HTMLUtils.htmlToPlainText(textTeaser));

        String jsonData = this.getJsonObject().getJSONObject(JSON_DATA).toString();
        this.data = new ObjectMapper().readValue(jsonData, (new HashMap<String, Object>()).getClass());

        // Informacion de la TESELA
        JSONObject tesela = this.getJsonObject().getJSONObject(JSON_TESELA);
        this.data.put(JSON_LIST_TITLE, tesela.getString(JSON_TEXTO));

        // Tratamos la imgen de la tesela
        String teselaImgOrigin = tesela.getString(JSON_IMAGEN);
        String teselaImgCreated = this.createFileInOpenCms(teselaImgOrigin, IMAGE_TYPE);
        this.data.put(JSON_LIST_IMAGE, teselaImgCreated);

        // Tratamos la imagen principal
        String imgEvent = this.getJsonObject().getJSONObject(JSON_DATA).getJSONObject(JSON_MEDIA).getJSONObject(JSON_IMAGE_MAIN).getString(JSON_IMAGE);
        if (StringUtils.isNotBlank(imgEvent)) {
            String imgEventCreated = this.createFileInOpenCms(imgEvent, IMAGE_TYPE);
            ((HashMap<String, Object>) ((HashMap<String, Object>) this.data.get(JSON_MEDIA)).get(JSON_IMAGE_MAIN)).put(JSON_IMAGE, imgEventCreated);
        } else {
            ((HashMap<String, Object>) this.data.get(JSON_CONTENT)).remove(JSON_MEDIA);
        }

        // Hay que controlar el ancho y alto
        this.data.put(JSON_LIST_WIDTH, (tesela.getInt(JSON_ANCHO) > 4 ? 4 : tesela.getInt(JSON_ALTO)) + "");
        this.data.put(JSON_LIST_HEIGHT, (tesela.getInt(JSON_ALTO) > 2 ? 2 : tesela.getInt(JSON_ALTO)) + "");

        // Eliminamos la Tematica ya que la trataresmo mas adelante
        this.data.remove(JSON_TEMATICA);

        return this.data;
    }

}
