/**
 * Package com.saga.diputacion.huelva.microservicios.util
 * File HTMLUtils.java
 * Created at 9 may. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.util;

import java.util.StringTokenizer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * @author chema.jimenez
 * @since 9 may. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class HTMLUtils {

    public static String htmlToPlainText(String htmlStr) {
        Document doc = Jsoup.parse(htmlStr);

        Elements eltosLI = doc.select("li");
        int nLi = eltosLI.size();

        StringBuffer res = new StringBuffer();
        htmlStr = htmlStr.replace("<", " <").replace(">", "> ");
        StringTokenizer st = new StringTokenizer(htmlStr, " ", false);
        boolean salto = false;
        int contLI = 0;
        while (st.hasMoreElements()) {
            salto = false;
            String object = st.nextElement().toString();

            if (object.contains("<") && object.contains(">")) {
                if (object.contains("</p>") || object.contains("</div>") || object.contains("</h")) {
                    res.append("\r\n");
                }
                if (object.contains("<li")) {
                    res.append("\r\n").append(" - ");
                }
                if (object.contains("/li")) {
                    contLI++;
                    if (contLI == nLi - 1) {
                        res.append("\r\n");
                    }
                }
            } else {
                if (object.contains("<")) {
                    while (st.hasMoreElements()) {
                        salto = false;
                        String object2 = st.nextElement().toString();
                        if (object2.contains(">")) {
                            break;
                        }
                    }
                } else {
                    res.append(object).append(" ");
                    if (salto) {
                        res.append("\r\n");
                    }
                }
            }

        }
        return (res.toString().replace("\r\n\r\n", "\r\n").replace(" , ", ", ").replace(" . ", ". ").replace(" \" ", " \"").replace(". \" ", ".\"")
                        .replace(". \"", ".\""));
    }

}
