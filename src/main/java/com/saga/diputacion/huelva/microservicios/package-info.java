/**
 * Package com.saga.diputacion.huelva.microservicios.config
 * File package-info.java
 * Created by chema.jimenez
 */
/**
 * @author chema.jimenez
 * @since 8 may. 2018
 *
 *        <pre>
 * Paquete principal
 *        </pre>
 */
package com.saga.diputacion.huelva.microservicios;