/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphcontenido
 * File DPHContenidoControllerCreate.java
 * Created at 12 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.view.dphcontenido;

import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_CONTENT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DATA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DESTACADOPORTADA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TEXT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TITLE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.LOCALE_STR;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.json.JSONException;
import org.opencms.json.XML;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;

import com.saga.diputacion.huelva.microservicios.exception.NotValidJsonInRequestException;
import com.saga.diputacion.huelva.microservicios.manager.resources.ResourceContent;
import com.saga.diputacion.huelva.microservicios.util.StringUtils;

/**
 * @author chema.jimenez
 * @since 12 abr. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class DPHContenidoControllerCreateUpdate extends DPHContenidoController {

    private Log LOG = CmsLog.getLog(DPHContenidoControllerCreateUpdate.class);

    /**
     * @param context
     * @param req
     * @param res
     */
    public DPHContenidoControllerCreateUpdate(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.saga.diputacion.huelva.microservicios.controller.Controller#execute()
     */
    @Override
    public void execute() throws Exception {
        super.execute();
        String typeTargetContenido = this.getTargetType();

        // Tenemos que crear el contenido y guardarlo
        String resourcePathAndName = this.getResourcePathAndName();

        CmsResource resource;
        ResourceContent resourceContent;
        // Si el recurso existe
        boolean exists = this.getCmsObjectAdmin().existsResource(resourcePathAndName, CmsResourceFilter.ALL);
        if (exists) {
            this.LOG.debug(" - Recurso " + resourcePathAndName + " ya existe.");
            this.delete();
        } else {
            this.LOG.debug(" - Recurso " + resourcePathAndName + " no existe.");
        }

        // Creamos el HashMap
        HashMap<String, Object> data = this.generateAttributesMap();
        resource = this.getResourceManager().save(data, resourcePathAndName, typeTargetContenido, false, LOCALE_STR);
        resourceContent = new ResourceContent(this.getCmsObjectAdmin(), resource, LOCALE_STR);

        if (exists) {
            this.LOG.debug(" - Recurso [" + resourcePathAndName + "] modificado");
        } else {
            this.LOG.debug(" - Recurso [" + resourcePathAndName + "] creado");
        }
        // En este punto tenemos el recurso creado.

        // Le añadimos las categorías
        this.addMunicipios(resourceContent);

        // Metemos la tematica en funcion de la Zona
        this.addTematicaForZona(resourceContent, null);

        // Tratamos los contenidos relacionados
        this.addRelatedContent(resourceContent);

        // Guardamos los cambios efectuados
        resourceContent.saveXml();

        // Guardamos JSON para la respuesta
        this.setJsonObjectResponse(XML.toJSONObject(resourceContent.getXmlContent().toString()));

        // Si todo ha ido bien, publicamos
        OpenCms.getPublishManager().publishResource(this.getCmsObjectAdmin(), resourcePathAndName);
        // Esperamos la publicacion
        OpenCms.getPublishManager().waitWhileRunning();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.saga.diputacion.huelva.microservicios.controller.Controller#save()
     */
    @Override
    public void save() throws Exception {
        super.save();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.saga.diputacion.huelva.microservicios.view.dphcontenido.DPHContenidoController#validate()
     */
    @Override
    public boolean validate() throws Exception {
        boolean valid = super.validate();
        if (valid) {
            // Validamos los valores del JSON para crear o actualizar contenido
            valid = this.validatePDHContenidoValuesToCreateOrUpdate();
        }
        return valid;
    }

    /**
     *
     * @return
     * @throws NotValidJsonInRequestException
     */
    private boolean validatePDHContenidoValuesToCreateOrUpdate() throws NotValidJsonInRequestException {
        boolean valid = true;
        try {
            if (this.getJsonObject().getJSONObject(JSON_DATA) == null) {
                valid = false;
                this.setValid(valid);
                throw new NotValidJsonInRequestException("Obligatorio valor en " + JSON_DATA);
            } else {
                if (StringUtils.isEmpty(this.getJsonObject().getJSONObject(JSON_DATA).getString(JSON_TITLE))) {
                    valid = false;
                    this.setValid(valid);
                    throw new NotValidJsonInRequestException("Obligatorio valor en " + JSON_DATA + "/" + JSON_TITLE);
                } else {
                    if (this.getJsonObject().getJSONObject(JSON_DATA).getJSONObject(JSON_CONTENT) == null) {
                        valid = false;
                        this.setValid(valid);
                        throw new NotValidJsonInRequestException("Obligatorio valor en " + JSON_DATA + "/" + JSON_CONTENT);
                    } else {
                        if (StringUtils.isEmpty(this.getJsonObject().getJSONObject(JSON_DATA).getJSONObject(JSON_CONTENT).getString(JSON_TEXT))) {
                            valid = false;
                            this.setValid(valid);
                            throw new NotValidJsonInRequestException("Obligatorio valor en " + JSON_DATA + "/" + JSON_CONTENT + "/" + JSON_TEXT);
                        } else {
                            if (this.getJsonObject().getJSONObject(JSON_DATA).get(JSON_DESTACADOPORTADA) == null
                                            || StringUtils.isEmpty(this.getJsonObject().getJSONObject(JSON_DATA).getString(JSON_DESTACADOPORTADA))) {
                                valid = false;
                                this.setValid(valid);
                                throw new NotValidJsonInRequestException("Obligatorio valor en " + JSON_DATA + "/" + JSON_DESTACADOPORTADA);
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            valid = false;
            this.setValid(valid);
            throw new NotValidJsonInRequestException(e);
        }
        return valid;
    }
}
