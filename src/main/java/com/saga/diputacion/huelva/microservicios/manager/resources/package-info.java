/**
 * Package com.saga.diputacion.huelva.microservicios.manager.resources
 * File package-info.java
 * Created by chema.jimenez
 */
/**
 * @author chema.jimenez
 * @since 8 may. 2018
 *
 *        <pre>
 * Este paquete contiene las clases de apoyo a la gestión de los recursos de OpenCms
 *        </pre>
 */
package com.saga.diputacion.huelva.microservicios.manager.resources;