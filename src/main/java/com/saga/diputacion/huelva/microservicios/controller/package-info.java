/**
 * Package com.saga.diputacion.huelva.microservicios.controller
 * File package-info.java
 * Created by chema.jimenez
 */
/**
 * @author chema.jimenez
 * @since 8 may. 2018
 *
 *        <pre>
 * Este paquete contiene la intarfaz que controla el WebService y la clase que la implementa
 *        </pre>
 */
package com.saga.diputacion.huelva.microservicios.controller;