/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphprograma
 * File DPHProgramaController.java
 * Created at 7 may. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.view.dphprograma;

import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ALTO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ANCHO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DATA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DESCRIPTION;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_HEIGHT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_TITLE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_WIDTH;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_REMARKS;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TESELA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TEXTO;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.opencms.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.saga.diputacion.huelva.microservicios.controller.Controller;

/**
 * @author chema.jimenez
 * @since 7 may. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
@SuppressWarnings("unchecked")
public class DPHProgramaController extends Controller {

    private HashMap<String, Object> data;

    /**
     * @param context
     * @param req
     * @param res
     */
    public DPHProgramaController(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /**
     *
     * @return Mapa con los atributos del recurso a crear/modificar
     * @throws Exception
     */
    protected HashMap<String, Object> generateAttributesMap() throws Exception {
        // Tratamos los Enlaces y Documentos
        this.addLinksAndDocuments();

        // Tratamos el texto de la descripción
        String textDescHtml = this.repairSrcInHtml(this.getJsonObject().getJSONObject(JSON_DATA).getString(JSON_DESCRIPTION));
        this.getJsonObject().getJSONObject(JSON_DATA).put(JSON_DESCRIPTION, textDescHtml);

        // Tratamos el texto de las observaciones
        String textObsHtml = this.repairSrcInHtml(this.getJsonObject().getJSONObject(JSON_DATA).getString(JSON_REMARKS));
        this.getJsonObject().getJSONObject(JSON_DATA).put(JSON_REMARKS, textObsHtml);

        String jsonData = this.getJsonObject().getJSONObject(JSON_DATA).toString();
        this.data = new ObjectMapper().readValue(jsonData, (new HashMap<String, Object>()).getClass());

        // Informacion de la TESELA
        JSONObject tesela = this.getJsonObject().getJSONObject(JSON_TESELA);
        this.data.put(JSON_LIST_TITLE, tesela.getString(JSON_TEXTO));

        // Hay que controlar el ancho y alto
        this.data.put(JSON_LIST_WIDTH, (tesela.getInt(JSON_ANCHO) > 4 ? 4 : tesela.getInt(JSON_ALTO)) + "");
        this.data.put(JSON_LIST_HEIGHT, (tesela.getInt(JSON_ALTO) > 2 ? 2 : tesela.getInt(JSON_ALTO)) + "");

        return this.data;
    }

}
