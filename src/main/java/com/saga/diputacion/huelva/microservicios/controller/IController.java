/**
 * Package com.saga.diputacion.huelva.microservicios.controller
 * File IController.java
 * Created at 12 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.controller;

/**
 * Interfaz para el controlador del WebService
 * 
 * @author chema.jimenez
 * @since 12 abr. 2018
 *
 */
public interface IController {
    public abstract void save() throws Exception;

    public abstract void execute() throws Exception;

    public abstract boolean validate() throws Exception;

    public abstract void load() throws Exception;

    public abstract void handleRequest() throws Exception;
}
