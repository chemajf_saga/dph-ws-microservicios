/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphgaleria
 * File DPHGaleriaControllerCreateUpdate.java
 * Created at 2 may. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.view.dphgaleria;

import static com.saga.diputacion.huelva.microservicios.config.Constants.ELTO_TIPO_VIDEO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.IMAGE_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ALTO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ANCHO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DATA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DATE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DESCRIPCION;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DESCRIPTION;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DURACION;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ELEMENTOS;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_FECHA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_FICHERO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ID;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_IMAGEN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_HEIGHT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_IMAGE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_TITLE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_WIDTH;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TESELA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TEXT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TEXTO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TIPO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TITLE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TITULO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.LOCALE_ES;
import static com.saga.diputacion.huelva.microservicios.config.Constants.PATT_DURATION_HOU;
import static com.saga.diputacion.huelva.microservicios.config.Constants.PATT_DURATION_MIN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.PATT_DURATION_SEC;
import static com.saga.diputacion.huelva.microservicios.config.Constants.PATT_TARGET_IMAGE_COVER;
import static com.saga.diputacion.huelva.microservicios.config.Constants.PATT_TARGET_VIDEO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.TEMPLATE_JSON_GALLERY_MEDIA_FILE_VIDEO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.WEB_TV_ID;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsResourceFilter;
import org.opencms.json.JSONArray;
import org.opencms.json.JSONException;
import org.opencms.json.JSONObject;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.icu.text.SimpleDateFormat;
import com.saga.diputacion.huelva.microservicios.util.StringUtils;

/**
 * @author chema.jimenez
 * @since 2 may. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class DPHGaleriaControllerCreateUpdate extends DPHGaleriaController {

    private Log LOG = CmsLog.getLog(DPHGaleriaControllerCreateUpdate.class);

    /**
     * @param context
     * @param req
     * @param res
     */
    public DPHGaleriaControllerCreateUpdate(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.saga.diputacion.huelva.microservicios.controller.Controller#execute()
     */
    @Override
    public void execute() throws Exception {
        super.execute();
        String typeTargetContenido = this.getTargetType();

        // Tenemos que crear el contenido y guardarlo
        String resourceFileName = this.getResourcePathAndName();

        if (this.getJsonObject().getString(JSON_ID).equalsIgnoreCase(WEB_TV_ID)) {
            this.createWebTVGalleryData(typeTargetContenido, resourceFileName);
        } else {
            this.createNormalGalleryData(typeTargetContenido, resourceFileName);
        }

    }

    /**
     * @param typeTargetContenido
     * @param resourceFileName
     * @throws Exception
     * @throws CmsException
     * @throws UnsupportedEncodingException
     * @throws JSONException
     */
    private void createNormalGalleryData(String typeTargetContenido, String resourceFileName)
                    throws Exception, CmsException, UnsupportedEncodingException, JSONException {
        // Si el recurso existe
        boolean exists = this.getCmsObjectAdmin().existsResource(resourceFileName, CmsResourceFilter.ALL);
        if (exists) {
            this.LOG.debug(" - Recurso " + resourceFileName + " ya existe.");
            this.delete();
        } else {
            this.LOG.debug(" - Recurso " + resourceFileName + " no existe.");
        }
        // Creamos el HashMap
        HashMap<String, Object> data = this.generateAttributesMap();
        this.saveDPHGaleriaResource(typeTargetContenido, resourceFileName, data, exists, "");
    }

    /**
     *
     * @param typeTargetContenido
     * @param resourceFileName
     * @throws Exception
     */
    private void createWebTVGalleryData(String typeTargetContenido, String resourceFileName) throws Exception {
        // Recorremos los elementos de un mes y generamos el JSON de cada uno
        JSONArray jSONObjectElementos = this.getJsonObject().getJSONObject(JSON_DATA).getJSONArray(JSON_ELEMENTOS);
        // Una vez que los tenemos los eliminamos del JSON
        this.getJsonObject().getJSONObject(JSON_DATA).remove(JSON_ELEMENTOS);
        // Recogemos los datos de los elementos
        if (jSONObjectElementos.length() > 0) {
            this.LOG.debug(" - Tenemos " + jSONObjectElementos.length() + " elementos de galeria");
            // Creamos un JSONArray del tamaño de elementos
            int anyoActual = -1, mesActual = -1, contGalerias = 0;

            String jsonTemplate = "";
            JSONArray jasonArrayGalleryMediaFile = new JSONArray();
            this.LOG.debug("");
            for (int i = 0; i < jSONObjectElementos.length(); i++) {
                // Array de elementos multimedia (vídeos)
                JSONObject elto = jSONObjectElementos.getJSONObject(i);

                mesActual = mesActual == -1 ? this.calculateFirstEltoMonth(elto) : mesActual;
                anyoActual = anyoActual == -1 ? this.calculateFirstEltoYear(elto) : anyoActual;

                String tipoElto = elto.getString(JSON_TIPO);
                String tituloElto = elto.getString(JSON_TITULO);

                if (mesActual == this.calculateFirstEltoMonth(elto)) {
                    // Creamos JSON de elementos
                    if (ELTO_TIPO_VIDEO.equalsIgnoreCase(tipoElto)) {
                        // Segun el fichero sabemos si es Video o Audio
                        String fichero = elto.getString(JSON_FICHERO);
                        boolean created = false;
                        if (this.isVideo(fichero)) {
                            // VIDEO
                            jsonTemplate = TEMPLATE_JSON_GALLERY_MEDIA_FILE_VIDEO;
                            // NO descargamos el video, si no que lo enlazamos directamente en el recurdo
                            String ficheroCreated = "";
                            fichero = this.prepareUrlVideoToW2(fichero);
                            this.LOG.debug(" - Video: " + fichero);
                            this.LOG.debug(" - [Web-TV] enlazamos video");
                            ficheroCreated = (fichero);
                            if (StringUtils.isNotBlank(ficheroCreated)) {
                                created = true;
                                jsonTemplate = jsonTemplate.replace(PATT_TARGET_VIDEO, ficheroCreated);
                                String imgCreated = this.createFileInOpenCms(elto.getString(JSON_IMAGEN), IMAGE_TYPE);
                                jsonTemplate = jsonTemplate.replace(PATT_TARGET_IMAGE_COVER, imgCreated);
                            } else {
                                created = false;
                            }
                        }
                        if (created) {
                            // String[] duracion = (elto.getString(JSON_DURACION) != null &&
                            // StringUtils.isNotBlank(elto.getString(JSON_DURACION))) ?
                            // this.duracion(elto.getString(JSON_DURACION)) : new String[3];
                            if (elto.getString(JSON_DURACION) != null && !elto.getString(JSON_DURACION).equalsIgnoreCase("null")
                                            && StringUtils.isNotBlank(elto.getString(JSON_DURACION))) {
                                String[] duracion = this.duracion(elto.getString(JSON_DURACION));
                                jsonTemplate = jsonTemplate.replace(PATT_DURATION_HOU, duracion[0]);
                                jsonTemplate = jsonTemplate.replace(PATT_DURATION_MIN, duracion[1]);
                                jsonTemplate = jsonTemplate.replace(PATT_DURATION_SEC, duracion[2]);
                            } else {
                                jsonTemplate = jsonTemplate.replace(PATT_DURATION_HOU, "");
                                jsonTemplate = jsonTemplate.replace(PATT_DURATION_MIN, "");
                                jsonTemplate = jsonTemplate.replace(PATT_DURATION_SEC, "");
                            }
                        } else {
                            jsonTemplate = "";
                        }
                    } else {
                        this.LOG.debug("El elemento #" + i + " no es de tipo VIDEO. No se tendrá en cuenta.");
                    }
                    // Añadimos el elemento al array
                    if (StringUtils.isNotBlank(jsonTemplate)) {
                        // Creamos el objeto JSONObject
                        JSONObject jsonAux = new JSONObject(jsonTemplate);
                        if (elto.getString(JSON_DESCRIPCION) != null && !elto.getString(JSON_DESCRIPCION).equalsIgnoreCase("null")) {
                            jsonAux.put(JSON_DESCRIPTION, elto.getString(JSON_DESCRIPCION));
                        }
                        jsonAux.put(JSON_TITLE, tituloElto);
                        jsonAux.put(JSON_DATE, elto.getString(JSON_FECHA));
                        jasonArrayGalleryMediaFile.put(jsonAux);
                    }
                } else {
                    // Con el JSON de elementso creado, creamos el recurso
                    resourceFileName = this.createWebTvDPHGaleria(typeTargetContenido, resourceFileName, anyoActual, mesActual,
                                    jasonArrayGalleryMediaFile);
                    contGalerias++;
                    // Reiniciamos
                    mesActual = -1;
                    anyoActual = -1;
                    i--;
                    jsonTemplate = "";
                    jasonArrayGalleryMediaFile = new JSONArray();
                    resourceFileName = this.getResourcePathAndName();
                }
            } // FOR
            // Creamos la ultima
            resourceFileName = this.createWebTvDPHGaleria(typeTargetContenido, resourceFileName, anyoActual, mesActual, jasonArrayGalleryMediaFile);
            contGalerias++;
            this.LOG.debug("-- Se han creado " + contGalerias + " galerias para Web-TV");
        }

    }

    /**
     *
     * @param typeTargetContenido
     * @param resourceFileName
     * @param anyoActual
     * @param mesActual
     * @param jasonArrayGalleryMediaFile
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    private String createWebTvDPHGaleria(String typeTargetContenido, String resourceFileName, int anyoActual, int mesActual,
                    JSONArray jasonArrayGalleryMediaFile) throws Exception {

        // Cambiamos el nombre de la Galeria
        resourceFileName = this.generateWebTvGalleriName(resourceFileName, mesActual, anyoActual);

        // Si el recurso existe
        boolean exists = this.getCmsObjectAdmin().existsResource(resourceFileName, CmsResourceFilter.ALL);
        if (exists) {
            this.LOG.debug(" - Recurso " + resourceFileName + " ya existe.");
            this.delete(resourceFileName);
        } else {
            this.LOG.debug(" - Recurso " + resourceFileName + " no existe.");
        }
        // Cambiamos el título a la galería
        GregorianCalendar gc = new GregorianCalendar(anyoActual, mesActual, 1);
        SimpleDateFormat sdfMonth = new SimpleDateFormat("MMMM 'de' yyyy", LOCALE_ES);
        String titleProp = StringUtils.firstCharacterUpercase(sdfMonth.format(new Date(gc.getTimeInMillis())));
        this.getJsonObject().getJSONObject(JSON_DATA).remove(JSON_TITLE);
        this.getJsonObject().getJSONObject(JSON_DATA).put(JSON_TITLE, titleProp);
        // Añadimos el array de GalleryMediaFile a data
        this.getJsonObject().getJSONObject(JSON_DATA).put("GalleryMediaElement", jasonArrayGalleryMediaFile);

        String textHtml = this.repairSrcInHtml(this.getJsonObject().getJSONObject(JSON_DATA).getString(JSON_TEXT));
        this.getJsonObject().getJSONObject(JSON_DATA).put(JSON_TEXT, textHtml);

        String jsonData = this.getJsonObject().getJSONObject(JSON_DATA).toString();
        HashMap<String, Object> data = new ObjectMapper().readValue(jsonData, (new HashMap<String, Object>()).getClass());

        // Informacion de la TESELA
        JSONObject tesela = this.getJsonObject().getJSONObject(JSON_TESELA);
        data.put(JSON_LIST_TITLE, tesela.getString(JSON_TEXTO));

        // Tratamos la imgen de la tesela
        String teselaImgOrigin = tesela.getString(JSON_IMAGEN);
        String teselaImgCreated = this.createFileInOpenCms(teselaImgOrigin, IMAGE_TYPE);
        data.put(JSON_LIST_IMAGE, teselaImgCreated);

        // Hay que controlar el ancho y alto
        data.put(JSON_LIST_WIDTH, (tesela.getInt(JSON_ANCHO) > 4 ? 4 : tesela.getInt(JSON_ALTO)) + "");
        data.put(JSON_LIST_HEIGHT, (tesela.getInt(JSON_ALTO) > 2 ? 2 : tesela.getInt(JSON_ALTO)) + "");

        this.saveDPHGaleriaResource(typeTargetContenido, resourceFileName, data, exists, "Web-TV - " + titleProp);
        return resourceFileName;
    }

    /**
     * @param resourceFileName
     * @param anyoActual
     * @param mesActual
     * @return
     */
    private String generateWebTvGalleriName(String resourceFileName, int mesActual, int anyoActual) {
        String mesActualStr = mesActual + 1 < 10 ? "0" + (mesActual + 1) : "" + (mesActual + 1);
        String res = "";
        String[] array = resourceFileName.split("/");
        String fileName = array[array.length - 1];
        for (int i = 0; i < array.length - 1; i++) {
            res += array[i] + "/";
        }
        res += fileName.split("\\.")[0] + "_" + anyoActual + "_" + mesActualStr + "." + fileName.split("\\.")[1];
        res = res.split(".content")[0] +  ".content"+res.split(".content")[1];
        return res;
    }

    /**
     *
     * @param elto
     * @return
     * @throws NumberFormatException
     * @throws JSONException
     */
    private int calculateFirstEltoYear(JSONObject elto) throws NumberFormatException, JSONException {
        if (elto != null && StringUtils.isNotBlank(elto.getString(JSON_FECHA))) {
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(Long.parseLong(elto.getString(JSON_FECHA)));
            return gc.get(GregorianCalendar.YEAR);
        } else {
            return -1;
        }
    }

    /**
     *
     * @param elto
     * @return
     * @throws NumberFormatException
     * @throws JSONException
     */
    private int calculateFirstEltoMonth(JSONObject elto) throws NumberFormatException, JSONException {
        if (elto != null && StringUtils.isNotBlank(elto.getString(JSON_FECHA))) {
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(Long.parseLong(elto.getString(JSON_FECHA)));
            return gc.get(GregorianCalendar.MONTH);
        } else {
            return -1;
        }
    }

}
