/**
 * Package com.saga.diputacion.huelva.microservicios.util
 * File StringUtils.java
 * Created at 23 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.commons.lang3.ArrayUtils;

/**
 * Clase de utilizadades sobre cadenas de texto que complementa a la clase {@link org.apache.commons.lang3.StringUtils}
 *
 * @author chema.jimenez
 * @since 23 abr. 2018
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {

    /**
     *
     */
    public StringUtils() {
        super();
    }

    /**
     *
     * @param in
     * @param patt
     * @return
     */
    public static String getLastStringFromSplit(String in, String patt) {
        if (isNotEmpty(in)) {
            return in.split(patt)[in.split(patt).length - 1];
        } else {
            return "";
        }
    }

    /**
     *
     * @param enu
     * @return
     */
    public static int getMaxLengthForEnumOfString(Enumeration<Object> enu) {
        int res = 0;
        while (enu.hasMoreElements()) {
            String string = enu.nextElement().toString();
            if (string.length() > res) {
                res = string.length();
            }
        }
        return res;
    }

    /**
     *
     * @param prop
     * @return
     */
    public static Map<String, Integer> getMaxLengthForProp(Properties prop) {
        Map<String, Integer> res = new HashMap<>();
        Enumeration<Object> keys = prop.keys();
        res.put("keys", getMaxLengthForEnumOfString(keys));
        int maxLengthPropertie = 0;
        keys = prop.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            String propertie = prop.getProperty(key);
            if (propertie.length() > maxLengthPropertie) {
                maxLengthPropertie = propertie.length();
            }
        }
        res.put("properties", maxLengthPropertie);
        return res;
    }

    /**
     *
     * @param string
     * @param max
     * @return
     */
    public static String completeWithBlanksToNumbre(String string, int max, boolean front) {
        String blanksToAdd = "";
        if (string.length() < max) {
            int toAdd = max - string.length();
            for (int i = 0; i < toAdd; i++) {
                blanksToAdd += " ";
            }
        }
        if (front) {
            return blanksToAdd + string;
        } else {
            return string + blanksToAdd;
        }
    }

    /**
     * @param i
     * @return
     */
    public static String getBlanks(int i) {
        String res = "";
        for (int j = 0; j < i; j++) {
            res += " ";
        }
        return res;
    }

    /**
     * @param i
     * @return
     */
    public static String getLine(int i) {
        String res = "";
        for (int j = 0; j < i; j++) {
            res += "-";
        }
        return res;
    }

    /**
     * Coamprueba si la cadena indicada está en el array indicado
     *
     * @param str
     * @param strArray
     * @return Devuelve <b>TRUE</b> si la cadena está en el array o <b>FALSE</b> en caso contrario
     */
    public static boolean isInArray(String str, String[] strArray) {
        boolean res = false;
        for (String s : strArray) {
            res = res || s.equalsIgnoreCase(str);
        }
        return res;
    }

    /**
     *
     * @param string
     * @param searchStrings
     * @return
     */
    public static boolean endsWithAnyIgnoreCase(CharSequence string, CharSequence... searchStrings) {
        if ((isEmpty(string)) || (ArrayUtils.isEmpty(searchStrings))) {
            return false;
        }
        for (CharSequence searchString : searchStrings) {
            if (endsWithIgnoreCase(string, searchString)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param txt
     * @return
     */
    public static String firstCharacterUpercase(String txt) {
        if (txt != null && isNotBlank(txt)) {
            return txt.substring(0, 1).toUpperCase() + txt.substring(1, txt.length());
        } else {
            return txt;
        }
    }

    /**
     *
     * @param txt
     * @return
     */
    public static String toCapitalice(String txt) {
        if (!(txt != null && isNotBlank(txt))) {
            return txt;
        }
        String res = "";
        StringTokenizer st = new StringTokenizer(txt, " ");
        while (st.hasMoreTokens()) {
            String aux = firstCharacterUpercase(st.nextToken());
            res += aux + " ";
        }
        res = res.substring(0, res.length() - 1);
        return res;
    }

}
