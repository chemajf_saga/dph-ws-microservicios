/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphgaleria
 * File DPHGaleriaController.java
 * Created at 2 may. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.view.dphgaleria;

import static com.saga.diputacion.huelva.microservicios.config.Constants.AUDIOS;
import static com.saga.diputacion.huelva.microservicios.config.Constants.AUDIO_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.ELTO_TIPO_IMAGEN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.ELTO_TIPO_VIDEO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.HOST_DPH;
import static com.saga.diputacion.huelva.microservicios.config.Constants.HTTP;
import static com.saga.diputacion.huelva.microservicios.config.Constants.IMAGE_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ALTO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ANCHO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DATA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DESCRIPCION;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DESCRIPTION;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DURACION;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ELEMENTOS;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_FICHERO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ID;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_IMAGEN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_HEIGHT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_IMAGE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_TITLE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_LIST_WIDTH;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TESELA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TEXT;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TEXTO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TIPO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TITLE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TITULO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.LOCALE_STR;
import static com.saga.diputacion.huelva.microservicios.config.Constants.PATT_DURATION_HOU;
import static com.saga.diputacion.huelva.microservicios.config.Constants.PATT_DURATION_MIN;
import static com.saga.diputacion.huelva.microservicios.config.Constants.PATT_DURATION_SEC;
import static com.saga.diputacion.huelva.microservicios.config.Constants.PATT_TARGET_AUDIO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.PATT_TARGET_IMAGE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.PATT_TARGET_IMAGE_COVER;
import static com.saga.diputacion.huelva.microservicios.config.Constants.PATT_TARGET_VIDEO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.PATT_TYPE_AUDIO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.TEMPLATE_JSON_GALLERY_MEDIA_FILE_AUDIO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.TEMPLATE_JSON_GALLERY_MEDIA_FILE_IMAGE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.TEMPLATE_JSON_GALLERY_MEDIA_FILE_VIDEO;
import static com.saga.diputacion.huelva.microservicios.config.Constants.VIDEO_TYPE;
import static com.saga.diputacion.huelva.microservicios.config.Constants.W2;
import static com.saga.diputacion.huelva.microservicios.config.Constants.WEB_TV_ID;
import static com.saga.diputacion.huelva.microservicios.config.Constants.WWW;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsResource;
import org.opencms.json.JSONArray;
import org.opencms.json.JSONException;
import org.opencms.json.JSONObject;
import org.opencms.json.XML;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.saga.diputacion.huelva.microservicios.controller.Controller;
import com.saga.diputacion.huelva.microservicios.manager.resources.ResourceContent;
import com.saga.diputacion.huelva.microservicios.util.StringUtils;
import com.saga.diputacion.huelva.microservicios.util.TimeUtils;

/**
 * @author chema.jimenez
 * @since 2 may. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
@SuppressWarnings("unchecked")
public class DPHGaleriaController extends Controller {
    private Log LOG = CmsLog.getLog(DPHGaleriaController.class);

    /**
     * @param context
     * @param req
     * @param res
     */
    public DPHGaleriaController(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /**
     *
     * @return Mapa con los atributos del recurso a crear/modificar
     * @throws Exception
     */
    protected HashMap<String, Object> generateAttributesMap() throws Exception {
        // Recuperamos el array de Elementos
        JSONArray jSONObjectElementos = this.getJsonObject().getJSONObject(JSON_DATA).getJSONArray(JSON_ELEMENTOS);
        // Una vez que los tenemos los eliminamos del JSON
        this.getJsonObject().getJSONObject(JSON_DATA).remove(JSON_ELEMENTOS);
        // Recogemos los datos de los elementos
        if (jSONObjectElementos.length() > 0) {
            this.LOG.debug(" - Tenemos " + jSONObjectElementos.length() + " elementos de galeria");
            this.LOG.debug(" ");
            // Creamos un JSONArray del tamaño de elementos
            JSONArray jasonArrayGalleryMediaFile = new JSONArray();
            int contVideo = 0, contAudio = 0, contImg = 0;

            for (int i = 0; i < jSONObjectElementos.length(); i++) {
                try {
                    JSONObject elto = jSONObjectElementos.getJSONObject(i);
                    String tipoElto = elto.getString(JSON_TIPO);
                    String tituloElto = elto.getString(JSON_TITULO);
                    String jsonTemplate = "";
                    if (ELTO_TIPO_VIDEO.equalsIgnoreCase(tipoElto)) {
                        // Segun el fichero sabemos si es Video o Audio
                        String fichero = elto.getString(JSON_FICHERO);
                        boolean created = false;
                        if (this.isVideo(fichero)) {
                            contVideo++;
                            // VIDEO
                            jsonTemplate = TEMPLATE_JSON_GALLERY_MEDIA_FILE_VIDEO;
                            // Si estamos creando Web TV con id 2116, NO descargamos el video, si no que lo enlazamos
                            // directamente en el recurdo
                            String ficheroCreated = "";
                            if (this.getJsonObject().getString(JSON_ID).equalsIgnoreCase(WEB_TV_ID)) {
                                fichero = this.prepareUrlVideoToW2(fichero);
                                this.LOG.debug(" - Video: " + fichero);
                                this.LOG.debug(" - Al tratarse de la galeria 2113, no se descarga el video si no que se enlaza");
                                ficheroCreated = (fichero);
                            } else {
                                ficheroCreated = this.createFileInOpenCms(fichero, VIDEO_TYPE);
                            }
                            if (StringUtils.isNotBlank(ficheroCreated)) {
                                created = true;
                                jsonTemplate = jsonTemplate.replace(PATT_TARGET_VIDEO, ficheroCreated);
                                String imgCreated = this.createFileInOpenCms(elto.getString(JSON_IMAGEN), IMAGE_TYPE);
                                jsonTemplate = jsonTemplate.replace(PATT_TARGET_IMAGE_COVER, imgCreated);
                            } else {
                                created = false;
                            }
                        } else {
                            // AUDIO
                            contAudio++;
                            jsonTemplate = TEMPLATE_JSON_GALLERY_MEDIA_FILE_AUDIO;
                            String ficheroCreated = this.createFileInOpenCms(fichero, AUDIO_TYPE);
                            if (StringUtils.isNotBlank(ficheroCreated)) {
                                created = true;
                                jsonTemplate = jsonTemplate.replace(PATT_TYPE_AUDIO, this.getTypeAudio(fichero));
                                jsonTemplate = jsonTemplate.replace(PATT_TARGET_AUDIO, ficheroCreated);
                            } else {
                                created = false;
                            }
                        }
                        if (created) {
                            String[] duracion = (StringUtils.isNotBlank(elto.getString(JSON_DURACION))) ? this.duracion(elto.getString(JSON_DURACION))
                                            : new String[3];
                            jsonTemplate = jsonTemplate.replace(PATT_DURATION_HOU, duracion[0]);
                            jsonTemplate = jsonTemplate.replace(PATT_DURATION_MIN, duracion[1]);
                            jsonTemplate = jsonTemplate.replace(PATT_DURATION_SEC, duracion[2]);
                        } else {
                            jsonTemplate = "";
                        }
                    } else {
                        if (ELTO_TIPO_IMAGEN.equalsIgnoreCase(tipoElto)) {
                            contImg++;
                            jsonTemplate = TEMPLATE_JSON_GALLERY_MEDIA_FILE_IMAGE;
                            String imgcreated = this.createFileInOpenCms(elto.getString(JSON_IMAGEN), IMAGE_TYPE);
                            if (StringUtils.isNotBlank(imgcreated)) {
                                jsonTemplate = jsonTemplate.replace(PATT_TARGET_IMAGE, imgcreated);
                            } else {
                                jsonTemplate = "";
                            }
                        } else {
                            // Nada que hacer
                        }
                    }
                    if (StringUtils.isNotBlank(jsonTemplate)) {
                        // Creamos el objeto JSONObject
                        JSONObject jsonAux = new JSONObject(jsonTemplate);
                        jsonAux.put(JSON_DESCRIPTION, elto.getString(JSON_DESCRIPCION));
                        jsonAux.put(JSON_TITLE, tituloElto);
                        jasonArrayGalleryMediaFile.put(jsonAux);
                    }

                } catch (Exception e) {
                    this.LOG.error("Error creando elemento: " + e);
                }
            }
            this.LOG.debug(" - Elementos de la galeria tratados: " + contImg + " imagenes, " + contVideo + " videos y " + contAudio + " audios");

            // Añadimos el array de GalleryMediaFile a data
            this.getJsonObject().getJSONObject(JSON_DATA).put("GalleryMediaElement", jasonArrayGalleryMediaFile);

            // data.put("GalleryMediaElement", jasonArrayGalleryMediaFile);
        } else {
            this.LOG.debug(" - NO Tenemos elementos de galeria");

        }

        String textHtml = this.repairSrcInHtml(this.getJsonObject().getJSONObject(JSON_DATA).getString(JSON_TEXT));
        this.getJsonObject().getJSONObject(JSON_DATA).put(JSON_TEXT, textHtml);

        String jsonData = this.getJsonObject().getJSONObject(JSON_DATA).toString();
        HashMap<String, Object> data = new ObjectMapper().readValue(jsonData, (new HashMap<String, Object>()).getClass());

        // Informacion de la TESELA
        JSONObject tesela = this.getJsonObject().getJSONObject(JSON_TESELA);
        data.put(JSON_LIST_TITLE, tesela.getString(JSON_TEXTO));

        // Tratamos la imgen de la tesela
        String teselaImgOrigin = tesela.getString(JSON_IMAGEN);
        String teselaImgCreated = this.createFileInOpenCms(teselaImgOrigin, IMAGE_TYPE);
        data.put(JSON_LIST_IMAGE, teselaImgCreated);

        // Hay que controlar el ancho y alto
        data.put(JSON_LIST_WIDTH, (tesela.getInt(JSON_ANCHO) > 4 ? 4 : tesela.getInt(JSON_ALTO)) + "");
        data.put(JSON_LIST_HEIGHT, (tesela.getInt(JSON_ALTO) > 2 ? 2 : tesela.getInt(JSON_ALTO)) + "");

        return data;
    }

    /**
     * @param urlVichero
     * @return
     */
    protected String prepareUrlVideoToW2(String urlVichero) {
        if (urlVichero.startsWith(WWW)) {
            urlVichero = HTTP + urlVichero;
        } else {
            if (urlVichero.startsWith("/")) {
                urlVichero = HTTP + HOST_DPH + urlVichero;
            }
        }
        if (urlVichero.contains(WWW)) {
            urlVichero = urlVichero.replaceAll(WWW, W2);
        }
        return urlVichero;
    }

    /**
     * @param file
     * @return
     */
    protected String getTypeAudio(String file) {
        String key = (file.split("\\.")[file.split("\\.").length - 1]).toLowerCase();
        return AUDIOS.get(key);
    }

    /**
     * @param duracionStr
     * @return
     */
    protected String[] duracion(String duracionStr) {
        return TimeUtils.getArrayDurationFromSecons(duracionStr);
    }

    /**
     * @param typeTargetContenido
     * @param resourceFileName
     * @param data
     * @throws CmsException
     * @throws UnsupportedEncodingException
     * @throws JSONException
     * @throws Exception
     */
    protected void saveDPHGaleriaResource(String typeTargetContenido, String resourceFileName, HashMap<String, Object> data, boolean exists,
                    String titleProp) throws CmsException, UnsupportedEncodingException, JSONException, Exception {
        CmsResource resource;
        ResourceContent resourceContent;
        resource = this.getResourceManager().save(data, resourceFileName, typeTargetContenido, false, LOCALE_STR);
        resourceContent = new ResourceContent(this.getCmsObjectAdmin(), resource, LOCALE_STR);

        if (exists) {
            this.LOG.debug(" - Recurso [" + resourceFileName + "] modificado");
        } else {
            this.LOG.debug(" - Recurso [" + resourceFileName + "] creado");
        }
        // En este punto tenemos el recurso creado.


        // Le añadimos las categorías
        this.addMunicipios(resourceContent);

        // Metemos la tematica en funcion de la Zona
        this.addTematicaForZona(resourceContent, null);

        // Tratamos los contenidos relacionados
        this.addRelatedContent(resourceContent);

        // Guardamos los cambios efectuados
        resourceContent.saveXml();

        // Le añadimos el título
        if (StringUtils.isNotBlank(titleProp)) {
            this.addCustomTitleProperty(resourceFileName, titleProp);
        }

        // Guardamos JSON para la respuesta

        // Si todo ha ido bien, publicamos
        OpenCms.getPublishManager().publishResource(this.getCmsObjectAdmin(), resourceFileName);
        this.setJsonObjectResponse(XML.toJSONObject(resourceContent.getXmlContent().toString()));
        // Esperamos la publicacion
        OpenCms.getPublishManager().waitWhileRunning();
        this.LOG.debug(" ");
    }

}
