/**
 * Package com.saga.diputacion.huelva.microservicios.util
 * File TimeUtils.java
 * Created at 3 may. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.util;

import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

/**
 * Clase con utilizades para calcular el tiempo de los archivos multimedia
 *
 * @author chema.jimenez
 * @since 3 may. 2018
 *
 */
public class TimeUtils {

    /**
     * @param duracionStr
     * @return
     */
    public static String[] getArrayDurationFromSecons(String duracionStr) {
        String[] res = new String[3];
        if (StringUtils.isNotBlank(duracionStr)) {
            try {
                int duracionsec = Integer.parseInt(duracionStr);
                GregorianCalendar gc = new GregorianCalendar(0, 0, 0, 0, 0, duracionsec);
                res[0] = "" + gc.get(GregorianCalendar.HOUR);
                res[1] = "" + gc.get(GregorianCalendar.MINUTE);
                res[2] = "" + gc.get(GregorianCalendar.SECOND);
            } catch (Exception e) {
            }
        }
        return res;
    }

    /**
     *
     * @param millis
     * @return
     */
    public static String calculateTimesFromMilis(long millis) {
        long days = TimeUnit.MILLISECONDS.toDays(millis);
        millis -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

        StringBuilder sb = new StringBuilder(64);
        if (days > 0) {
            sb.append(days);
            if (days == 1) {
                sb.append(" día ");
            } else {
                sb.append(" días ");
            }
        }
        if (hours > 0) {
            sb.append(hours);
            if (hours == 1) {
                sb.append(" hora ");
            } else {
                sb.append(" horas ");
            }
        }
        if (minutes > 0) {
            sb.append(minutes);
            if (minutes == 1) {
                sb.append(" minuto");
            } else {
                sb.append(" minutos");
            }
            if (seconds > 0) {
                sb.append(" y ");
            }
        } else {
            //sb.append("00:");
        }
        if (seconds > 0) {
            if (("" + seconds).length() == 1) {
                sb.append("0" + seconds);
            } else {
                sb.append(seconds);
            }
            sb.append(" segundos");
        }
        return (sb.toString());
    }

}
