/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphenlace
 * File DPHEnlaceControllerDelete.java
 * Created at 30 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.view.dphenlace;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

/**
 * @author chema.jimenez
 * @since 30 abr. 2018
 * 
 *        <pre>
 *
 *        </pre>
 */
public class DPHEnlaceControllerDelete extends DPHEnlaceController {

    /**
     * @param context
     * @param req
     * @param res
     */
    public DPHEnlaceControllerDelete(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.saga.diputacion.huelva.microservicios.controller.Controller#execute()
     */
    @Override
    public void execute() throws Exception {
        this.delete();
    }

}
