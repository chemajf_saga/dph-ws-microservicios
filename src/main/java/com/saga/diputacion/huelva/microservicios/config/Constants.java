/**
 * Package com.saga.diputacion.huelva.microservicios
 * File Constants.java
 * Created at 13 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.config;

import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Clase con constantes comunes al WebService
 *
 * @author chema.jimenez
 * @since 13 abr. 2018
 */
public class Constants {

    public static final Locale LOCALE_ES =  new Locale("es_ES");

    public static final String W2 = "w2";
    public static final String WWW = "www";
    public static final String HTTP = "http://";
    public static final String HOST_DPH = "www.diphuelva.es";
    public static final String HOST_DPH_WITH_HTTP = HTTP + HOST_DPH;

    /**
     * Metodo de llamada permitido
     */
    public static final String REQ_METHOD_OK = "post";
    public static final String ZERO = "0";
    public static final String SRC = "src=";
    public static final String HREF = "href=";
    public static final String LOCALE_STR = "es";

    /** Recursos */
    public static final String TYPE_EVENTO = "dphevento";
    public static final String TYPE_PROGRAMA = "dphprograma";

    /** WEB-TV ID */
    public static final String WEB_TV_ID = "2116";

    /** JSON */
    public static final String JSON_TYPE = "Type";
    public static final String JSON_ID = "Id";
    public static final String JSON_ZONA = "Zona";
    public static final String JSON_DATA = "Data";
    public static final String JSON_TITLE = "Title";
    public static final String JSON_HREF = "Href";
    public static final String JSON_LINK = "Link";
    public static final String JSON_CONTENT = "Content";

    public static final String JSON_DESCRIPTION = "Description";
    public static final String JSON_LIST_TITLE = "ListTitle";
    public static final String JSON_LIST_IMAGE = "ListImage";
    public static final String JSON_LIST_WIDTH = "ListWidth";
    public static final String JSON_LIST_HEIGHT = "ListHeight";
    public static final String JSON_LINKS_GALLERY = "LinksGallery";
    public static final String JSON_RELATED_CONTENT = "RelatedContent";
    public static final String JSON_OPTION = "Option";

    public static final String JSON_SUBTITLE = "SubTitle";
    public static final String JSON_PROGRAM = "Program";
    public static final String JSON_MEDIA = "Media";
    public static final String JSON_IMAGE_MAIN = "ImageMain";
    public static final String JSON_IMAGE = "Image";
    public static final String JSON_GALLERY = "Gallery";
    public static final String JSON_FILE = "File";
    public static final String JSON_ATTACHMENT = "Attachment";
    public static final String JSON_DATE = "Date";
    public static final String JSON_TEXT = "Text";

    public static final String JSON_ENTRADILLA = "Entradilla";
    public static final String JSON_PROGRAMA = "Programa";
    public static final String JSON_FICHA_EVENTO = "FichaEvento";
    public static final String JSON_TEASER = "Teaser";
    public static final String JSON_REMARKS = "Remarks";
    public static final String JSON_MAS_INFO = "MasInfo";
    public static final String JSON_VALUE = "Value";
    public static final String JSON_AVAILABILITY = "Availability";
    public static final String JSON_EXPIRATION = "Expiration";
    public static final String JSON_DESTACADOPORTADA = "DestacadoPortada";
    public static final String JSON_TAGS = "Tags";
    public static final String JSON_ELEMENTOS = "Elementos";
    public static final String JSON_TEMATICA = "Tematica";

    public static final String JSON_TIPO = "Tipo";
    public static final String JSON_FICHERO = "Fichero";
    public static final String JSON_TITULO = "Titulo";
    public static final String JSON_DESCRIPCION = "Descripcion";
    public static final String JSON_DURACION = "Duracion";

    public static final String JSON_MUNICIPIO = "Municipio";
    public static final String JSON_TESELA = "Tesela";
    public static final String JSON_TEXTO = "Texto";
    public static final String JSON_ANCHO = "Ancho";
    public static final String JSON_ALTO = "Alto";
    public static final String JSON_IMAGEN = "Imagen";
    public static final String JSON_RELACIONADOS = "Relacionados";
    public static final String JSON_ENLACES = "Enlaces";
    public static final String JSON_DOCUMENTOS = "Documentos";
    public static final String JSON_NOMBRE = "Nombre";
    public static final String JSON_URL = "URL";
    public static final String JSON_CONTENT_RESPONSE_PATTERN = "XXXPATTERNJSONRESPONSEXXX";
    public static final String JSON_CODE_RESPONSE_PATTERN = "XXXPATTERNJSONRESPONSECODEXXX";
    public static final String JSON_FECHA = "Fecha";

    /** Default types */
    public static final String POINTER_TYPE = "pointer";
    public static final String IMAGE_TYPE = "image";
    public static final String BINARY_TYPE = "binary";
    public static final String VIDEO_TYPE = "video";
    public static final String AUDIO_TYPE = "audio";
    public static final String DOCUMENT_TYPE = "binary";
    public static final String FOLDER_TYPE = "folder";
    public static final String PLAIN_TYPE = "plain";
    public static final String JSP_TYPE = "jsp";
    public static final String CONTAINERPAGE_TYPE = "containerpage";
    public static final String HTML_REDIRECT_TYPE = "htmlredirect";
    public static final String IMAGE_GALLERY_TYPE = "imagegallery";
    public static final String DOWNLOAD_GALLERY_TYPE = "downloadgallery";
    public static final String LINK_GALLERY_TYPE = "linkgallery";
    public static final String SITEMAP_CONFIG_TYPE = "sitemap_config";
    public static final String CONTENT_FOLDER_TYPE = "content_folder";
    public static final String SUBSITEMAP_TYPE = "subsitemap";
    public static final String INHERITANCE_GROUP_TYPE = "inheritance_group";
    public static final String INHERITANCE_CONFIG_TYPE = "inheritance_config";

    public static final String GALLERIES = "/.galleries/";
    public static final String IMAGES_GALLERIES_ORIGIN = "Imagenes";
    public static final String VIDEOS_GALLERIES_ORIGIN = "Multimedia";
    public static final String FICHEROS_GALLERIES_ORIGIN = "Ficheros";
    public static final String DOCUMENTOS_GALLERIES_ORIGIN = "Documentos";
    public static final String AGENDA_GALLERIES_ORIGIN = "Agenda";

    public static final String TIPO_BASICO = "B";
    public static final String TIPO_NOTICIA = "N";
    public static final String TIPO_ENLACE = "E";
    public static final String TIPO_GALERIA = "G";
    public static final String TIPO_PROGRAMA = "P";
    public static final String TIPO_ACTIVIDAD = "A";

    public static final String ELTO_TIPO_VIDEO = "Video";
    public static final String ELTO_TIPO_IMAGEN = "Imagen";

    public static final String[] TIPOS_VIDEO = { "mp4", "mov", "avi", "mpg", "mpeg", "wmv", "rm", "flv", "mkv", "divx", "div", "xvid" };

    public static final String[] TIPOS_AUDIO = { "mp3", "wav", "ogg" };

    public static final String[] TIPOS_DOC = { "pdf", "csv", "doc", "docx", "xls", "xlsx", "txt", "ppt", "pptx", "odt", "ods", "odp", "odb", "odg" };

    public static final String[] TIPOS_IMG = { "tif", "tiff", "bmp", "gif", "jpg", "jpeg", "jif", "jfif", "png" };


    public static final String[] EXT_VIDEOS = { ".mp4", ".mov", ".avi", ".mpg", ".mpeg", ".wmv", ".rm", ".flv", ".mkv", ".divx", ".div", ".xvid" };

    public static final String[] EXT_AUDIO = { ".mp3", ".wav", ".ogg" };

    public static final String[] EXT_DOC = { ".pdf", ".csv", ".doc", ".docx", ".xls", ".xlsx", ".txt", ".ppt", ".pptx", ".odt", ".ods", ".odp", ".odb", ".odg" };

    public static final String[] EXT_IMG = { ".tif", ".tiff", ".bmp", ".gif", ".jpg", ".jpeg", ".jif", ".jfif", ".png" };

    public static Map<String, String> AUDIOS = new HashMap<>();
    static {
        Map<String, String> auxMap = new HashMap<>();
        auxMap.put("mp3", "FileMp3");
        auxMap.put("wav", "FileWav");
        auxMap.put("ogg", "FileOgg");
        AUDIOS = Collections.unmodifiableMap(auxMap);
    }

    public static final String PATT_TARGET_IMAGE = "XXXPATTTARGETIMAGEXXX";
    public static final String PATT_TARGET_IMAGE_COVER = "XXXPATTTARGETIMAGECOVERXXX";
    public static final String PATT_TARGET_VIDEO = "XXXPATTTARGETVIDEOXXX";
    public static final String PATT_TARGET_AUDIO = "XXXPATTTARGETAUDIOXXX";
    public static final String PATT_TYPE_AUDIO = "XXXPATTTYPEAUDIOXXX";
    public static final String PATT_DURATION_SEC = "XXXPATTDURATIONSECXXX";
    public static final String PATT_DURATION_MIN = "XXXPATTDURATIONMINXXX";
    public static final String PATT_DURATION_HOU = "XXXPATTDURATIONHOUXXX";
    public static final String PATT_OPTION = "XXXPATTOPTIONXXX";

    public static final String TEMPLATE_JSON_GALLERY_MEDIA_FILE_IMAGE =
                    "{\"GalleryMediaFile\":{\"Image\":{\"Image\":\"" + PATT_TARGET_IMAGE + "\"}}}";

    public static final String TEMPLATE_JSON_GALLERY_MEDIA_FILE_VIDEO =
                    "{\"GalleryMediaFile\":{\"Video\":" + "{\"ImageCover\":\"" + PATT_TARGET_IMAGE_COVER + "\",\"Duration\":{\"Seconds\":\"" + PATT_DURATION_SEC + "\",\"Hours\":\"" + PATT_DURATION_HOU
                    + "\",\"Minutes\":\"" + PATT_DURATION_MIN + "\"},\"FileMp4\":\"" + PATT_TARGET_VIDEO + "\"}}}";

    public static final String TEMPLATE_JSON_GALLERY_MEDIA_FILE_AUDIO = "{\"GalleryMediaFile\":{\"Audio\":{\"Source\":{\"" + PATT_TYPE_AUDIO + "\":\""
                    + PATT_TARGET_AUDIO + "\"},\"Duration\":{\"Seconds\":\"" + PATT_DURATION_SEC + "\",\"Hours\":\"" + PATT_DURATION_HOU
                    + "\",\"Minutes\":\"" + PATT_DURATION_MIN + "\"}}}}";

    public static final String TEMPLATE_JSON_CONTENT_GALLERY_LINKS_GALLERY =
                    "{\"" + JSON_LINKS_GALLERY + "\": {\"" + JSON_OPTION + "\": {" + PATT_OPTION + "}}}";
}
