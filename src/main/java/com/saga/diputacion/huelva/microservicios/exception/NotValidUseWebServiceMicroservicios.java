/**
 * Package com.saga.diputacion.huelva.microservicios.exception
 * File NotValidUseWebServiceMicroservicios.java
 * Created at 13 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.exception;

/**
 * @author chema.jimenez
 * @since 13 abr. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class NotValidUseWebServiceMicroservicios extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -6785851288024667535L;

    /**
     *
     */
    public NotValidUseWebServiceMicroservicios() {
        super();
    }

    /**
     * @param message
     */
    public NotValidUseWebServiceMicroservicios(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public NotValidUseWebServiceMicroservicios(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public NotValidUseWebServiceMicroservicios(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public NotValidUseWebServiceMicroservicios(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
