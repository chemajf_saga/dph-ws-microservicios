/**
 * Package com.saga.diputacion.huelva.microservicios.manager
 * File ResourceContent.java
 * Created at 17 abr. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.manager.resources;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.i18n.CmsEncoder;
import org.opencms.loader.CmsLoaderException;
import org.opencms.main.CmsException;
import org.opencms.main.OpenCms;
import org.opencms.xml.CmsXmlContentDefinition;
import org.opencms.xml.CmsXmlEntityResolver;
import org.opencms.xml.CmsXmlException;
import org.opencms.xml.CmsXmlUtils;
import org.opencms.xml.content.CmsXmlContent;
import org.opencms.xml.content.CmsXmlContentFactory;
import org.opencms.xml.types.I_CmsXmlContentValue;
import org.xml.sax.SAXException;

/**
 * @author chema.jimenez
 * @since 17 abr. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class ResourceContent {

    CmsObject cmso;
    String path;
    Locale locale;
    CmsFile file;
    String strContent;
    public CmsXmlContent xmlContent;
    CmsXmlEntityResolver resolver;

    public ResourceContent() {
    }

    /**
     * Initialize content. The resource must be locked by user before change content. Plain objects have no xmlContent
     * but strContent is still readable.
     *
     * @param cmso
     * @param resourcePath
     */
    public ResourceContent(CmsObject cmso, String resourcePath) throws CmsException, UnsupportedEncodingException {
        this.init(cmso, resourcePath, cmso.getRequestContext().getLocale());
    }

    /**
     * Initialize content. The resource must be locked by user before change content. Plain objects have no xmlContent
     * but strContent is still readable.
     *
     * @param cmso
     * @param resource
     */
    public ResourceContent(CmsObject cmso, CmsResource resource) throws CmsException, UnsupportedEncodingException {
        this.init(cmso, cmso.getSitePath(resource), cmso.getRequestContext().getLocale());
    }

    /**
     * Initialize content. The resource must be locked by user before change content. Plain objects have no xmlContent
     * but strContent is still readable.
     *
     * @param cmso
     * @param resourcePath
     * @param locale
     */
    public ResourceContent(CmsObject cmso, String resourcePath, Locale locale) throws CmsException, UnsupportedEncodingException {
        this.init(cmso, resourcePath, locale);
    }

    /**
     * Initialize content. The resource must be locked by user before change content. Plain objects have no xmlContent
     * but strContent is still readable.
     *
     * @param cmso
     * @param resourcePath
     * @param localeStr
     */
    public ResourceContent(CmsObject cmso, String resourcePath, String localeStr) throws CmsException, UnsupportedEncodingException {
        Locale locale = new Locale(localeStr);
        this.init(cmso, resourcePath, locale);
    }

    /**
     * Initialize content. The resource must be locked by user before change content. Plain objects have no xmlContent
     * but strContent is still readable.
     *
     * @param cmso
     * @param resource
     * @param localeStr
     */
    public ResourceContent(CmsObject cmso, CmsResource resource, String localeStr) throws CmsException, UnsupportedEncodingException {
        String resourcePath = cmso.getSitePath(resource);
        Locale locale = new Locale(localeStr);
        this.init(cmso, resourcePath, locale);
    }

    /**
     * Initialize content. The resource must be locked by user before change content. Plain objects have no xmlContent
     * but strContent is still readable.
     *
     * @param cmso
     * @param resource
     * @param locale
     */
    public ResourceContent(CmsObject cmso, CmsResource resource, Locale locale) throws CmsException, UnsupportedEncodingException {
        String resourcePath = cmso.getSitePath(resource);
        this.init(cmso, resourcePath, locale);
    }

    /**
     * Initialize content. The resource must be locked by user before change content. Plain objects throw exception but
     * strContent is readable.
     *
     * @param cmso
     * @param path
     * @param locale
     */
    private void init(CmsObject cmso, String path, Locale locale) throws CmsException, UnsupportedEncodingException {
        this.cmso = cmso;
        this.path = path;
        this.locale = locale;

        // Leemos el recurso
        this.file = cmso.readFile(path, CmsResourceFilter.ALL);
        this.strContent = new String(this.file.getContents(), CmsEncoder.ENCODING_UTF_8);
        this.resolver = new CmsXmlEntityResolver(cmso);
        try {
            this.unmarshall(this.file);
        } catch (Exception e) {
            // throw new Exception("unmarshalled failed for resource $path".toString())
            this.xmlContent = null;
        }
    }

    /**
     *
     * @param strContent
     * @return
     * @throws UnsupportedEncodingException
     * @throws CmsException
     */
    public ResourceContent update(String strContent) throws UnsupportedEncodingException, CmsException {
        this.strContent = strContent;
        this.file.setContents(strContent.getBytes(CmsEncoder.ENCODING_UTF_8));
        if (this.xmlContent != null) {
            this.unmarshall(this.file);
            this.repair();
        }
        return this;
    }

    /**
     *
     * @param file
     * @return
     * @throws CmsXmlException
     */
    public ResourceContent unmarshall(CmsFile file) throws CmsXmlException {
        this.xmlContent = CmsXmlContentFactory.unmarshal(this.cmso, file);
        return this;
    }

    /**
     *
     * @return
     * @throws CmsXmlException
     */
    public ResourceContent unmarshall() throws CmsXmlException {
        this.xmlContent = CmsXmlContentFactory.unmarshal(this.cmso, this.strContent, CmsEncoder.ENCODING_UTF_8, this.resolver);
        return this;
    }

    /**
     *
     * @return
     * @throws CmsXmlException
     * @throws UnsupportedEncodingException
     */
    public ResourceContent marshall() throws CmsXmlException, UnsupportedEncodingException {
        this.strContent = new String(this.xmlContent.marshal(), CmsEncoder.ENCODING_UTF_8);
        return this;
    }

    /**
     * Obtiene la definición del contenido destino al que se va a mapear by: rtinoco
     *
     * @param type
     * @return
     */
    CmsXmlContentDefinition contentDefinition(String type) throws CmsLoaderException, CmsXmlException, SAXException, IOException {
        String schemaUri = "opencms:/${resType.configuration.schema}";
        return CmsXmlContentDefinition.unmarshal(schemaUri, new CmsXmlEntityResolver(this.cmso));
    }

    /**
     * Asegura un valor en el path indicado, devolviendo uno existente o creandolo
     *
     * @param path
     * @param pos
     * @return
     */
    public I_CmsXmlContentValue procureValue(String path, int pos) throws Exception {
        if (this.xmlContent == null) {
            throw new Exception("XML content not exists for resource $path".toString());
        }
        if (this.xmlContent.hasValue(path, this.locale, pos)) {
            return this.xmlContent.getValue(path, this.locale, pos);
        } else {

            // Si no existe aseguramos que el padre exista
            if (CmsXmlUtils.isDeepXpath(path)) {
                String parentPath = CmsXmlUtils.createXpath(CmsXmlUtils.removeLastXpathElement(path), 1);
                return this.procureValue(parentPath, this.obtainNodeIndex(parentPath));
            }

            // Comprobamos que al crear el padre no se haya creado automaticamente el hijo
            if (this.xmlContent.hasValue(path, this.locale, pos)) {
                return this.xmlContent.getValue(path, this.locale, pos);
            } else {
                return this.xmlContent.addValue(this.cmso, path, this.locale, pos);
            }
        }
    }

    /**
     *
     * @param path
     * @return
     */
    private int obtainNodeIndex(String path) {
        int index = CmsXmlUtils.getXpathIndexInt(path);
        if (index > 0) {
            index = index - 1;
        }
        return index;
    }

    /**
     * Asegura un valor en el path indicado, devolviendo uno existente o creandolo
     *
     * @param path
     */
    public I_CmsXmlContentValue procureValue(String path) throws Exception {
        return this.procureValue(path, 0);
    }

    /**
     *
     * @param path
     * @return
     * @throws Exception
     */
    public ResourceContent setStringValue(String path) throws Exception {
        return this.setStringValue(path, "", 0);
    }

    /**
     * Set value for element path.
     *
     * @param path
     * @param value
     * @return
     */
    public ResourceContent setStringValue(String path, String value) throws Exception {
        return this.setStringValue(path, value, 0);
    }

    /**
     * Set value for element path. If the element does not exist, procure new element and set value.
     *
     * @param path
     * @param value
     * @param pos
     * @return
     */
    public ResourceContent setStringValue(String path, String value, int pos) throws Exception {
        I_CmsXmlContentValue content = this.procureValue(path, pos);
        content.setStringValue(this.cmso, value);
        return this;
    }

    /**
     *
     * @param path
     * @return
     * @throws Exception
     */
    public ResourceContent removeAllValue(String path) throws Exception {
        int size = this.xmlContent.getValues(path, this.locale).size();
        if (size == 1) {
            this.setStringValue(path, "");
        } else {
            for (int i = 0; i < size; i++) {
                this.xmlContent.removeValue(path, this.locale, 0);
            }
        }
        return this;
    }

    /**
     *
     * @param path
     * @return
     * @throws Exception
     */
    public ResourceContent removeAllValueAlmostOne(String path) throws Exception {
        int size = this.xmlContent.getValues(path, this.locale).size();
        if (size == 1) {
            this.setStringValue(path, "");
        } else {
            for (int i = 0; i < size; i++) {
                if (i == size - 1) {
                    this.setStringValue(path, "");
                } else {
                    this.xmlContent.removeValue(path, this.locale, 0);
                }
            }
        }
        return this;

    }

    /**
     *
     * @param path
     * @param pos
     * @return
     * @throws Exception
     */
    public ResourceContent removeValue(String path, int pos) throws Exception {
        this.xmlContent.removeValue(path, this.locale, pos);
        return this;
    }

    /**
     *
     * @param path
     * @return
     * @throws Exception
     */
    public ResourceContent removeValue(String path) throws Exception {
        int size = this.xmlContent.getValues(path, this.locale).size();
        if (size > 0) {
            this.xmlContent.removeValue(path, this.locale, 0);
        }
        return this;
    }

    /**
     * Create a new element at last position and set value for element path.
     *
     * @param path
     * @param value
     * @return
     */
    public ResourceContent appendStringValue(String path, String value) throws Exception {
        this.setStringValue(path, value, this.count(path));
        return this;
    }

    /**
     * Append category at last position.
     *
     * @param XmlPath
     * @param value
     * @return
     */
    public ResourceContent appendCategoryStringValue(String XmlPath, String value) throws Exception {
        String strValue = this.getStringValue(XmlPath);
        String newVal = "";
        if (StringUtils.isEmpty(strValue)) {
            newVal = value;
        } else {
            newVal = this.getStringValue(XmlPath) + "," + value;
        }
        this.setStringValue(XmlPath, newVal, 0);
        return this;
    }

    /**
     * Set value for element path. If the element does not exist, procure new element and set value.
     *
     * @param pathWithIdx
     * @param value
     * @return
     */
    public ResourceContent setStringValueIdx(String pathWithIdx, String value) throws Exception {
        I_CmsXmlContentValue content = this.procureValueIdx(pathWithIdx);
        content.setStringValue(this.cmso, value);
        return this;
    }

    /**
     * Filling the content with empty tags
     *
     * @param tag
     * @param limit
     */
    public void fillWithEmptyValues(String tag, int limit) throws Exception {
        if (this.xmlContent == null) {
            throw new Exception("xml content not exists for resource $path".toString());
        }
        for (int i = 0; i < limit; i++) {
            if (!this.xmlContent.hasValue(tag, this.locale, i)) {
                this.xmlContent.addValue(this.cmso, tag, this.locale, i);
            }
        }
    }

    /**
     * Saving file changes from String content. For updating xml content execute unmarshall.
     *
     * @return
     * @throws CmsException
     * @throws UnsupportedEncodingException
     */
    public ResourceContent saveStr() throws CmsException, UnsupportedEncodingException {
        ResourceCMS.lock(this.cmso, this.path);
        this.update(this.strContent);
        this.cmso.writeFile(this.file);
        ResourceCMS.unlock(this.cmso, this.path);
        return this;
    }

    /**
     * Saving file changes from String content. For updating xml content execute unmarshall.
     *
     * @param contents
     * @return
     * @throws CmsException
     * @throws UnsupportedEncodingException
     */
    public ResourceContent saveStr(String contents) throws CmsException, UnsupportedEncodingException {
        ResourceCMS.lock(this.cmso, this.path);
        this.update(contents);
        this.cmso.writeFile(this.file);
        ResourceCMS.unlock(this.cmso, this.path);
        return this;
    }

    /**
     * Saving file from Xml content changes. For updating string content execute marshall.
     */
    public ResourceContent saveXml() throws CmsException {
        ResourceCMS.lock(this.cmso, this.path);
        this.file.setContents(this.xmlContent.marshal());
        this.cmso.writeFile(this.file);
        ResourceCMS.unlock(this.cmso, this.path);
        return this;
    }

    /**
     *
     * @return
     * @throws CmsException
     */
    public ResourceContent repair() throws CmsException {
        ResourceCMS.lock(this.cmso, this.path);
        this.xmlContent.setAutoCorrectionEnabled(true);

        // now correct the XML
        this.xmlContent.correctXmlStructure(this.cmso);

        // Prepare for write
        this.xmlContent.getHandler().prepareForWrite(this.cmso, this.xmlContent, this.file);
        ResourceCMS.unlock(this.cmso, this.path);

        return this;
    }

    /**
     * Returns String value from OpenCmsString element
     *
     * @param tag
     * @return
     */
    public String getStringValue(String tag) throws Exception {
        if (this.xmlContent == null) {
            throw new Exception("xml content not exists for resource $path".toString());
        }
        return this.xmlContent.getStringValue(this.cmso, tag, this.locale);
    }

    /**
     * Returns String value from OpenCmsString element
     *
     * @param base Parent path
     * @param tag Tag element
     * @param pos 0 based position
     * @return
     * @throws Exception
     */
    public String getStringValue(String base, String tag, Integer pos) throws Exception {
        String path = base + "/" + tag + "[" + (pos + 1) + "]";
        return this.getStringValue(path);
    }

    /**
     * String value for element defined by path without indexes and list of indexes. For example, "Gallery/Image" and
     * indexes [0, 1] define "Gallery[1]/Image[2]".
     *
     * @param pathNoIdx Element path without index "Gallery/Image"
     * @param idxs List of indexes [0, 1]
     * @return
     */
    public String getStringValueWithIndex(String pathNoIdx, List<Integer> idxs) throws Exception {
        String pathWithIndex = this.generatePathWithIndex(pathNoIdx, idxs);
        return this.getStringValue(pathWithIndex);
    }

    /**
     * String value for element defined by path without indexes and list of indexes. For example, "Gallery/Image" and
     * indexes [0, 1] define "Gallery[1]/Image[2]".
     *
     * @param pathNoIdx Element path without index "Gallery/Image"
     * @param idxs List of indexes [0, 1]
     * @return
     */
    public String getStringValueWithIndex(String pathNoIdx, Integer[] idxs) throws Exception {
        return this.getStringValueWithIndex(pathNoIdx, Arrays.asList(idxs));
    }

    /**
     * Returns String value from OpenCmsHtml element
     *
     * @param element
     * @return
     */
    public String getHtmlStringValue(String element) throws Exception {
        if (this.xmlContent == null) {
            throw new Exception("xml content not exists for resource $path".toString());
        }
        String value = null;
        if (this.xmlContent.hasValue(element, this.locale)) {
            value = this.xmlContent.getValue(element, this.locale).getStringValue(this.cmso);
        }
        return value;
    }

    /**
     * Check if the content has value for element
     *
     * @param element
     * @return
     */
    public boolean hasValue(String element) throws Exception {
        if (this.xmlContent == null) {
            throw new Exception("xml content not exists for resource $path".toString());
        }
        return this.xmlContent.hasValue(element, this.locale);
    }

    /**
     * Check if the content has value for element in the position
     *
     * @param element
     * @param pos
     * @return
     */
    public boolean hasValue(String element, int pos) throws Exception {
        if (this.xmlContent == null) {
            throw new Exception("xml content not exists for resource $path".toString());
        }
        return this.xmlContent.hasValue(element, this.locale, pos);
    }

    /**
     * Add value to content definition
     *
     * @param element
     * @param pos
     * @return
     */
    public I_CmsXmlContentValue addValue(String element, int pos) {
        return this.xmlContent.addValue(this.cmso, element, this.locale, pos);
    }

    /**
     * Add value to content definition
     *
     * @param element
     * @return
     */
    public I_CmsXmlContentValue addValue(String element) {
        return this.xmlContent.addValue(this.cmso, element, this.locale, this.count(element));
    }

    /**
     * Add value to content definition
     *
     * @param element
     * @param value
     * @return
     */
    public ResourceContent addValue(String element, String value) throws Exception {
        I_CmsXmlContentValue xmlValue = this.addValue(element);
        this.setStringValue(element, value, xmlValue.getIndex());
        return this;
    }

    /**
     * Check if the content has value for element in the position
     *
     * @param element
     * @param pos
     * @return
     */
    public I_CmsXmlContentValue getValue(String element, int pos) throws Exception {
        if (this.xmlContent == null) {
            throw new Exception("xml content not exists for resource $path".toString());
        }
        return this.xmlContent.getValue(element, this.locale, pos);
    }

    /**
     * Check if the content has value for element in the position
     *
     * @param element
     * @return
     */
    public I_CmsXmlContentValue getValue(String element) throws Exception {
        return this.getValue(element, 0);
    }

    /**
     * Return list of contents into resource defined by element
     *
     * @param element
     * @return
     */
    public List<I_CmsXmlContentValue> getValues(String element) {
        return this.xmlContent.getValues(element, this.locale);
    }

    /**
     * Check if exists xmlPath content
     *
     * @param xmlPath
     * @param pos
     * @return
     */
    public boolean contains(String xmlPath, int pos) {
        return this.xmlContent.getValue(xmlPath, this.locale, pos) != null;
    }

    /**
     * Check if exists xmlPath content
     *
     * @param xmlPath
     * @return
     */
    public boolean contains(String xmlPath) {
        return this.contains(xmlPath, 0);
    }

    /**
     * Check if exists xmlPath content
     *
     * @param xmlPath
     * @return
     */
    public int count(String xmlPath) {
        return this.xmlContent.getIndexCount(xmlPath, this.locale);
    }

    /**
     * Return map composed by resource identification and content: <structureId@rootPath: <path1: value1, path2: value2,
     * ...>> by: rtinoco
     *
     * @return
     */
    public Map<String, Map<String, String>> export() throws CmsException {
        String cmsVfsFileType = "OpenCmsVfsFile";
        Map<String, Map<String, String>> exp = new LinkedHashMap<>();
        Map<String, String> content = new LinkedHashMap<>();

        final List<String> names = this.elemNames();
        Collections.sort(names);
        final List<String> elemPaths = new ArrayList<>();
        for (int i = 0; i < names.size(); i++) {
            if (i > 0 && names.get(i).startsWith(names.get(i - 1))) {
                elemPaths.remove(elemPaths.size() - 1);
            }
            elemPaths.add(names.get(i));
        }

        for (String elemPath : elemPaths) {
            final I_CmsXmlContentValue value = this.xmlContent.getValue(elemPath, this.locale);
            if (value.isSimpleType()) {
                if (value.getTypeName().equals(cmsVfsFileType)) {
                    String onlineLink = OpenCms.getLinkManager().getOnlineLink(this.cmso, value.getStringValue(this.cmso));
                    content.put(elemPath, onlineLink);
                } else {
                    content.put(elemPath, value.getStringValue(this.cmso));
                }
            }
        }

        CmsResource resource = this.cmso.readResource(this.path);
        exp.put(resource.getStructureId().getStringValue() + "@" + resource.getRootPath(), content);
        return exp;
    }

    /**
     * Return list of elements names
     *
     * @return
     */
    public List<String> elemNames() {
        return this.xmlContent.getNames(this.locale);
    }

    /**
     * Return escape HTML block
     *
     * @param s
     * @return
     */
    public static String escapeHTML(String s) {
        return StringEscapeUtils.escapeHtml4(s);
    }

    /**
     * Return last path value and ensure parent path and brother elements exist
     *
     * @param pathWithIndex Content[2]/Image[4]/Link
     * @return
     */
    public I_CmsXmlContentValue procureValueIdx(String pathWithIndex) throws Exception {
        return this.procureValueIdx(new Path(pathWithIndex));
    }

    /**
     * Return last element value. Ensure parent elements and brother elements exist
     *
     * @param path
     * @return
     */
    public I_CmsXmlContentValue procureValueIdx(Path path) throws Exception {
        I_CmsXmlContentValue value = null;
        for (int elemPos = 0; elemPos < path.elems.size(); elemPos++) {
            for (int idx = 1; idx <= path.elems.get(elemPos).idx; idx++) {
                value = this.procureValueIdx(path.elems.get(elemPos), idx - 1);
            }
        }
        return value;
    }

    /**
     * Procure value for element path and position.
     *
     * @param elem Absolute element path
     * @param pos Element Index less 1
     * @return
     */
    public I_CmsXmlContentValue procureValueIdx(Elem elem, int pos) throws Exception {
        String elemPath = elem.absElemPathWitoutLastIdx();
        if (this.hasValue(elemPath, pos)) {
            return this.getValue(elemPath, pos);
        } else {
            return this.addValue(elemPath, pos);
        }
    }

    /**
     * Generate path for mapping content
     *
     * @param cleanPath
     * @param tarPattIdxs
     * @param srcPattIdxs
     * @param srcIdxValues
     * @return
     */
    String generatePathWithIndex(String cleanPath, List<Integer> tarPattIdxs, List<Integer> srcPattIdxs, List<Integer> srcIdxValues) {
        Path path = new Path(cleanPath);
        for (int i = 0; i < srcPattIdxs.size(); i++) {
            path.elems.get(tarPattIdxs.get(i)).idx = srcIdxValues.get(srcPattIdxs.get(i));
        }
        return path.toString();
    }

    /**
     * Generate path for mapping content
     *
     * @param cleanPath
     * @param srcIdxValues
     * @return
     */
    String generatePathWithIndex(String cleanPath, List<Integer> srcIdxValues) {
        Path path = new Path(cleanPath);
        for (int i = 0; i < srcIdxValues.size(); i++) {
            path.elems.get(i).idx = srcIdxValues.get(i);
        }
        return path.toString();
    }

    /**
     * Path y Elem classes emulate elems path with index. For example: Path: Content[1]/Text[2] Elem: Content[1]
     */
    public class Path {
        public String absPath;
        public List<Elem> elems;

        Path(String path) {
            this.absPath = path;
            this.elems = new ArrayList<>();
            String[] elemsPathParts = path.split("/");

            for (int i = 0; i < elemsPathParts.length; i++) {
                String parent = null;
                if (i > 0) {
                    parent = elemsPathParts[0];
                    for (int j = 1; j < i; j++) {
                        parent = parent + "/" + elemsPathParts[j];
                    }
                }

                String elemPath = elemsPathParts[i];
                String idx = String.valueOf(1);
                if (elemPath.contains("[")) {
                    int idxBegin = elemPath.indexOf("[");
                    idx = elemPath.substring(idxBegin + 1, elemPath.length() - 1);
                    elemPath = elemPath.substring(0, idxBegin);
                }

                this.elems.add(new Elem(idx, elemPath, parent));
            }
        }

        /**
         * Return a list of indexes for path. For example: Content[2]/Image[10]/Image[1] returns [2, 10, 1]
         *
         * @param pathWithIdx
         * @return
         */
        public List<Integer> indexes(String pathWithIdx) {
            List<Integer> idxs = new ArrayList<>();
            String[] parts = pathWithIdx.split("/");
            for (String part : parts) {
                idxs.add(CmsXmlUtils.getXpathIndexInt(part));
            }
            return idxs;
        }

        public List<Integer> indexes() {
            List<Integer> idxs = new ArrayList<>();
            for (int i = 0; i < this.elems.size(); i++) {
                idxs.add(this.elems.get(i).idx);
            }
            return idxs;
        }

        @Override
        public String toString() {
            String res = this.elems.get(0).toString();
            for (int i = 1; i < this.elems.size(); i++) {
                res = res + "/" + this.elems.get(i).toString();
            }
            return res;
        }
    }

    public class Elem {
        public int idx; // XPath index. For example, Gallery[1] idx is 1. Must not be 0.
        public String elemPath;
        public String parent;

        public Elem(Integer idx, String path, String parent) {
            this.idx = idx;
            this.elemPath = path;
            this.parent = parent;
        }

        public Elem(String idx, String path, String parent) {
            this.idx = Integer.valueOf(idx);
            this.elemPath = path;
            this.parent = parent;
        }

        public Elem(String idx, String path) {
            this(idx, path, null);
        }

        /**
         * Return absolute element path
         *
         * @return
         */
        public String absElemPath() {
            if (StringUtils.isNotBlank(this.parent)) {
                return this.parent + "/" + this.elemPath + "[" + this.idx + "]";
            } else {
                return this.elemPath + "[" + this.idx + "]";
            }
        }

        /**
         * Return absolute element path without last index
         *
         * @return
         */
        public String absElemPathWitoutLastIdx() {
            if (StringUtils.isNotBlank(this.parent)) {
                return this.parent + "/" + this.elemPath;
            } else {
                return this.elemPath;
            }
        }

        @Override
        public String toString() {
            return this.elemPath + "[" + this.idx + "]";
        }
    }

    /**
     * @return xmlContent
     */
    public CmsXmlContent getXmlContent() {
        return this.xmlContent;
    }

    /**
     * @param xmlContent the xmlContent to set
     */
    public void setXmlContent(CmsXmlContent xmlContent) {
        this.xmlContent = xmlContent;
    }

}
