/**
 * Package com.saga.diputacion.huelva.microservicios.view.dphevento
 * File DPHEventoControllerCreateUpdate.java
 * Created at 9 may. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.microservicios.view.dphevento;

import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_DATA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ID;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_PROGRAM;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_PROGRAMA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_TEMATICA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.JSON_ZONA;
import static com.saga.diputacion.huelva.microservicios.config.Constants.LOCALE_STR;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import org.apache.commons.logging.Log;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.json.JSONArray;
import org.opencms.json.JSONException;
import org.opencms.json.JSONObject;
import org.opencms.json.XML;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;

import com.saga.diputacion.huelva.microservicios.manager.resources.ResourceContent;
import com.saga.diputacion.huelva.microservicios.util.JSONUtils;

/**
 * @author chema.jimenez
 * @since 9 may. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class DPHEventoControllerCreateUpdate extends DPHEventoController {
    private Log LOG = CmsLog.getLog(DPHEventoControllerCreateUpdate.class);

    /**
     * @param context
     * @param req
     * @param res
     */
    public DPHEventoControllerCreateUpdate(PageContext context, HttpServletRequest req, HttpServletResponse res) {
        super(context, req, res);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.saga.diputacion.huelva.microservicios.controller.Controller#execute()
     */
    @Override
    public void execute() throws Exception {
        super.execute();
        String typeTargetContenido = this.getTargetType();

        // Tenemos que crear el contenido y guardarlo
        String resourceFileName = this.getResourcePathAndName();

        CmsResource resource;
        ResourceContent resourceContent;
        // Si el recurso existe
        boolean exists = this.getCmsObjectAdmin().existsResource(resourceFileName, CmsResourceFilter.ALL);
        if (exists) {
            this.LOG.debug(" - Recurso " + resourceFileName + " ya existe.");
            this.delete();
        } else {
            this.LOG.debug(" - Recurso " + resourceFileName + " no existe.");
        }

        // Creamos el HashMap
        HashMap<String, Object> data = this.generateAttributesMap();
        resource = this.getResourceManager().save(data, resourceFileName, typeTargetContenido, false, LOCALE_STR);
        resourceContent = new ResourceContent(this.getCmsObjectAdmin(), resource, LOCALE_STR);

        if (exists) {
            this.LOG.debug(" - Recurso [" + resourceFileName + "] modificado");
        } else {
            this.LOG.debug(" - Recurso [" + resourceFileName + "] creado");
        }
        // En este punto tenemos el recurso creado.

        // Lo relacionamos con su programa
        this.addProgram(resourceContent);

        // Le añadimos las categorías
        this.addMunicipios(resourceContent);

        // Metemos la tematica en funcion de la Zona
        this.addTematica(resourceContent);

        // Guardamos los cambios efectuados
        resourceContent.saveXml();

        // Guardamos JSON para la respuesta
        this.setJsonObjectResponse(XML.toJSONObject(resourceContent.getXmlContent().toString()));

        // Si todo ha ido bien, publicamos
        OpenCms.getPublishManager().publishResource(this.getCmsObjectAdmin(), resourceFileName);
        // Esperamos la publicacion
        OpenCms.getPublishManager().waitWhileRunning();

    }

    /**
     * @param resourceContent
     * @throws Exception
     */
    private void addProgram(ResourceContent resourceContent) throws Exception {
        JSONObject programa = this.getJsonObject().getJSONObject(JSON_PROGRAMA);
        if (programa != null && programa.length() > 0) {
            String idP = programa.getString(JSON_ID);
            String zonaP = programa.getString(JSON_ZONA);
            // Creamos el path del programa
            String pathP = this.getResourceName(idP, zonaP, "P");
            // Existe el programa?
            boolean exists = this.getCmsObjectAdmin().existsResource(pathP, CmsResourceFilter.ALL);
            if (exists) {
                this.LOG.debug(" - Existe el programa del evento: " + pathP);
                resourceContent.appendStringValue(JSON_PROGRAM, pathP);
            } else {
                this.LOG.warn(" - ¡ATENCION! NO existe el programa del evento: " + pathP);
            }
        } else {
            this.LOG.warn(" - ¡ATENCION! El evento no tiene Programa informado en el JSON");
        }
    }

    /**
     * @param resourceContent
     * @throws Exception
     * @throws CmsException
     * @throws JSONException
     */
    private void addTematica(ResourceContent resourceContent) throws JSONException, CmsException, Exception {
        JSONArray tematicas = this.getJsonObject().getJSONObject(JSON_DATA).getJSONArray(JSON_TEMATICA);
        this.addTematicaForZona(resourceContent, new JSONUtils(tematicas).toList());
    }

}
