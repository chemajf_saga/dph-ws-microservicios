<%@page import="com.google.gson.*"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.opencms.main.CmsLog"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="org.json.XML"%>
<%@page import="com.saga.diputacion.huelva.microservicios.manager.resources.ResourceContent"%>
<%@page import="org.opencms.file.CmsResourceFilter"%>
<%@page import="org.opencms.file.CmsObject"%>
<%@page import="org.opencms.workplace.CmsWorkplaceAction"%>
<%@page import="org.opencms.file.CmsResource"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSON de un recurso</title>
</head>
<body>
	<%
	    Log LOG = CmsLog.getLog("ReadResourceToJson");
	%>
	<div>
		<fieldset>
			Indica el archivo con path absoluto
			<form action="" method="get">
				<input type="text" name="fileName" id="fileName" value="${param.fileName}" style="width: 90%;" /> <input
					type="submit" value="Buscar" />
			</form>
		</fieldset>
	</div>
	<c:choose>
		<c:when test="${not empty param.fileName}">
			<%
			    String resourceFileName = request.getParameter("fileName");
			            String res = "";
			            CmsObject cmsObjectAdmin = CmsWorkplaceAction.getInstance().getCmsAdminObject();
			            CmsResource resource = cmsObjectAdmin.readResource(resourceFileName, CmsResourceFilter.IGNORE_EXPIRATION);
			            if (resource != null) {
			                LOG.debug(" - Leemos el recurso [" + resourceFileName + "]");
			                ResourceContent resourceContent = new ResourceContent(cmsObjectAdmin, resource, "ES");
			                res = XML.toJSONObject(resourceContent.getXmlContent().toString()).toString();
			                pageContext.setAttribute("jsonStr", res);
			                //
			                try {
			                    //Gson
			                    JsonParser parser = new JsonParser();
			                    JsonObject json = parser.parse(   res   ).getAsJsonObject();
			                    
			                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
			                    String prettyJson = gson.toJson(json).toString() ;
			                    //out.print("<br><hr>" + prettyJson);

			                } catch (Exception e) {
			                    out.print("ERROR en el Pretty-Print JSON");
			                    out.print(e);
			                }
			            } else {
			                LOG.error(" - No existe el recurso indicado [" + resourceFileName + "]");
			                throw new Exception("No existe el recurso indicado [" + resourceFileName + "]");
			            }
			%>
		</c:when>
		<c:otherwise></c:otherwise>
	</c:choose>
	<div>${jsonStr}</div>
</body>
</html>