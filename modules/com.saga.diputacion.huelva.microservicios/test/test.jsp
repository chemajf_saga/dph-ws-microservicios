<%@page import="org.apache.http.impl.client.DefaultHttpClient"%>
<%@page import="org.apache.http.client.entity.UrlEncodedFormEntity"%>
<%@page import="java.util.*"%>
<%@page import="org.apache.http.client.methods.*"%>
<%@page import="org.apache.http.client.*"%>
<%@page import="org.apache.http.message.*"%>
<%@page import="org.apache.http.*"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="org.opencms.main.CmsLog"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.net.URL"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" buffer="none" session="false"
	trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="core" tagdir="/WEB-INF/tags/core/templates"%>
<%@ page import="com.saga.diputacion.huelva.microservicios.view.dphcontenido.DPHContenidoControllerCreateUpdate"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Pruebas Microservicios</title>
</head>
<body>
	<%!String USER_AGENT = "Mozilla/5.0";

    private void sendGet(String url) throws Exception {

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);
/*
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());*/

    }

    private void sendPost(String url) throws Exception {

        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);

        // add header
        post.setHeader("User-Agent", USER_AGENT);

        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("sn", "C02G8416DRJM"));
        urlParameters.add(new BasicNameValuePair("cn", ""));
        urlParameters.add(new BasicNameValuePair("locale", ""));
        urlParameters.add(new BasicNameValuePair("caller", ""));
        urlParameters.add(new BasicNameValuePair("num", "12345"));

        post.setEntity(new UrlEncodedFormEntity(urlParameters));

        HttpResponse response = client.execute(post);
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + post.getEntity());
        System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
/*
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        System.out.println(result.toString());
        */

    }
    %>
	<%
	    // http://192.168.1.199:8080/opencms/system/modules/com.saga.diputacion.huelva.microservicios/test/test.jsp
	    // http://192.168.1.199:8080/opencms/dphcontenido/create/index.jsp
	    //	    DPHContenidoControllerCreate dphContenidoControllerCreate = new DPHContenidoControllerCreate(pageContext, request, response);
	    //	dphContenidoControllerCreate.handleRequest();
	    Log LOG = CmsLog.getLog(DPHContenidoControllerCreateUpdate.class);
	    try {
	        URL url = new URL(" http://192.168.1.199:8080/opencms/dphcontenido/create/");
	        HttpURLConnection con = (HttpURLConnection) url.openConnection();

	        // optional default is GET
	        con.setRequestMethod("GET");

	        //add request header
	        con.setRequestProperty("User-Agent", USER_AGENT);

	        con.connect();
	        
	        int responseCode = con.getResponseCode();
	        System.out.println("\nSending 'GET' request to URL : " + url);
	        System.out.println("Response Code : " + responseCode);

	        //URLConnection miUrlCon = url.openConnection();
	        //BufferedReader br = new BufferedReader(new InputStreamReader(miUrlCon.getInputStream()));
	        /* String strstr = br.readLine();
	        while (str != -1) {
	        System.out.println(str);
	        } */

	    } catch (Exception e) {
	        LOG.error("", e);
	    }
	%>
	<form action="http://192.168.1.199:8080/opencms/dphcontenido/create/index.jsp" method="get" id="fromget">
		<input type="text" value="prueba de tipo get" id="paramget" name="paramget"> <input type="submit" value="GET">
	</form>
	<br>
	<form action="http://192.168.1.199:8080/opencms/dphcontenido/create/index.jsp" method="post" id="frompost">
		<input type="text" value="prueba de tipo post" id="parampost" name="parampost"> <input type="submit"
			value="POST">
	</form>
</body>
</html>